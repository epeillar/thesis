
In this section, we present an experimental protocol to assess how repeatedly
interacting with the virtual/real content with an interaction technique changes
the perception in \gls{AR}. Participants will be asked to proceed to a distance
estimation task in the personal space using real and then virtual targets 
\fa{I will presented the other way around. The main task is the interaction
task, then the distance estimation task is used to measure changes in
perception.}. 
They will then perform an interaction task\fa{present the interaction task?},
chosen among three and before estimating again egocentric distances with of
virtual targets. In the following, we detail the apparatus, the perception and
interaction tasks, as well as the experimental design.

\subsection{Apparatus}
%participants
Participants seat on a chair in front of a white table. The table is
covered by a white sheet of paper to prevent any effect of the material
(texture, reflections, ...) and to fix makers for tracking purposes.
Participants wear an \gls{OST} \gls{HMD}, namely the Microsoft Hololens,
during all the experiment. 

%interaction device
The interaction device used is a commercial clicker, integrated in a 3D-printed
case. This custom device, designated as ``stylus'' in the following, has one
button used by participants to validate their answer, a thin tip to precisely
interact with the targets and a 4-sphere constellation of tracking markers. 
Its position is detected by a 6-camera ART
tracking system\footnote{\url{https://ar-tracking.com/}}. 
%TODO art precision

\begin{figure}[tbp]
	\centering
	\includegraphics[width=0.4\textwidth]{percinter/device.JPG}
	\hspace{5pt}
    \includegraphics[width=0.4\textwidth]{percinter/device2.JPG}
    \caption[Exp 5 - Interaction device]{Interaction device.}
    \label{fig:exp5-stylus}
\end{figure}


% stimuli
%TODO detail about proj
The real\fa{The notion of real stimuli is disturbing as they are projected stimuli..} stimuli are projected by a commercial projector on the table, while the
virtual ones are viewed on the table through the \gls{HMD}. Both stimuli are
white disks of the same size (3cm in diameter), crossed by a thin black cross
marking the center. Their brightness is adjusted to be as close as possible.
During the experiment, participants are instructed to point the center of the
circle. 

% systems
Overall, four systems are running during the experiment and are connected on a
dedicated network: (1) the camera-projector system, with a dedicated C++ program
running the calibration program explained thereafter; (2) the \gls{HMD}, running
a program created with Unity v2019.3.0f6\footnote{\url{https://unity.com/}} (3)
the tracking system, through the Dtrack
software\footnote{\url{https://ar-tracking.com/products/software/dtrack/}},
running on a Windows 7 PC; (4) the main experiment Python runner, which
processes the experimental parameters and allows the experimenter to control the
experiment.


\subsection{Calibration}
This calibration phase aims at determining the relation between the different
coordinates systems. Five systems must be referenced in relation to each other:
the headset, the physical workspace (the tabletop), the ART tracking system, the
camera detecting the user's responses, and the projector displaying real
objects. 

Markers are placed on the table to ensure the calibration of the workspace.
During the calibration phase, the camera is calibrated using the markers and the
Aruco computer vision
library\footnote{\url{https://www.uco.es/investiga/grupos/ava/node/26}}. The
Hololens uses the Vuforia library\footnote{\url{https://developer.vuforia.com/}}
to detect the position of the workspace. The projector is calibrated by
projecting markers, detected by the camera. Finally, participants are asked to
point at the different markers' corners to calibrate the stylus' position. The
entire calibration is checked by simultaneously displaying the real and virtual
elements and making sure that they are superimposed. 

\begin{figure}[tbp]
	\centering
    \includegraphics[width=0.4\textwidth]{percinter/markersHand.JPG}
    \caption[Exp 5 - Calibration markers]{Calibration markers and participant's hand during perception task. }
    \label{fig:exp5-markers}
\end{figure}

\subsection{Task and Procedure}
% overall presentation
During the experiment and after a presentation and calibration phase, the
participant performs four different tasks:
\begin{enumerate}
    \item a perception task using ``real'' stimuli (displayed by the projector); 
    \item a perception task using ``virtual'' stimuli (displayed in the
    \gls{HMD});
    \item an interaction task (chosen among three different ones);
    \item a perception task using ``virtual'' stimuli. 
\end{enumerate}
The ``real'' perception task will provide a baseline to evaluate the
performance of the virtual estimates. 

% perception task
\paragraph{Perception task.}
This task is composed of several trials (see \autoref{sec:expInter-expDesign}).
For each trial, the participant sees either a real (projected by a projector) or
a virtual (displayed by the \gls{HMD}) target presented for one second between
15 to 50 cm from the participant in the sagittal axis. Once the target
disappears, the participant is asked to point the center of the target with his
finger. He can validate his answer by clicking on the stylus' button carried in
the other hand. The position of the finger is captured using a camera and the
pointed distance recorded. 
%TODO param camera

% interaction task
\paragraph{Interaction task.}
This task is also divided in several trials (see
\autoref{sec:expInter-expDesign}). For each trial, a target is presented to the
participant. This target is displayed in the \gls{HMD} and is placed at one of
nine different positions in a $3\times3$ square. %\fa{Why this layout?}
This layout allows for covering the whole interaction space, ensures that each
participant has the same interaction task and provides sufficient different
positions to generate a random order without repetition. 
The central position is at 30
cm from the participant and the other possible target's position are distant
from 10 cm one from another. Each of the nine positions is displayed in a
random order. 

\begin{figure}[tbp]
	\centering
    \includegraphics[width=\textwidth]{percinter/interaction.png}
    \caption[Exp 5 - Interaction techniques]{Interaction techniques: direct interaction (left), indirect pointing (middle), gaze pointing (right).}
    \label{fig:exp5-interactionTechniques}
\end{figure}

For each target, the participant is asked to point its center using one of
the three following interaction techniques: 
\begin{itemize}
    \item \textbf{Direct interaction}: Using the stylus' tip, the participant
    touches the center of the target. 
    \item \textbf{Indirect pointing}: With the stylus acting like a laser
    pointer, the participant controls a virtual pointer on the table's surface.
    The participant validates his answer by clicking the button on the stylus.
    \fa{can we get a participant in the indirect pointing behaving as in the
    direct condition?}\ep{maybe we can ask them to rest their elbow on the table?}
    \item \textbf{Gaze pointing}: In this configuration, a virtual pointer on
    the table follows the participant's head movement. The stylus'
    button is still used to validate the answer. 
\end{itemize}
The target is visible during the whole interaction task. As soon as the target
is hit, the distance between the center of the target and the point selected by
the participant is recorded, the target disappears and the next one is
displayed. 

\subsection{Experimental Design} \label{sec:expInter-expDesign}

\begin{table}
	\small
	\centering
	\caption[Exp. 5 - Summary of experimental variables]{Summary of Independent and Dependent Variables.}
	\begin{tabular}{|c|r|c|l|l|}
		\hline
		\multicolumn{5}{|c|}{\textbf{Independent Variables}} \\
		\hline
		\multicolumn{2}{|r|}{\textit{observers}} & ? & \multicolumn{2}{l|}{(random variable)} \\
		\hline
		\textbf{C1} & \textit{distance} & 8 & \multicolumn{2}{l|}{from 15 to 50 cm} \\
		\hline
		\textbf{C2} & \textit{interaction} & 2 & before or after & (within subjects)\\
		\hline
		\textbf{C3} & \textit{interaction type} & 3 & direct, indirect or gaze interaction & (between subjects)\\
		\hline
		\multicolumn{2}{|r|}{\textit{repetition}} & 6 & \multicolumn{2}{l|}{} \\
		\hline
		\multicolumn{5}{|c|}{\textbf{Dependent Variables}} \\
		\hline
		\textbf{D1} & \textit{reported distance} &
		\multicolumn{3}{p{280pt}|}{Distance between the participant and his
		fingertip along the sagittal axis} \\
		\hline
		\textbf{D2} & \textit{reported distance ratio} &
		\multicolumn{3}{p{280pt}|}{Ratio between the reported answer for the
		virtual and the mean reported distance for the real stimuli} \\
		\hline
	\end{tabular}
	\label{tab:inter-exp-variables}
\end{table}

We propose a mixed-model design with the following conditions:
\begin{itemize}
	\item \textbf{C1}: The distance between the participant and the target.
	Eight distances are considered, all in the personal space, ranging from 15
	to 50 cm. 
	\item \textbf{C2}: The time of the perception task: before of after the
	interaction task. 
	\item \textbf{C3}: The interaction technique of the interaction task: direct
	interaction, indirect pointing or gaze pointing. 
\end{itemize}

The distance between the participant and the target (\textbf{C1}) is chosen to
be in the near distance field and easily reachable to ensure the precision of
the reporting technique and to facilitate the interaction task. 

In addition to the reported distance, the ratio between the reported distance in
virtual compared to real (\textbf{D2}) is also included in the analysis to
elicit the specific biases of virtual estimates. 

In summary, participants are presented with 48 trials for a given perception
task: 8 distances (\textbf{C1}) $\times$ 6 repetitions. Two perception tasks
with virtual targets are conducted, one before and one after the interaction
task (\textbf{C2}), which leads to 96 data points per participant. For each
perception task, the order of \textbf{C1} is randomized. The participants are
divided in three groups according to the interaction technique they use
(\textbf{C3}). The two dependent variables are the reported distance for a
virtual stimulus (\textbf{D1}) and the ratio between this distance and the mean
distance reported with a real stimulus in the same condition (\textbf{D2}). 