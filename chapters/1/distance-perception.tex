The topic of visual perception and in particular the perception of distances has
been the subject of a very large number of studies for several centuries. In
fact, visual perception is considered to be the first sense of human beings and
the perception of distances -- i.e. the ability to evaluate the position of an
object in space, relative to another object or to the observer position -- makes
it possible to measure and quantify this perception. Numerous levels of detail
are considered in this field, from the biological phenomenology of perception
\autocite{Hubel1965} up to models seeking to provide a global model of all the
factors influencing the perception of distances in visual space
\autocite{Koenderink2008}. 

In the perceptual space, the three dimensions are \emph{up-down}, \emph{sideways} and \emph{towards-away}. 
This last dimension informs the brain about depth and is known to be the one giving the less accurate information~\autocite{Ware2008}.
The majority of studies presented in this work aims to evaluate the distance perception of the observers, from their own point of view. 
That is to say the distance between an object and their own body, defined as \emph{egocentric distance}. \al{ref}
Some elements may extend to other dimensions, such as the perception of
distances between objects, called exocentric distances, and will be discussed
further in this thesis. 

Moreover, in this thesis we will use various names for similar but still
different concepts. 
\begin{itemize}
	\item \textbf{Distance perception}: the outcome from the perceptual
	process, applied to distances; it includes distance evaluation but is also
	part of some other perception, such as space perception. 
	\item \textbf{Distance evaluation}: externalization of distance perception,
	by word or gesture. 
	\item \textbf{Depth perception}: distance perception along the depth axis.
	It includes egocentric distance perception but also exocentric distances in
	the depth axis or relief perception. 
	\item \textbf{Space perception} includes ``perception of the spatial
	attributes of objects, their size, shape, stability, motility, and their
	distance and directional locations in reference to each other and to the
	perceiving subject'' \autocite{Carr1935}. 
\end{itemize}

Thus, we will focus on more general elements of depth perception,
without dwelling on the physical and physiological upstream elements. We will
first introduce common definitions to categorize distance perception and then
present some context elements related to the ``visual space''.
\al{figures ?}

\subsection{Depth cues and cue theory} \gm{en y repensant, tu ne parles pas de la notion d'indices dans la partie précédente sur la perception. et notamment pas des incohérences entre indices (qui pourrait constituer une intro à la RV notamment)}
\label{part:def-cues} 
The human is able to analyse the 2D images given by its eyes to get a 3D representation of his environment. 
This is reported to be achieved by using \emph{depth cues}. \jmn{ref}\al{ref}
Categorizing these depth cues is a difficult task as they are usually closely related and similar in nature. 
Several authors have proposed slightly different classifications~\autocite{Goldstein2017,Howard2002}. 
We present here a typology adapted from Goldstein's classification~\autocite{Goldstein2017} but simplifying it into only three categories. 
Our classification is presented in \autoref{fig:persoDepthCues}. 
Refer to \autoref{fig:howardDepthCues} and \autoref{fig:goldsteinDepthCues} for the classifications of the authors mentioned above. 


\begin{figure}
    \def\svgwidth{\textwidth}
    \input{pictures/howardDepthCues.pdf_tex}
    \caption{Depth cues classification from \cite{Howard2002}.}
    \label{fig:howardDepthCues}
\end{figure}

\begin{figure}
    \def\svgwidth{\textwidth}
    \input{pictures/goldsteinDepthCues.pdf_tex}
    \caption{Depth cues classification from \cite{Goldstein2017}.}
    \label{fig:goldsteinDepthCues}
\end{figure}

\begin{figure}
    \def\svgwidth{\textwidth}
    \input{pictures/persoDepthCues.pdf_tex}
    \caption{Depth cues classification proposed.}
    \label{fig:persoDepthCues}
\end{figure}


\begin{figure}[tbp]
    \centering
    \subfloat[Linear perspective]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/linearPerspective.jpg}
	}
    \hspace{0.005\textwidth}
    \subfloat[Distance from the horizon]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/height.jpg}
	}
    \hspace{0.01\textwidth}
    \subfloat[With no familiar object visible, the distance information provided by the size of the objects is only relative ...]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/relativeSize.jpg}
	}
    \hspace{0.005\textwidth}
    \subfloat[... while the distance information becomes absolute when familiar elements are present.]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/absoluteSize2.jpg}
	}
    \hspace{0.01\textwidth}
    \subfloat[Atmospheric haze]{
       \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/atmosphericHaze.jpg}
    }
    \hspace{0.01\textwidth}
    \subfloat[Texture gradient]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{depthCues/textureGradient.jpg}
	}
	\caption{Example of main depth cues.}
\end{figure}

Depths cues can be classified in three categories: oculomotor, pictorial and motion cues. \fa{does it make sense to introduce here the range of the depth cues, and their properties? (relative, absolute) and so on?}

\textbf{Oculomotor cues} refer to the cues provided by the oculomotor system of the eyes and the fact that the human being has two eyes. 
This category includes: 

\begin{itemize}
	\item \textbf{Binocular disparity} takes advantage of the difference in the points of view and the overlap of the visual field of each eye. It allows, by comparing the position of an object on one side with respect to the other, to have an estimation of the distance. For example, an object placed on the right in the field of vision of the right eye and on the left when seen by the left eye will thus be further away than an object in the center of each eye. % (cf. figure xx). \fa{needed?}
	\item \textbf{Binocular convergence} corresponds to the rotational movement of the eyes, i.e. the angle at which the eye muscles must position the eyes to look at an object. Somatosensory receptors in the muscles of the eye allow for continuous feedback of this angular position. 
	Interestingly, the resting position of the eyes is divergent. When the eye
	muscles relax, the eyes align with the axis of the orbits and are therefore
	slightly exotropic, i.e. turning outwards. 
	\item \textbf{Accommodative focus} results from the movements of the eye muscles which must compress the crystalline lens in order to clearly project the image of the object on the retina. When these muscles are completely relaxed, the objects placed at infinity (beyond 6 meters for the human eye~\autocite{Koretz1988}) are sharp. Although this cue gives an absolute indication of distance, it is much more effective in discriminating distance in an ordinal manner. 
\end{itemize}

\textbf{Pictorial cues} are sources of depth information that can be depicted in a picture~\autocite{goldstein2005pictorial}. 
This category includes: 

\begin{itemize}
	\item \textbf{Atmospheric haze}. The further away the objects are, the greater the air between the observer and the object. Light rays must therefore pass through more air and hence more airborne particles (dust, water droplets, air pollution, etc.). They are then deviated by these elements which causes a less sharp image with a slight blue tint. 
	\item \textbf{Linear perspective} or \textbf{perspective convergence}. Looking at parallel rectilinear elements we can see that they seem to come closer and converge towards a point. This cue is particularly useful in urban environments with many rectilinear elements. 
	\item \textbf{Height in the visual field} also provide depth information, in particular for open views. The closer the elements are placed to the horizon line, the farther from the observer they will be perceived. 
	\item \textbf{Relative size} makes it possible to interpret larger objects
	as being closer. The size of an object is inversely proportional to the
	distance from the viewer. 
	\item \textbf{Occlusion} and transparency allow the observer to see the order of the objects. When one overlaps the other then it is perceived as closer. However, this depth cue is only ordinal and does not give an absolute indication of distance. 
	\item \textbf{Shadows, shading and other anchors}. Shading refers to the
	shadows that appear on the object due to its shape and orientation in
	relation to the light source. It makes it possible to interpret the shape of
	an object and thus to better understand its position. Shadowing designates
	shadows cast by the object on another surface. They allow to have more
	elements of reference, called anchors. 
	\item \textbf{Texture gradient}. When elements are repeated many times over
	a large area, they appear as a texture whose patterns narrows with distance.
	Distant elements are seen smaller, but unlike the depth cue of relative
	size, it is the whole texture that serves as a reference. The texture
	contrast seems to diminish with distance. 
\end{itemize}

\textbf{Motion cues} take advantage of the continuity of observation during a displacement to infer distance information from the speed and relative positions of the elements seen. 
This category includes: 
\begin{itemize}
	\item \textbf{Motion parallax}. As the observer moves and fix a still object
	(or when objects move and the observer stays still), the closest elements
	will move more than the farthest elements. This makes it possible to
	estimate the distance between the observer and the object under
	consideration. This depth cue is not only relative but also allows an
	absolute estimate of the distance. This cue is massively used in animated
	movies to simulate depth~\autocite{Nawrot2010}. 
	\item \textbf{Deletion and accretion} are the dynamic counterpart of the occlusion and an ordinal approach to the parallax motion. As the observer moves, some objects are occluded by others. The more an object is subject to accretion/deletion, the more it is perceived as distant~\autocite{Kaplan1969}. 
	\item We can also mention \textbf{optic flow}, which translates all the visual elements generated by a movement. It includes parallax motion but also other types of movement. For example, a rotation of the head or a displacement in the direction of the gaze generates an optic flow but does not involve motion parallax. This cue is a major asset used by  animals for visual perception~\autocite{Srinivasan1996a,Wallace1959,Eckmeier2013}. 
\end{itemize}

As summarized in \autoref{tab:depthCues}, in addition to type (oculomotor/pictorial/motion) and effective distance range (see \autoref{part:def-fields}), depth cues can also provide an absolute or relative distance information.

Moreover, while the depth cue theory explains high-level depth information, some
specificities of the human vision such as stereoscopic depth perception of local
surfaces provide low-level interpretation. For example, random-dot stereograms
demonstrate that stereopsis based on a simple point-to-point comparison of the
two eyes' images can provide a depth sensation and does not require the presence
of monocularly visible forms or contours
\autocite{Collett1985,Ramachandran1985,Takeichi1992,Nakayama1996}.

In the real world, several of those cues are then combined to evaluate distances. 
Interaction and fusion models have been proposed to specify the weight of each cues~\autocite{Landy1995}. 
Many models were presented, including more or less depth cues and with more or less strict fusion criteria. 
Overall, no model seems to be clearly dominant. 
The two main obstacles seem to be the strong and non-linear interactions between cues coupled with the highly individual nature of the perception, as highlighted in the previous section. 
However, those models suggest that the \emph{consistency} between cues play a great role. 
Consistency between cues is a much more impacting factor than the individual
effect of each cue \autocite{Hubel1965,Fischer2000,Landy1991}. 

Finally, a last factor arises: the \gls{SDT}~\autocite{Gogel1973}. Indeed, this
inner bias is defined as a tendency for the observers to perceive objects at a
relatively near distance when presented without any depth cue. In such a
configuration, observers tend to perceive objects at about 2
meters~\autocite{Gogel1969}. 

% Relative importance
% \autocite{Nagata1991} in Ellis1991


\subsection{Distance fields} \fa{provide first this?}\ep{fields needs cues to
be defined, but cues varies according to fields}
\label{part:def-fields} 
\textcite{Cutting2003} proposed a categorization of spaces
based on the relative strength of depth cues according to distances. (See Fig.
\ref{fig:vishton1995-000} for this relative weights plotted.) This induced three
different spaces: \textit{near-}, \textit{medium-} and \textit{far-}field , also
reported in \textcite{Vishton1995} as \textit{personal}, \textit{action} and
\textit{vista} space. As the second naming suggests, these spaces are also related
to human action.

\textbf{Personal space} is defined as the zone immediately surrounding the
observer, within arm's reach and slightly beyond.
\Citeauthor{Cutting2003} defined it within 1.5~meters, which is the working
space for a stationary individual. At that range, fives cues are the most
important: occlusion, retinal disparity, relative size, convergence and
accommodation, in that order.

\textbf{Action space} is defined as the space of an individual's public action.
He can move quickly within this space, talk without difficulty and throw
objects. This space induces also a different ranking of depth cues. The five
most, sorted by weight are occlusion, height in the visual the field, binocular
disparity, motion perspective and relative size. This field extends to about
30~meters.

\textbf{Vista space} extends beyond these 30~meters, until the visual field
limit, a space which can be seen but not reached quickly. At these distances,
the four more important cues are the usually called \textit{pictorial cues}:
occlusion, height in the visual field, relative size and aerial perspective.

As explained in the previous part, distance fields are associated with the
evolution of the weight of depth cues according to the distance but also actions
that can be done in these spaces. On the one hand, they are defined by the depth
cues, but they also make it possible to predict and interpret the evolution of
perception as a function of distance (See \autoref{fig:vishton1995-000}). As we
have previously detailed, action and perception are strongly linked, and the
fact that distance fields allow for actions of different nature also induces
differences in perception.

\input{papers/survey/tabDepthCues}

\begin{figure} \centering
    \includegraphics[width=\textwidth, height=8cm, keepaspectratio]{radialDepthCues.png}
	\caption[Effective range of depth cues]{Effective range of depth cues, from \textcite{Renner2013}.}
\end{figure}

\begin{figure} \centering
    \includegraphics[width=\textwidth]{pictures/depthCuesWeight-color.pdf}
	\caption[Relative depth cues strength]{Relative depth cues strength, adapted
		from \textcite{Vishton1995}.These functions delimit three types of
		spaces, determined by different depth cues' weight.}
	\label{fig:vishton1995-000} 
	\label{fig:depthCues-weight}
\end{figure}

\subsection{Global theories of visual space in real environments} \fa{the goal and purpose of this part is less clear to me.}
Beyond the specific aspect of perception and a factorial modeling of perception, some authors seek on the contrary a more holistic approach to the modeling of perception. 
The notion of \textbf{visual space}, i.e.\ the global perception observers have of the space based on their visual perception, has been largely investigated in real environments. 
Depth perception in real environments is covered by a very wide corpus of
studies dating back to the 18\textsuperscript{th} century, providing both
experimental studies and complex numerical models, as detailed thereafter. 
Many of those studies report that people commit systematic errors when asked to estimate geometrical properties of their environment from a fixed point of view~\autocite{Battro1976, Battro1978, Wagner1985}. 

Several mathematical models have been proposed to explain these differences between the visual space and the real physical space. 
All these models seek to propose a global theory of transformation between the real space and the visual space measured through perceptual experiments, and thus to find the mathematical shape  that best describes the visual space.  
\textcite{Gibson1950} stated that the visual space is Euclidean. 
\textcite{Angell1974}, \textcite{Daniels1974} and \textcite{Reid1764} suggested it was spherical. 
\textcite{Blank1953,Blank1959,Blank1978}, \textcite{Indow1967,Indow1979} and \textcite{Luneburg1947,Luneburg1950} hinted it was hyperbolic. 
\textcite{Hoffman1966,Hoffman1980} proposed that it reflects a Lie algebra group. 
Then, \textcite{Wagner1985} suggested affine-transformed Euclidean models and another observer-based model. 
To date, there is no consensus yet on this topic. 
\textcite{Koenderink2008} even suggested that the visual space cannot be described by any Riemannian structure.

Thus, most perceptual models lean on the notion of space's intrinsic curvature. This notion is defined by \textcite{Gauss1828} and used by many authors to measure the deformation of the perceived visual field. 
They used various techniques to evaluate this curvature, such as alley building~\autocite{Indow1967}, exocentric pointing~\autocite{Koenderink2000}, or distance, angle and area estimations~\autocite{Wagner1985}. 
The studies conducted on that specific parameter state that the curvature of the visual space might not be constant~\autocite{Indow1991}. 
Moreover, \textcite{Koenderink2000} suggested that the curvature changes from
elliptic in near space to hyperbolic in far space, and then becomes parabolic at
very large distances. \textcite{Koenderink2002} also evaluated the curvature of
frontoparallels: observers were asked to place a radio-commanded vehicle at the
bisected point of a linear segment. 
They found that frontoparallels were significantly concave toward the observers. 
Interestingly, these results were the opposite of those obtained in a similar open environment, but with a different task~\autocite{Koenderink2000}.

It therefore appears that the visual space cannot be easily modeled and that current models fail to completely explain it. 
Furthermore, the inconsistency of distance perception is not only specific to
the visual field. 
The haptic space, i.e. the space perceived through the active exploration of
surfaces and objects by a moving subject, also appears to be
non-Euclidean~\autocite{Kappers1999} and the haptic perception of spatial
properties of objects seems distorted and geometrically
inconsistent~\autocite{Fasse2000}. 
Even if the in-depth explanation of other perception channels is beyond the scope of this study, it illustrates the global inconsistency of human space perception.

