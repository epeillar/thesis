

The principles underlying human perception have been widely studied
for many centuries \autocite{Berkeley1709}. The initial theory of the internal
representation of space in the form of a mind map \autocite{kaplan1973cognitive}
was later challenged by a topological approach of space
\autocite{kuipers1983cognitive}, rejecting the idea of a metric representation
in favor of a semantic representation of objects in relation to each other.
However, these theories have also proved to be too restrictive. The most recent
works agree on a mixed, more ecological conceptualization of perception. They
focus on describing how different types of information, resulting from the
interaction of the individual with his environment, can contribute to the
construction of a mental representation \autocite{tversky1993cognitive}. This
approach is the one underlined by \textcite{Goldstein2017} in their model of
perception and in particular of the perceptual process. It is on this theory and
related concepts that we will base the remainder of this chapter. 


\subsection{The Perceptual Process} \label{subsec:perceptualProcess}

This process allows external stimuli to be transformed into a conscious experience, and eventually action. 
It can be broken down into several relatively independent steps, which are detailed below. 
However, it is important to note at this point that although relatively independent, these steps are not simply unidirectional links in a chain. 
There are constant exchanges between the different levels of perceptual
information processing, and the organization and segmentation presented are a
simplified version of the overall perceptual process. 
Most of the examples in this section will focus on vision, but it should be
noted that this process is not limited to vision alone and is common to all
human senses. 


\begin{figure}
    \def\svgwidth{\textwidth}
    \input{pictures/perceptualProcess.pdf_tex}
    \caption[Overview of the perceptual process]{Overview of the perceptual process, adapted from \cite{Goldstein2017}.}
    \label{fig:perceptualProcess}
\end{figure}

The perceptual process can be described as follows, and is presented in \autoref{fig:perceptualProcess}:
\begin{itemize}
    \item stimuli (distal \& proximal), 
    \item physiological response (receptors \& neuronal processing), 
    \item perception \& recognition, 
    \item action. 
\end{itemize}

\paragraph{Stimuli.} They are the first step of perception. 
They consist of all the elements external to the observer's body that work together to create what will be perceived. 
There are two types of stimuli: distal stimuli and proximal stimuli. 
%
\textbf{Distal stimuli} include all elements that could be perceived and are ``distant'' from the observer. For example, a tree, a bird song or the sun. 
They are not directly perceived (the tree, like the sun, does not come into contact with the eye that sees them) but are at the origin of the perception reaction. 
%
They are then transformed to finally become \textbf{proximal stimuli}. 
This second category includes the final stimuli that are then detected by the human body's sensors: light image projected on the retina, vibration of the air in contact with the eardrum, etc. \jmn{Les 2 paragraphes manquent de refs}

\paragraph{Physiological response.} Once the stimulus hits the sensory \textbf{receptor}, it is transformed, through a series of electro-chemical reactions that depend on each receptor, into an electrical signal. 
It is then routed through the neural system, which transmits and transforms the signal. 
At this point, the reaction becomes internal to the individual. 
However, this is still an electrical impulse and not perception. 
The signals are transmitted and modified by nerves and then pre-\textbf{processed} by neurons, but this part of the perceptual process is unconscious and solely physical. 
%The magic happens in the next step. \gm{\textit{aime pas}}

\paragraph{Perception \& Recognition.}
At this step, electrical signals are transformed into conscious experience. 
All the signals are combined to give a mental representation of what is perceived. 
At this stage perception and recognition are two distinct yet interrelated processes. 
The notion of perception reflects the fact that the subject is conscious of perceiving something, which is therefore different from recognizing it. 
Some brain pathologies sometimes can provoke a defect of one of these two functions. 
Patients are then conscious of seeing certain things but unable to recognize them. 

The fundamental role of \textbf{knowledge} at this stage should also be underlined. 
Indeed, the conscious experience that results from perception is not only the
sum of the electrical signals coming from the receptors but also the whole of
the observer's past experiences and even his cognitive state. 
\fa{top-down vs bottom-up processes.}

\paragraph{Action.}
%TODO ajouter une partie complète sur l'interaction, boucle etc ?
Action can be seen as the last step in the process of perception and is one of the main outcomes of perception. 
While its integration as a part of the perceptual process is debatable, some authors suggest that it must be associated in it because action used to be a major goal of perceptual processing for animals~\autocite{Milner1995}. 
However, the interconnection between perception and action must be stressed. 
Action makes it possible to adapt one's point of view, one's way of perceiving as well as reacting to what is perceived. 
The links between perception and interaction will thus be explained further on. 

\subsection{The perception system's purposes: compensating and interpreting} 
Although we have detailed the perceptual process systemically here, it must be stressed that none of these steps can be considered ``simple'' or ``perfect''. 
Imperfections, unexpected behaviors or simply ambiguous results can be observed at each step. 
Firstly, the sensors of the human body are limited: the eyes have a limited field of view and depth of field, they are not able to distinguish some nuances of colour, the ears have a limited frequency range, the olfactory receptors may be saturated after too long an exposure, etc. 
But it does not stop there. The next steps, perception and recognition, internal to the individual, can also be biased. Cognitive state, experience, a priori are all factors likely to blur perception. 
The possibilities of interaction can even be responsible for significant
perceptual biases \autocite{Franklin1990}. 
Thus, the cognitive process that enables perception is by nature limited by its inputs and must constantly adapt to compensate for its shortcomings. 
This adaptation constitutes a formidable capacity but is also at the origin of many defects of perception. 
While it is useful to be able to interpret a deficiency, it can also alter perception by causing over-interpretation. 
Many illusions and perceptual biases have their origins not in a failure of the
sensory system but in an erroneous interpretation of the entire scene
\autocite{Bartz2008}. 
\fa{provide some? Bartz, D., Cunningham, D. W., Fischer, J., \& Wallraven, C. (2008, April). The Role of Perception for Computer Graphics. In Eurographics (STARs) (pp. 59-80).}\gm{ça sort un peu du chapeau dit comme ça, manque de références}\al{très interprétatif}

\begin{figure}[tbp]
    \centering
        \subfloat[The Hermann's grid. Contrast enhancement in the visual system create gray points at the intersections which vanish when looked directly. ]{
            \centering
            \includegraphics[height=0.2\textheight,width=0.49\linewidth,keepaspectratio, trim={-5cm 0 -5cm 0}, clip]{illusion/hermann}
        }
	\hspace{0.01\linewidth}
    \subfloat[The blind spot. When closing the left eye, staring at the cross with the right eye and slowly moving the head towards and away from the screen, the dot disappears. This happens when the image falls in the ``blind spot'' of the retina, where the optic nerve starts and no receptors are present. ]{
            \centering
            \includegraphics[height=0.2\textheight,width=0.49\linewidth,keepaspectratio, trim={-1cm 0 -1cm 0}, clip]{illusion/blindSpot}
    }
    \hspace{0.01\linewidth}
        \subfloat[The squares A and B have the same color but the brain interpret one as darker than the other because of the shadow. ]{
            \centering
            \includegraphics[height=0.2\textheight,width=0.49\linewidth,keepaspectratio, trim={-5cm 0 -5cm 0}, clip]{illusion/gray_square}
        }
    \hspace{0.01\linewidth}
        \subfloat[Concentric gray circles appear to be spirals, from \cite{Kitaoka1998}]{
            \centering
            \includegraphics[height=0.2\textheight,width=0.49\linewidth,keepaspectratio, trim={-1cm 0 -1cm 0}, clip]{illusion/uzuampan2011b}
        }
    \hspace{0.01\linewidth}
    \subfloat[Blue field entoptic phenomenon. Tiny bright dots appear in the visual field when looking to a bright blue light. The speed of the dots varies in sync with the pulse; they briefly accelerate at each heartbeat.  They are created by an accumulation of red blood cells behind the white blood cells moving in the capillaries in front of the retina of the eye. The brain usually edit out those dots under normal vision but since the blue light is especially absorbed by the red cells, some artifacts remain visible when the eye is exposed to bright blue light. ]{
        \centering
       \includegraphics[height=0.15\textheight,width=\linewidth, trim={-3cm 0 -3cm 0}, clip]{illusion/blue}
	}
    \caption{Experiments showing flaws in the visual perception system. }
    \label{fig:illusions}
\end{figure}


\section{Forewords about he human visual system}

Vision is the main sense of humans. This sense is processed by the visual
cortex, the largest system in the human brain and is provided by the
\textit{visual system}. The visual
system comprises the sensory organ (the eye) and the part of the central nervous
system (the optical nerve and the brain) which gives organisms the ability to
process visual detail. It detects and interprets information from visible light
to build a representation of the surrounding environment. 

\begin{figure}[tbp]
    \includegraphics[width=\textwidth]{pictures/eye.png}
    \caption{Inner structure of the eye (from \cite{Goldstein2017})}
    \label{fig:eye}
\end{figure}

At the beginning of the visual system lies the eye. As represented in
\autoref{fig:eye}, the light enters the eye via the pupil, passing through the
cornea. Then, the lens focuses the light on the retina. The light is absorbed and
activates the receptors cells (cones and rods) of the retina. The cones, of three
different types, allows to perceive the different colors and are more sensitive
to contrast. The rods, more numerous but less precise, are mostly used for low
luminosity vision. When excited, the receptor cells generate an electric impulse
which is transmitted to the brain by the optical nerve. 

Because of its design, the image formed by the eye must be projected onto the
retina to be seen clearly. Thus, to see objects placed at different distances,
the eye has to adjust the focal length of the lens to form the image on the
retina. At rest, the image is sharp to infinity. The ciliary muscles need to
contract to see closer objects sharply. 

It has to be noted that the horizontal \gls{FoV} of a still eye is only
160\textdegree~\autocite{Denion2014}. The movements of the eye and the head,
when combining the \gls{FoV} of both eyes, increase this value to
300\textdegree. Moreover, the eye has an especially sharp vision in the center
of the retina (the fovea), which makes these movements even more important.
Indeed, the fovea, which is 0.01\% of the retina is represented by a large area
in the visual cortex (8-10\% of the cortical map's area, \cite{Goldstein2017}).