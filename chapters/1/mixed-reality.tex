
\section{Definitions}

The definitions and scope of so called Virtual, Augmented or \acrfull{MR} are
numerous and changing. The purpose of this section is then to elicit the main
principles underlined by these notions, in order to highlight the specificities
related to perception. 

\subsection{Real and Virtual}

First of all, and because these concepts will be widely discussed afterwards,
let us first take some time to define these two notions. \al{bof}
We will use the definitions given by \textcite{Milgram1994a}:
\begin{itemize}
	\item \textbf{Real} objects are any objects that have an actual physical existence.
	\item \textbf{Virtual} objects are objects that exist in essence or effect, but not formally or actually.
\end{itemize}

\subsection{Virtual Reality}
We will not discuss here the premises of what can be seen as \gls{VR}, as they can be found in descriptions such as Plato's myth of the cave~\autocite{Plato-400} or Giovani Fontana's magic lantern~\autocite{Fontana1420}. \al{vague}
We will focus on \acrlong{VR} in its current use, a term popularized by Jaron Lanier in the 1980s~\autocite{Machover1994}. 

For the remainder of this work, we will use the following definition, stated as \textit{technical definition} in \textcite{Fuchs2003}: 
\begin{quote}
	Virtual Reality is a technical and scientific area making use of computer science and behavioral interfaces in order to simulate 3D entities behavior in a virtual world that interact in real time among themselves and with the user in pseudo-natural immersion through sensory-motor channels.
\end{quote}

However, it is also interesting to mention another, so-called \textit{functional} definition from the same book: 
\begin{quote}
	Virtual reality will allow the user to extract himself from the physical reality in order to virtually change the time, place and/or type of interaction: interaction with an environment simulating reality or interaction with an imaginary or symbolic world. 
\end{quote}

This definition is quite close to one of those given by \textcite{lanier2017dawn}: 
\begin{quote}
	The substitution of the interface between a person and the physical environment with an interface to a simulated environment.
\end{quote}

Moreover, I cannot resist quoting a few other definitions, from Jaron Lanier's book, which are related to concepts that will be detailed later in this work. 
%
One recalls the fundamental notion of perception, while underlining the difference between real and virtual, and the potential contribution of one to the other:
\begin{quote}
	A coarser, simulated reality fosters appreciation of the depth of physical reality in comparison. As VR progresses in the future, human perception will be nurtured by it and will learn to find ever more depth in physical reality.
\end{quote}

And this last definition, which underlines the contribution of the brain in the process of creation and perception, essential to the perception of a coherent environment: 
\begin{quote}
	Technology that rallies the brain to fill in the blanks and cover over the mistakes of a simulator, in order to make a simulated reality seem better than it ought to.
\end{quote}

Overall, some common elements of these definitions can be highlighted. 
First, the fact that ``virtual'' reality is defined in opposition to ``actual'' reality; as a mirror or as a way of substituting for it. 
In addition, one also observes the major place given to interaction in these definitions. 
We will discuss later in this work the effects of interaction through the prism
of perception in \gls{VR}, but it is worth emphasizing that interaction lies in
most of the \gls{VR}'s definition. 

\subsection{Augmented Reality}
Concerning \gls{AR}, we will use the definition of \textcite{Azuma1997}, revised in \textcite{azuma2001recent} and supported by the book from \textcite{Schmalstieg2016}. 
This definition describes AR as:
\begin{quote}
	[...] any system that has the following three characteristics:
	\begin{itemize}
		\item combines real and virtual objects in a real environment;
		\item runs interactively, and in real time; and
		\item registers (aligns) real and virtual objects with each other.
	\end{itemize}
\end{quote}

This definition can also be compared with that of \gls{VR}. 
Both emphasize the importance of interaction, but they are at odds in their purpose. 
While \gls{VR} seeks to substitute the virtual for the real, \gls{AR} seeks to mix the two in the best possible way. 
However, does this difference in purpose between the two concepts imply a difference in nature? 
Are these two notions really so different? 

\subsection{Continuum, Mixed Reality and other notions}

% MR
Rather than two separate concepts, \textcite{Milgram1994} proposed the vision of a continuum between reality and virtual reality, which they named \acrfull{MR}.
It would contain all the concepts and techniques that mix real and virtual, excluding solely real or virtual techniques. 
Technologies are then placed on this continuum according to their proportion of real/virtual elements. 
\Gls{AR} corresponds here to any system which, in a real environment, adds virtual elements. 
We can also cite \emph{Augmented Virtuality} which, mirrored to \gls{AR} on this
continuum, corresponds to the addition of real elements in virtual environments
\autocite{Milgram1994}. 

% RV ⊂ RM ?
\paragraph{Does VR belongs to MR?}
The term ``\acrfull{MR}'' is sometimes preferred over ``augmented reality'' because it is considered to be broader and more versatile~\autocite{Schmalstieg2016}. 
Therefore, \gls{MR} is both defined as a continuum including \gls{AR} but not \gls{VR}, but also as a more generic word to refer to any kind of application mixing real and virtual. 
Moreover, in its motivation \gls{VR} appears to be in opposition with every shape of the concept of \gls{MR} which seeks to mix real and virtual content, while \gls{VR} seeks to substitute the real for the virtual. 
However, it does not fully succeed to. 
Indeed, \gls{VR} always includes interaction with a human (who is undoubtedly
real). For example, a CAVE is a \gls{VR} device but the real body of the user is
still visible. 
% \jmn{pas totalement d'accord, c'est ce que perçoit l'humain qui devient virtuel et plus réel. 
% L'humain en tant que tel n'est jamais considéré comme faisant partie du monde
% virtuel}\ep{ben, un CAVE c'est de la VR, et pourtant on voit son corps réel}
This necessity of interaction implies that \gls{VR} cannot be fully virtual as the interaction always has a real origin. 
As such, and despite its purpose, \gls{VR} does not succeed to be solely virtual. 
Since the Ultimate Display \autocite{Sutherland1965}, which would be able to fully substitute real for virtual is not ready yet, a ``complete'' Virtual Reality cannot be achieved yet. 
As such, we believe that for now, and as long as a real human would interact with virtual elements in \gls{VR}, Virtual Reality has to be considered as a Mixed Reality technology. \al{opinion}\ep{oui}

\section{Devices}

The devices used for \gls{AR} purposes can be classified in three categories,
according to the distances between the device, the observer and the
object~\autocite{Schmalstieg2016, Bimber2005a}: near eye displays, handheld
devices and projectors. In order to underlines the perceptual challenges
associated with these categories and be able to compare with \gls{VR} devices,
we will complement these categories with \gls{VR} devices. The main difference
is that the ``projectors'' category will be extended in order to include all
planar devices, such as screens. See \autoref{fig:devices-classif} for an
overview of this classification. 

\begin{figure}[tbp]
    \def\svgwidth{0.9\textwidth}\centering
    \input{pictures/devicesCategories.pdf_tex}
	\caption[Classification of \gls{MR} devices]{Classification of \gls{MR}
	devices, adapted from \textcite{Schmalstieg2016}.}
	\label{fig:devices-classif}
\end{figure}

\subsection{Near-eye displays}
Near-eye displays are devices that ``place the image directly in front of the
user’s eye using one or two small screens'' \autocite{Bowman2004}. Nowadays
near-eye displays are generally \acrfullpl{HMD} \autocite{laviola20173d}. Wearing a device on the head
involves complex designs in order to make the device unobtrusive and
comfortable \autocite{Rolland2009}. Even if the first one were not tracked in
position, today's every modern \glspl{HMD} have in-build tracking systems. There
are three categories of \glspl{HMD}, depending on if and how the real
environment is visible: Non see-through \glspl{HMD}, video see-through
\glspl{HMD} and optical see-through \glspl{HMD}. 

\paragraph{Non see-through HMDs} In \gls{VR}, near-eye displays are usually
referred as ``non see-through \glspl{HMD}'' or simply ``\glspl{HMD}''. Those
devices are designed to make the user unable to see the real world and then
provide a complete immersion in the \gls{VE}. Nowadays, several major companies
have developed their own non see-through \gls{HMD} like HTC, Oculus, Valve or
Samsung (see \autoref{fig:VR-HMD}). These devices use either two small LCD (or
OLED) screens or a smartphone combined with optical lenses to enable the user to
see the rendered image. Usually, the devices that use smartphones are limited by
the smartphone's performance and it is common to have more latency and a lower
refresh rate.

\begin{figure}[tbp]
    \centering
	\includegraphics[height=4cm]{devices/vive.png}
	\includegraphics[height=4cm]{devices/hololens.jpg}
	\includegraphics[height=4cm]{devices/hpMR.jpg}
	\caption[Example of commercial \glspl{HMD}]{Example of commercial \glspl{HMD}: the HTC Vive (left), the Microsoft Hololens 2 (center) and the HP VR1000 (right)}
	\label{fig:VR-HMD}
\end{figure}

\paragraph{\Acrfull{VST} \glspl{HMD}} These \glspl{HMD} use cameras placed in
front of the user's eye in order to provide the visual feedback of the real world.
Images from real and virtual environment are then displayed by the \gls{HMD} in
the same way as for non see-through \glspl{HMD}. The \gls{VST} technique allows
for a good superimposition of the real and the augmented layers, since they are
both displayed by the same screen. However, the intrinsic specificities of the
cameras used to provide visual feedback, such as \acrlong{FoV} or resolution
limitation and optical distortion, may induce viewing issues. 

\paragraph{\Acrfull{OST} \glspl{HMD}} These \glspl{HMD} mostly use
semi-transparent mirrors to superimpose the augmentation and the direct view of
the environment. The main advantage of this technology is that it provides a
direct view of the environment, with sharp and quick view but induce more
constraints on the augmentations' rendering (e.g. latency, colors and depth of
field). Indeed, the augmentations will suffer from the constraints of the
displays while the direct real view will not be affected. Then, the differences
between real and virtual rendering will be even more visible. 


\subparagraph{More details on accomodation issues}

The more common \gls{OST} \glspl{HMD} are devices which use fixed lenses and
screens to provide the virtual images \autocite{Schmalstieg2016}. However, this
induces a fixed accommodation cue. 

In order to overcome this phenomenon, some \gls{AR} devices propose other
techniques to display virtual images, which can be categorized as follows: 
\begin{enumerate}
    \item The most common of those devices are \textit{varifocal displays}.
    Images provided by these devices are displayed at a certain depth but the
    focal plane is movable. Varifocal displays can be implemented in several
    ways, each having some limitations, e.g. \gls{FoV}, bulkiness or eyebox
    size~\autocite{Liu2008,Hu2014,Dunn2017,Aksit2017}.

    \item \textit{Multifocal devices} use multiplanar volumetric projection
    displays based on a stack of laminated planes. This technique can create a
    more plausible accommodation depth cue and, if dense enough, can provide a
    continuous and accurate depth cue~\autocite{Rolland2000, Hua2014}. This
    technique is used in the Magic Leap \gls{AR}
    headset\footnote{\url{https://www.magicleap.com/}} to provide a better depth
    sensation. However, this device only provides two distinct focal planes.

    \item Other devices try to reproduce the exact light-field which would have
    been created by the virtual images with \textit{holographic rendering}.
    These devices are able to provide a three dimensions image, with a per-pixel
    focal control. If the final image is precise enough, the depth cues provided
    by the virtual object are the same as the ones which would have been created
    by a real object~\autocite{Maimone2017,Kim2015}. 
\end{enumerate} 

All these technologies are more accurate than standard fixed-focal \gls{OST}
displays. However, they still suffer from other major drawbacks and limitations,
such as a limited resolution, a high space requirement and high cost. Overall,
studies presenting those technologies usually focus on a specific rendering
technique and are never compared to each other. Furthermore, these studies focus
on technological challenges but the techniques they are presenting remain
untested regarding their impact on perception. 
%
Then, another technology can be presented; a type of displays that does not
have a fixed focal plane since they do not have any focal plane:
\acrfullpl{RPD}. 

\subparagraph{What are Retinal Projection Displays?}\label{subpar:RDP}

\begin{figure}
    \centering
    \subfloat[Principle of the Maxwellian view~\autocite{Lin2017}.]{
       \includegraphics[width=0.5\linewidth]{pictures/maxwellianView.png}
       \label{subfig:SLM-Maxwellian-view}
	}
	\hspace{0.05\linewidth}
    \subfloat[QD-Laser VISIRIUM® technology using a MEMS mirror and a free-form
    reflecting mirror~\protect\footnotemark.]{
       \includegraphics[width=0.4\linewidth]{pictures/retissa.png}
       \label{subfig:MEMS-Maxwellian-view}
	} 
	\caption{Possible optical configurations of \glspl{RPD}.}
    % \includegraphics[width=0.95\linewidth]{pictures/maxwellianView.png}
    % \caption{Principle of the Maxwellian view~\autocite{Lin2017}.}
    % \label{subfig:SLM-Maxwellian-view}
    % \label{fig:RPDboth}
\end{figure}
\footnotetext{\url{https://www.qdlaser.com/en/applications/eyewear/}}

In 1980, the \gls{SLO} was developed by Webb et al.~\autocite{Webb1980}. This
device was used to provide retina images using a scanning light beam. This first
prototype of retinal scanning device was then iteratively improved along the
years to include a modulation of the light beam to eventually project a whole
image in the retina. As a result, the light beam stimulates the retina like any
ordinary light ray seen by the eye, allowing the device to act as a virtual
display. 

Retinal scanning technology relies on the Maxwellian view,
cf. Figure~\ref{subfig:SLM-Maxwellian-view}. This specific configuration of an
optical display forces all the light beams from the visual source to converge at
the center of the pupil. This point is the center of the optical system of the
eye and the light beams are supposed thin enough not to be affected by the eye
lens. Thus, the thin light beam directly hits the retina and forms a very small
and sharp point of light.

Enforcing Maxwellian view on an optical display can be achieved in several ways.
The two main ones are: (1) using a \gls{SLM} to generate a grid of thin parallel
beams that converge toward the eye pupil using a lens system and (2) using a \gls{MEMS} scanning
mirror to reflect a laser beam toward a reflecting free-form mirror which lastly
focuses the light in the eye pupil (see Figure~\ref{subfig:SLM-Maxwellian-view}\&b.). 
%(see~\autoref{subfig:MEMS-Maxwellian-view}). 
%
While the first approach is mainly used for rendering only virtual
images~\autocite{Dewald2016}, the second one is more suitable for \gls{AR}
purposes since the free-form mirror can be semi-transparent to make both real
and virtual elements visible. 
%
Several other Maxwellian-based configurations and more devices layout have also
been proposed in the literature and, for more details, the reader is referred to
Lin et al.'s survey~\autocite{Lin2017}. 

Thanks to their design based on the principle of Maxwellian view, \glspl{RPD}
are able to render an image that is always sharp, regardless of the focal
accommodation of the eye. In addition, recent technical improvements related to
\gls{MEMS} mirrors allow the emergence of more compact devices that could be
used for \gls{AR} headsets. These devices therefore have the potential to
replace the current \gls{OST} \glspl{HMD} if their specificities allow them a
better depth perception for users. 


\subsection{Handheld Devices} 
This category includes phones and tablets, as well as some more unusual devices
like autosteroscopic tablets\footnote{Autosteroscopic displays have the ability
to provide stereoscopic images without needing addition tools such as specific
glasses. The reader is referred to the survey from \textcite{Holliman2011} for a
complete review of autosteroscopic techniques.}. Those devices have the
characteristic to use a camera to capture the video stream of the real
environment and then display it on a screen; that is to say from a displaced
point of view. This specific paradigm has been detailed in
\autocite{Kruijff2010} but can also be compared with some studies conducted on
pictures and even paintings. This particular effect appends the more common
issue of the limited \gls{FoV} for such devices. The tracking part of this tools
are mostly image-based. Thus, in the past decade, new tracking techniques have
been developed, and computational power of handheld devices has
increased~\autocite{Dey2012}. This technology being mature enough to provide
proper \gls{AR}, more experiments could be conducted with these devices.
However, since they have to be carried by hand, the interactions with the
virtual content remain limited to indirect interactions. 

\subsection{Distant displays}
We choose here to regroup all those devices in one category but we can
distinguish different types of devices.

\begin{figure}[tbp]
    \centering
	\includegraphics[height=4.5cm]{devices/cave.png}
	\includegraphics[height=4.5cm]{devices/mosar.png}
	\includegraphics[height=3cm]{devices/proj-plopski.png}
	\caption[Examples of distant displays]{Examples of distant
    displays: the Immersia CAVE-like display from Inria Rennes-Bretagne
    Atlantique and IRISA (top left), MoSART (Mobile Spatial Augmented Reality for 3D Interaction With Tangible Objects), from \textcite{Cortes2018} (top right), VST/OST replication with projectors, from \textcite{Plopski2016} (bottom).}
	\label{fig:VR-HMD}
\end{figure}

\paragraph{Projectors and screens for \gls{VR}} This category includes all the
devices based on a fixed plane to provide virtual images. This encompasses
wide or surrounding displays such as immersive screens or CAVEs
\autocite{Cruz-Neira1993}, but also smaller displays such as workbench displays.
These devices render stereoscopic images on planar surfaces and the point of view
of the observer is usually tracked to provide a coherent rendering. One of the
key advantages of these technologies is the ability to immerse the user in a
virtual word while keeping the possibility to see its own body. 

\paragraph{Projectors for \gls{AR}} Two types of projectors can be distinguished
here, characterized by their uses. Indeed, projectors can be used to do
\gls{SAR}~\autocite{Bimber2005} which directly projects virtual elements onto a
physical real scene. If the projected image is not stereoscopic, the image is
not dependent on the point of view and allows several people to see and interact
at the same time with the same augmentation. 

Projectors can also be used to display images on semi-transparent glasses. This
case is similar to the screen condition described for \gls{VR} but allows to see
the real scene superimposed with the augmentations. 

Some researchers have also used projectors to mimic \gls{OST} and \gls{VST}
conditions~\autocite{Plopski2016}. 


\section{Mixed reality and perception, limitations and challenges}

Due to all technical specificities of \gls{MR} displays cited previously, the
perception of mixed environments can be different from the perception of reality,
and even different according to the device used. In this part, we will focus
more in depth on some issues specific to \gls{MR}. One is related to the
technical limitation of the devices and the functioning of the human perceptive
system, the \acrfull{VAC}, while the other issue is related to the display
capacity of \gls{MR}, which goes beyond the usual rendering of objects: the
X-ray vision. 

\subsection{The vergence-accommodation conflict}

\begin{figure}[tbp]
	\centering
	\includegraphics[height=6cm]{vac.pdf}
	\caption[The Vergence-Accommodation conflict]{The Vergence-Accommodation conflict: a) natural vision versus b) screen based-vision.}
	\label{fig:VAC}
\end{figure}

As detailed in~\autoref{part:def-cues}, accommodation and vergence are two
important cues for depth perception~\autocite{Azuma1997}. Under normal
circumstances, these cues are coherent: they both provide the same depth
information when looking at a specific distant object. The human vision system
is familiar with this coherence and acts accordingly. When the eyes converge at
a certain distance, they accommodate simultaneously to see the image sharp at
this distance~\autocite{Fincham1957,Martens1959} (see~\autoref{fig:VAC}.a). 

In \gls{AR} \gls{OST} headsets as for stereoscopic screens, the distance of
vergence and the distance of accommodation are different as shown
in~\autoref{fig:VAC}.b. To perceive the image sharp, the eyes have to focus at
a fixed distance (at the screen position for screen-based devices, and at the
focal distance of the lenses used for \gls{OST} \glspl{HMD}). However, they have
to converge at another distance, which corresponds to the intended distance of
the virtual object. This phenomenon is known as the
\gls{VAC}~\autocite{Julesz1971,Hoffman2008} 

This effect has been extensively studied in real, virtual and augmented reality
and is known to be responsible for inducing visual fatigue and in a more general
way to hinder visual performance~\autocite{Hoffman2008}. However, the \gls{VAC}
may be even more impacting in~\gls{AR} \gls{OST} devices since an augmented
environment contains both real objects, which provide coherent depth cues, and
virtual objects, which do not.

\subsection{X-ray vision} \label{subsec:xRay-vision}

\begin{figure}[tbp]
    \centering
	\includegraphics[height=3.5cm]{xray/hole.png}
	\includegraphics[height=3.5cm]{xray/edge-avery.png}
	\includegraphics[height=3.5cm]{xray/saliency-sandor.png}
	\caption[Examples of visualization techniques using x-ray vision]{Examples of visualization techniques using x-ray vision: virtual hole, from \textcite{State1994} (left), edge-based representation, from \textcite{Avery2009} (center), and saliency-based representation, from \textcite{Sandor2010} (right).}
\end{figure}

Showing to the user occluded information that would not be visible in the real
world is called ``x-ray vision''~\autocite{Feiner1995}. This possibility is
specific to \gls{AR} applications. Different visualization techniques have been
experimented to provide an accurate representation of this unusual vision.
Moreover, this ability has been used to many applications in different fields
such as medical, architectural, inspection or military applications. \ep{ref}

Some of the first applications that used this so-called x-ray vision were providing a
symbolic representation of the occluded objects, while other ones displayed a
``virtual hole'' in the real object~\autocite{State1994}. Subsequent studies
experimented other representations which provide a more realistic view of the
occluded part such as edge-based~\autocite{Avery2009},
saliency-based~\autocite{Sandor2010} or melted visualization~\autocite{Dey2010}.
In their review, \textcite{Dey2014} advocate that these representations for
occluded objects have to be considered as a suite of visualizations in order to
complement each other in different situations. 

However, the usual depth cue of occlusion is absent from this representation
which deeply impact the ability of observers to perceive distances. To
understand this kind of representation, the human perceptual system has to
interpret both the relation between the occluded object with its surroundings
and the relation with the viewer's point of view. Moreover, the occlusion depth
cue is stated as one of the most important, in particular to order objects along
the depth axis (see \autoref{part:def-cues}). Breaking the coherence with the
other cues might induce a major impact on depth perception. 

