
\subsection{Usual perception evaluation techniques}

% \fa{gap is big between this two sections I wonder if this makes more sense if you just talk about this in the context of distance perception.}
As detailed previously, perception is a complex and mostly internal process. 
As such, it is difficult to define what ``measuring'' means for such a process. 
For the entire internal part, physical models can often be established. Translations into a functional model of the structure of certain areas of the brain are also beginning to emerge. 
However, since the core of perception lies in the creation of perceptual experiences, it remains very difficult to measure it precisely. 
In addition, its variability (inter-personal but also for a single subject) makes the measurement of perception quite difficult. 
The methods used take advantage of considering the perception mechanism as a whole and primarily evaluate the final outcome of this process.

Therefore, several techniques can be described: \jmn{manque de refs}\al{+1}
% TODO add ref
\begin{itemize}
    \item \textbf{Thresholds} are the simplest element to evaluate since the
    expected answer is binary. People are asked either to compare two stimuli or
    to assess the presence or absence of a stimulus to evaluate the smallest
    noticeable stimulus. Methods used to assess these thresholds are usually
    referred as ``classical psychophysical methods''
    \autocite{fechner1966elements}. 
    \item \textbf{Magnitude estimation} consists in asking a person to evaluate
    one object or any stimulus criteria on an arbitrary scale. Given a standard
    reference, they are asked to evaluate a given stimulus. This can be
    performed with an arbitrary scale but also with more standard scales, such
    as the metric system. This estimation allows to go beyond the dichotomy of
    threshold evaluation and enables a continuous measurement of the perception
    of specific features. 
    \item \textbf{Recognition testing} more specifically evaluates the
    perception at the recognition state. Subjects are asked to recognize certain
    elements, with or without guidance. This allows on the one hand to test some
    basic functions in the case of treatment of pathologies but also, for
    healthy subjects, to evaluate the performance and accuracy of recognition,
    for example by limiting the available features or in a limited time. 
    \item \textbf{Reaction time} is another metric that can be used to evaluate perception. This is rather a roundabout form of metric but allows for example to evaluate the performance of recognition or attention. 
    \item \textbf{Phenomenological report}. Only qualitative, this form of metric consists of asking subjects to describe what they perceive. Although this does not provide precise measures of perceptual elements, it does allow more complex elements to be highlighted and then analyzed. Again, the internal nature of perception must be emphasized. Asking observers to express their perception in their own words allows to raise effects that could have been overlooked. 
    \item \textbf{Physical tasks or judgments}. This last evaluation method takes advantage that action is part of the perceptual process to use it as a metric. Reaching, navigating, pointing, driving, etc. are all actions that can be used as metrics to assess the perceptions behind them. 
\end{itemize}

After this general presentation of the main categories of evaluation techniques,
we will now consider the particular case of the evaluation of the perception of
distances. 

\subsection{Distance perception evaluation techniques}
When it comes to perception a major problem usually arises: the perceived distance of an object by an observer is a subjective cognitive data which cannot directly be measured. 
Whichever is the chosen reporting technique, the \textit{distance judgement} given by the observer induces but not perfectly represents the perceived distance.

However, many studies suggested various techniques to report judged distance. 
In this section, we will focus in particular on techniques that measure perceived distances and that can be applied in \gls{MR}. 
Other techniques exist, for example to measure more globally visual biases such as alignment or angular deviations (used for example to answer the questions mentioned in the previous paragraph) but they will not be presented here. 
\textcite{Loomis2003} presented a review of most of the distance evaluation techniques, and recently \textcite{Swan2015} provided a more in depth analysis of some of these for \gls{AR}.  

Perceived distance evaluation techniques can be defined in several groups:
verbal report, open-loop action based tasks, perceptual matching and indirect
techniques. \fa{what about providing a table of the works using each
method?}\ep{pk pas mais ca ne sera pas exhaustif, i.e. ca ne sera que des
exemples, pas toutes les études qui utilise tel ou tel technique, à faire dans
le RW pour AR}

\begin{figure}[tbp]
    \centering
    \subfloat[Matching and reaching task from \textcite{Swan2015}.]{
       \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{reportingTechniques/matchingSwan.png}
    }
    \hspace{0.01\textwidth}
    \subfloat[Gap affordance estimation from \textcite{Pointon2018}.]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{reportingTechniques/gapAffordance-pointon.png}
	}
    \hspace{0.01\textwidth}
    \subfloat[Matching task from \textcite{Diaz2017}.]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{reportingTechniques/matchingDiaz.png}
	}
    \hspace{0.005\textwidth}
    \subfloat[Triangulation by walking task from \textcite{Thompson2011}.]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{reportingTechniques/triangulatedWalking-thompson.png}
	}
    \hspace{0.005\textwidth}
    \subfloat[Bisection task from \textcite{Swan2017}.]{
        \includegraphics[height=0.3\textheight, width=0.45\textwidth, keepaspectratio]{reportingTechniques/bisection-swan2017.png}
	}
    \hspace{0.01\textwidth}
    \subfloat[Grasping task from \textcite{Al-Kalbani2019}.]{
        \includegraphics[height=0.3\textheight, width=0.37\textwidth, keepaspectratio]{reportingTechniques/grasping-alkalbani.png}
	}
    \hspace{0.01\textwidth}
    \subfloat[Exocentric pointing task from \textcite{Koenderink2008}.]{
        \includegraphics[height=0.3\textheight, width=0.53\textwidth, keepaspectratio]{reportingTechniques/exoPoint-koenderink.png}
	}
	\caption{Different distance evaluation techniques.}
\end{figure}

\textbf{Verbal report} is the more common technique to evaluate perceived distance. 
Observers have to verbally estimate the distance to an object in
whatever unit they are familiar with~\autocite{Foley1977,Gibson1954,Gibson1955,
Gogel1976,Gogel1981,Gogel1973,Mershon1977}, or using multiples of a given
referent\jmn{pas compris} (called \textit{magnitude estimation} in psychophysical
studies)~\autocite{Baird1967,Galanter1973,Teghtsoonian1970,Teghtsoonian1970a,
Teghtsoonian1970b,Teghtsoonian1978}. 
This technique has been widely used in real environments (up to 9km, \cite{DaSilva1985}) but indicates systematically compressed perceived distances %TODO ref
and can be biased by cognitive knowledge~\autocite{Loomis2008}.
Using the same technique but with an indirect metric, observers can also verbally estimated the size of familiar objects~\autocite{Loomis2003} which makes it possible to deduce the distance they estimate to be from the object. 
However, this method does not provide an absolute measure of estimated distance because the relationship between perceived size and estimated distance is not accurate for human perception. 


\textbf{Open-loop action based tasks. }
Every technique that use an observer's action to indicate a distance falls into
this category. Most of the time, when these techniques are used, the observer
looks at a stimulus and then the stimulus disappears and they must recreate the
distance they perceived without seeing the original object. 
This category includes:
\begin{itemize}
	\item \textit{Visually directed walking} where, after seeing an object, subjects must walk to the location of the object~\autocite{Creem-Regehr2005,Knapp2004a,Loomis2003,Messing2005,Willemsen2004,Wu2004,Rieser1995}. This technique has been widely used in many environments and is reported to be accurate up to 20 m in real environments~\autocite{Loomis2003}, with little systematic bias \autocite{Waller2008} and improving performance with training~\autocite{Mohler2006,Richardson2005}. 
    \item \textit{Imagined visually directed walking}, where participants estimate how long it would take to walk to targets~\autocite{Plumert2005}. 
    \item \textit{Triangulation by walking}, where subjects start walking in an
    oblique direction from the direction of a previously viewed target. Then, on
    the experimenter signal, they turn and take several steps towards where they
    perceived the previously viewed target to
    be~\autocite{Loomis2003,Thompson2004,Willemsen2004}. 
	\item \textit{Open-loop pointing tasks}, where participants indicate
	distance with a finger or manipulate a slider that is hidden from their
	view~\autocite{Mon-Williams2000, Swan2015}. 
	This technique can also be used for measuring derivative visual space's indicators such as orientation~\autocite{Koenderink2000}. 
    \item \textit{Throwing}, where participants have to throw (blindly or not) an object toward the target previously seen~\autocite{Thomson1983,Sahm2005,Peer2016,Peer2017,Sahm2005}. 
\end{itemize}

\textbf{Perceptual matching} procedures consist in closed-loop action-based tasks~\autocite{Bingham2001,Prablanc1979}. 
Participants are asked to align a cursor with the target. 
The distinctive aspect of these techniques is that the participant can compare the position of the target and the cursor at any time. 
Thus, some authors point out that the nature of the task can be changed from an evaluation task of a perceived distance to simply minimizing the disparity between the target and cursor. 
Then, these techniques would fail to measure \textit{absolute distance perception}~\autocite{Bingham1998,Prablanc1979}. 
However, this technique is still widely used to compare different targets and cursors, for example in \gls{AR}~\autocite{Ellis1998,McCandless2000,Wu2004}. 

\textbf{Indirect techniques} include all techniques that do not directly measure distance by asking the user to recreate it but use a derived metric. 
The verbal estimation of the size of objects mentioned earlier may also arguably fall into this category. 

This category also includes:
\begin{itemize}
	\item \textit{Forced-choice tasks.} Participants are asked to choose a position among several \autocite{Livingston2003}, or to perform an ordinal task by choosing for example the closest one among several stimuli \autocite{Rolland1995}. 
	\item \textit{``Can I pass?'' techniques} which rely on \gls{affordance} and the body perception of participants. 
	They are asked to look toward two poles and to assess if they can pass through without touching them~\autocite{Geuss2010,Walker2012}. 
	Similar techniques can be found with other tasks relying on \gls{affordance}, such as crossing a gap~\autocite{Wu2019,Pointon2018}. 
	\item \textit{Bisection.} Participants are asked to point or place an object at the midpoint between two objects. This technique is close to the matching technique presented before but does not suffer from the alignment bias underlined. 
	However, distances given by bisected intervals are reported as compressed by some studies \autocite{Gilinsky1951}, accurate by others \autocite{Bodenheimer2007,DaSilva1985,Purdy1955,Rieser1990}, or even expanded and environment dependent \autocite{Vishton1995}. 
\end{itemize}

Overall, some general outlines can be drawn from all these different techniques.
First, one of the major issues of reporting distances and also one of the theory
to explain the variability of distance reporting is that distance judgments are
influenced by the amount of energy observers anticipate to expend in order to
reach the item to which distances are judged~\autocite{Decety1989,
Proffitt2006}. This so called effort-based theory helps to explain some
perception results and enriches attempts to create models of perception.
However, like all the theories on perception, it is still neither fully
understood nor fully accepted. Then, the methods used to obtain distance
judgments have been reported to have a major effect on the accuracy of those
judgments~\autocite{Thomson1983, Elliott1987, Rieser1990, Witmer1998a}. As such,
since the techniques affect the results, both by their nature and by the fact
that they need an action from the observers, perception studies cannot be
carried out without this multitude of techniques. A combination of these
techniques could therefore be needed in order to compensate for the biases they
may cause. 

\gm{conclusion?}