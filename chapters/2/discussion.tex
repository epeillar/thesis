The results of our studies show a novel bias in distance perception in virtual
environments: the perception of egocentric distances in \gls{VR} is anisotropic.
Virtual objects located on the sides are perceived to be farther away than
objects in front. The first experiment showed that an object placed on the side
is perceived 5\% farther than an object placed in front. The work of
\textcite{Koenderink2002}, conducted in a real environment, underlines that the
\textit{forward} direction is specific and leads to a deformation of the visual
space. 
%
Thus, with our second experiment we aimed to characterize the observed bias by
assessing distance estimation while varying the exploration angle. The results
of the second experiment show that the overestimation effect on the side
increases along with the angle. The greater the angle between the \textit{in
front} and the \textit{on the side} stimuli, the more the observer tends to
overestimate the distance of the stimuli on the side. Combined with the results
of the first experiment, this effect seems to reach a plateau at 45°. 


However, \gls{VR} suffers from specific limitations which are not present in
real environments. One of the most important regarding vision is the relative
size of the \gls{FoV} compared with ordinary real vision, and potential image
distortions introduced by \glspl{HMD}' optical systems. We conducted a third
experiment to evaluate the potential impact of \glspl{HMD}' optical systems
specificities on the observed bias. We forced participants to look at specific
directions in order to ensure that the visual stimulus was always displayed and
viewed at the center of the \gls{HMD}'s \gls{FoV}. With such an experimental
protocol, the previously discussed issues were minimized. In this case, the
results did not differ from those obtained in the first experiment. As a
conclusion, we can state that the ocular particularity of side vision in
\gls{VR} does not seem to be responsible for the observed anisotropy of distance
perception. Then, the effect observed in the two first experiments could not be
due to different exploration behaviours or hardware limitations, we hypothesized
that the motor action of the head was a good candidate to explain this bias.


The increase in overestimation as related to the head rotation amplitude seems
to verify the effort-based theory of depth perception. Indeed, according to this
theory, distance judgments seem to be influenced by the amount of energy
observers anticipate to expend~\autocite{Decety1989, Proffitt2006}. In our
experiments, the \textit{in front} object is directly seen by the observer,
while the \textit{on the side} object requires an action (head movement) to be
seen. This additional movement from the resting position could modify distance
perception and lead to an overestimation. This assumption requires subsequent
studies to be evaluated. In particular, additional movements or forces could be
added during the experiment to assess the effort-linked dimension of space
perception under these conditions. 

Finally, the last experiment we conducted, aimed at evaluating the impact of the
\gls{VE} on the observed bias. Indeed, in the three first experiments the
\gls{VE} mainly provided binocular depth cues which could have made the task
more difficult, facilitating the appearance of the perceptual bias. In contrast,
in our fourth experiment, the visual stimulus provided additional graphical
depth cues, lighting, and increased contrast. Here again, results show that,
even with more salient depth cues, the same perceptual bias is observed even if
it is slightly weaker than in the first experiment. Since the environment
provided more depth cues, the ordering task was probably easier. Participants
were thus more likely to choose the correct sphere, decreasing a bit the
appearance of the perceptual bias. 

To analyze this study with respect to those conducted in real environments, our
series of experiments can be compared with the frontoparallel curvature
experiments of \textcite{Koenderink2002}. In this paper, they determined that
frontoparallels are perceived as being curved, with the concave side facing the
observer. In their experiments, when participants had to place objects to a
given distance to define a frontoparallel, the objects in front of them were
placed further away than the correct position, which implies that they were
perceived as being closer. This observation is coherent with our results,
suggesting that the bias we observe is not dependent on the visual stimulus but
on the distance perception assessment (e.g. motor actions) and on the
directionality of the virtual environment. 
However, since experimental conditions were not the same in their experiments and in ours, other studies, at different distances, are needed to further characterize this bias. 

Last, the observed bias could be considered to draw new guidelines for the
design of \gls{VR} applications. When a \gls{VR} application deals with realism,
in architectural applications for example, it could be used to better match the
space perception of the observer, and/or to provide an improved experience. The
\gls{VE} could also be oriented in order to be perceived longer in some
direction. Moreover, \gls{VR} applications which rely on highly oriented
\glspl{VE}, such as industrial accessibility tests, driving simulations, or some
specific immersive games, could also consider this new perceptual bias to better
specify their scenarios and their 3D contents.
