
\fa{I wonder if Discussion is the right title for this chapter, to me it feels more like conclusion and future works. After an introduction you summarize the different chapters and then present the future works. Also, the final conclusion chapter would be great as a long term perspective and research project.}

\section{Space perception and Mixed Reality - Current state of knowledge and research challenges}

\fa{Personally, this content seems more an introduction to me than a discussion. To check with others.}

% perception theory history
The principles underlying human perception have been debated for a long
time in the scientific community. The initial theory of the internal
representation of space in the form of a mind map \autocite{kaplan1973cognitive}
was later challenged by a topological approach of space
\autocite{kuipers1983cognitive}, rejecting the idea of a metric representation
in favor of a semantic representation of objects in relation to each other.
However, these theories have also proved to be too restrictive. The most
recent works agree on a mixed, more ecological conceptualization of perception.
They focus on describing how different types of information, resulting from the
interaction of the individual with his environment, can contribute to the
construction of a mental representation \autocite{tversky1993cognitive}. 

% perceptual process
This approach is the one underlined by \textcite{Goldstein2017} in their model
of perception and in particular of the perceptual process. This process can be
divided into four stages: (1) stimuli (distal \& proximal), (2) physiological
response (receptors \& neuronal processing), (3) perception \& recognition, (4)
action. This process can be seen as a loop, integrating external elements,
processing them and then acting on them, but it should also be noted that all
these steps are interconnected and can influence each other. Finally, knowledge
plays a major role in this process, influencing each step through learning and
experience. 

% challenges of measurement
Perception is therefore a mental state, an internal representation, which can
potentially provoke an action. However, the internal nature of this process is
already a major challenge. On the one hand, action is not necessarily a faithful
translation of this internal representation and there may be a difference
between what is perceived and what is presented to someone. On the other hand,
the action externalizing the perception can influence this same perception and
thus bias its measurement. 

% depth cues & distance fields
The theory of distance perception is based on depth cues
\autocite{Howard2002,Goldstein2017}. These cues allow the human visual system to
determine distances by combining them. There are three main categories of depth
cues: (1) oculomotor cues, provided by the oculomotor system of the eyes, (2)
pictorial cues, that rely on 2D information, and (3) motion cues, based on the
movements of the objects or of the point of view. 
Thus, the weight of these depends on the distance between the object and the
observer which defines three spaces, also characterized by the actions that
can be carried out inside \autocite{Cutting2003}: (1) the personal space, within
1.5 meters, within arm's reach, (2) the action space, up to 30 meters, where the
observer can quickly interact with an object, (3) the vista space, beyond 30
meters, where interaction is difficult or even impossible. 

% \acrfull{MR} => new perceptual challenges
The perception of distances in real environments is already a major challenge,
in particular when trying to model it. Differences between real space and
perceived space can be observed, and current models of perception are still
struggling to transcribe these biases. Considering the case of environments
containing both real and virtual elements, i.e. mixed environments, things become
even more difficult. The virtual elements are displayed differently from the
real ones, so they will cause different perception biases. But in addition,
perception being a global process, these ``simple'' biases will also interfere
with the perception of all other elements, even real ones. 

% many techniques and rendering, challenge to find general trends
In \acrfull{MR}, the devices and rendering techniques are numerous.
\acrfullpl{HMD} are the most widely used devices. They can be non-see through
for \gls{VR} headsets or \acrfull{OST} or \acrfull{VST} for \gls{AR} headsets.
However, other devices can be used, such as handheld devices, using a camera and
a screen, or projection devices. Each of these types of devices has its
own characteristics and therefore produces different effects on perception.
Since virtual elements are simulated elements, techniques for creating virtual
images and rendering choices can also vary, which will also impact perception.
Overall, finding general trends within all these potential effects is a major
challenge. 

% classification made: depth cues, intrinsic, rendering specific,
% environment&object related
Thus, to categorize perceptual biases in \gls{MR}, their origins can be
divided into four groups: (1) depth cues related biases, (2) intrinsic biases,
(3) rendering induced biases and (4) environment and object specific biases.
However, if this categorization allows a for systematic analysis of perception
biases in \gls{MR}, it also shows many configurations that have not yet been
studied from a perceptual point of view. Moreover, while this classification
allows for an individual analysis of the factors influencing perception in
\gls{MR}, much work remains to be done to be able to model \gls{MR} perception
in a systematic way, if indeed this can be done at all. 
\gm{tu mets les biais de connaissances prélalables dans quelle catégorie ?
je verrais bien aussi une allusion aux différences inter-individuelles et le
lien à ces groupes quand c'est possible}\jmn{+1 + est-ce que les limites du
matériel rentrent qq part ou un peu partout ? + pas de device parfait (fov, etc}

\section{Contributions of this Thesis}

In this thesis, we aimed at characterizing some perceptual biases that occur
in \acrlong{MR}. In particular, we focused on the study of several factors
inducing distance misperception in mixed environments. To do so, we have studied
different factors, of various nature and origin. We investigated factors
intrinsic to observers, factors related to display devices and rendering
techniques, as well as variations in referential from egocentric to exocentric
distance estimates. \ep{ajouter qqchose ?}\gm{représentation graphique ?}\jmn{interaction?}

\medskip
In \textsc{Part I}, we have detailed the different elements of the state of the
art related to perception and Mixed Reality. 

% chap 1 : perception
In \textbf{Chapter 1} we presented an overview of the previous literature on
perception and more especially on distance perception. First, we presented the
process which transforms sensations into perception: the perceptual process.
We also underlined the interconnections between the different stages of this
process as well as the impact of knowledge. Then, we highlighted the challenges
that the perceptual process has to face and the difficulty in measuring
perception. Subsequently, we presented the specific case of distance perception,
describing in detail the depth cues theory as well as more global theories on
distance perception. Finally, we presented different reporting techniques
that can be used to evaluate distance perception and the issues and possible
biases due to these techniques.  

% chap 2 : mr
In \textbf{Chapter 2} we detailed the theoretical background of \acrlong{VR} and
\acrlong{AR}. Starting from the definitions and basic presentation of the mixed
reality continuum, we highlighted the fundamental differences between actual
reality and mixed reality. Then, we reviewed all the devices that allow to
perform \gls{MR}, highlighting the different categories of devices and their
characteristics in order to evaluate their impact on perception. Finally, we
underlined two major challenges for \gls{MR}: the vergence-accommodation
conflict and the so-called ``x-ray vision''.

% chap 3 : perception in AR
In \textbf{Chapter 3} we presented an overview of the key studies in distance
perception related to \gls{AR}. For each element influencing the perception of
distances, we detailed the impact of \gls{AR} on this factor and thus the
induced perceptual bias. For this, we have distinguished four types of biases:  
(1) depth cues related biases, (2) intrinsic biases, (3) rendering induced
biases and (4) environment and object specific biases. Finally, we also
presented the existing studies in distance perception for exocentric distances.
This review highlights some major factors impacting the perception of distance
in \gls{AR}. 

\medskip
In \textsc{Part II}, we presented four series of experiments conducted to elicit
several factors of perceptual biases in \acrlong{MR}. 

% chap 4 : intrinsic bias, anisotropy of distance perception
In \textbf{Chapter 4} we presented a series of four user experiments aiming at
highlighting an intrinsic perceptual bias in \gls{VR}. This series of
experiments showed that the spatial perception of observers is anisotropic in
\gls{VR}. The first experiment showed that virtual objects placed in front
of the participant are perceived closer than objects placed on the side. The
second experiment confirmed this effect and provided evidence that it
increases with the viewing angle (the more the object is placed on the side and
the farther it is perceived). Finally, two control experiments were conducted
and showed that this effect is neither affected by the position of the object in
the field of view nor by the amount of depth cues provided by the virtual
environment.  

% chap 5 : transfer of perceptual bias, from ego to exo ?
In \textbf{Chapter 5} we studied the effect of \gls{AR} on exocentric distance
perception. We reported a user experiment aiming at evaluating distance
perception between two objects lying in a frontoparallel plane in the
medium-field perceptual space. Four conditions were tested in our study,
involving two virtual objects, two real objects and the two symmetrical
conditions with one real object and one virtual object. Results showed that two
virtual objects were perceived closer one to another than two real objects, i.e.
the exocentric distance was underestimated in \gls{AR} compared to reality.
Moreover, a non symmetrical behavior was found for the mixed condition. 

% chap 6 : impact of depth cues
In \textbf{Chapter 6} we reported two series of experiments, aimed at studying
the impact of depth cues on distance perception in \gls{AR}. 

% sec 6.1 : shadows shape and light position
The first two experiments focused on the effect of shadows on depth
perception. Both experiments showed an increase in distance perception
performance when shadows were provided. The first experiment showed that the
shape of the shadows has no significant impact on distance perception. Simple
circular shadows had the same results as more realistic shadows, with hard or
soft edges. The second experiment showed that the position of lighting sources
is an important factor for virtual shadows since a misalignment of virtual and
real lights sources (and then inaccurate shadows position) can lead to a
misperception of egocentric distances.

% sec 6.2 : accommodation & RPD
The last experiment reported in this chapter aimed at evaluating the effect of
the fixed accommodation distance in \acrfull{OST} \acrfullpl{HMD}. In this experiment,
distance perception of the participants was evaluated using a usual \gls{OST}
\glspl{HMD} with a fixed focal distance and a \acrfull{RPD}. \glspl{RPD} do not
have focal distance thanks to their laser projection. This study showed that
while the participants overestimated distances in \gls{AR} compared to reality
with the classical \gls{OST} \gls{HMD}, their performance significantly
increased when using the \gls{RPD}. Besides, the task with the \gls{RPD} was
reported with the same level of difficulty and no difference in precision. 

\medskip
In \textsc{Part III}, we presented the perspectives and prospects related to perception in \acrlong{MR}. 

% chap 7 : prospect, effect of interaction
In \textbf{Chapter 7} we presented a proposal of user experiment to evaluate the
effect of interaction techniques on distance perception. First, we presented the
motivations and the theoretical background on action-related perception
theories, as well a the proposed research hypotheses of this study. Then, we
presented the detailed protocol of this study and mentioned some of the
expected results and potential prospects. 


\section{Limitations and Future Works}
% \fa{Talk about the limitations here seems tricky, as the reader might have forgoten the details for each experiment. I wonder if this should be higher level and mainly focused on the future works. I personally like the "short-term" perspectives.}
% \al{utiliser les types de biais pour les titres}


In this section, we first present the different experimental variations
that could be made on each of the experiments detailed in this thesis in order
to study more parameters. Then, we will present the issues and new questions
related to perception raised specifically by each of our experiments. Finally,
we present more global perspectives on the potential contribution of the
study of perception to other major subjects in \acrlong{MR}. 

\subsection{Experimental variations}

% vary hmd
\paragraph*{Varying devices.}
All our studies were conducted with one or two specific \glspl{HMD}. Each device
has its particular (characteristics, display technology, \gls{FoV},
accommodation distance or the technical possibilities for rendering). These
characteristics constrained our experimental designs and setups, such as the
distances of the virtual targets or the choices for rendering. Then, new
experiments could be conducted using other \gls{AR} devices: \gls{OST} or
\gls{VST} \glspl{HMD}, or screens, handheld or \gls{SAR} devices. 

% change answer protocol
\paragraph*{Different evaluation tasks.}
The protocol used in each of our experiment was selected to ensure the
evaluation of a specific element. For example, a \gls{2AFC} protocol was
preferred to highlight potential biases, while an open-loop protocol was used to
evaluate the extent of the distance estimation biases, without dwelling with the
comparison and calibration effects of the closed-loop protocol. However, as
stated in the first chapters of this thesis, the evaluation protocol can
influence perception. For example, in many distance perception studies in
\gls{AR}, an open-loop action based answering protocol might be partly
responsible for the overall underestimation of the perceived distances. Indeed,
reaching tasks are generally used to report perceived distances, as they are not
biased by any visual feedback, but suffer from proprioception-based biases since
no correction is provided \autocite{Swan2015}. Thus, new experiments using
different protocols could be carried out, to compensate for the effects of one
specific answering protocol. It is also possible to focus on some specific
conditions using \gls{2AFC} reporting protocols as they are more accurate when
assessing the existence of potential biases.

% Our protocol focused on absolute distance perception. However, our results --
% and in particular the specific case of the \textit{real-virtual} and
% \textit{virtual-real} layouts -- were difficult to interpret. New experiments,
% more focused on these specific cases are needed to assess the similarities of
% distance perception under those conditions. In particular, \gls{2AFC}
% reporting protocols could be used since they are known to be more accurate
% when assessing the existence of potential biases. \gm{ref}\jmn{oui}

% change answering protocol
% As in many distance perception studies in \gls{AR}, the answering protocol
% might be partly responsible for the overall underestimation of the perceived
% distances. \jmn{pas ultra clair que c'est le résultat de l'étude} Reaching
% tasks are generally used to report perceived distances as they are not biased
% by any visual feedback but suffer from proprioception-based biases since no
% correction is provided. Other tasks with no visual feedback such as verbal
% report or indirect pointing could be used to confirm the particularity of
% distance perception with \glspl{RPD}. In this case, absolute perception is not
% important, as the bias is also affecting real answers. Here, the key point is
% the difference in perception between real and virtual. 

% vary distance
\paragraph*{Considering other distances.}
As stated by the literature, space perception varies according to the distance
field considered~\autocite{Howard2002}. Each of our experiment focused on one
specific distance field, or even one distance, but space perception could also
be studied for other distance fields. The weight of the depth cues varying
according to distance, one could thus see an evolution of the effects studied
previously in interaction with distance. More broadly, all variations in
positions and orientations of the targets could be considered and studied.
Moreover, we have proposed to move the targets, but we could also consider
moving the observer and therefore his point of view, since it was shown in
\autoref{sec:anisotropicVR} that this also has an effect on perception.

% vary shape, size and rendering & shading
\paragraph{Changing size, shape or rendering.}
Moreover, the visual aspect of the considered objects could also influence the
results. In our studies, we focused on very salient objects, with known sizes
and simple shapes. 
% However, the shape and size of the presented objects could vary. Two different
% objects can also be presented to the observers to evaluate the influence of
% the relative size of the objects in \gls{AR}. 
Moreover, the rendering and shading parameters could be changed, in
particular to observe the impact of the rendering quality on distance
perception. Since the evaluation of distance in real or mixed environments has
been reported to be dependent on the shape, size and rendering of the virtual
object~\autocite{Phillips2009, Eggleston1996, Renner2013}, these parameters
should also be evaluated to extend the experiments presented in this thesis.
Augmented objects -- with mixed real and virtual parts -- could even be used. In
such configurations, the effect of other usual anchoring cues such as projected
shadows or other \acrlongpl{AA} should also be evaluated. 


% other distances
% Other characteristics of the experiment could be modified to further explore
% the use of \glspl{RPD} in visual perception. The near distance field was
% chosen in this experiment to highlight the specificities of accommodation in
% \glspl{RPD} but other distance fields could be considered. vary shape, size
% and rendering & shading Since the evaluation of distance in real or mixed
% environments has been reported to be dependent on the shape, size and
% rendering of the virtual object~\autocite{Phillips2009, Eggleston1996,
% Renner2013}, these parameters should also be evaluated for \glspl{RPD}. 


% target movement (ecological) + different frame setup (no chin rest)
\paragraph*{More ecological configurations allowing head movements.}
In some of our experiments, the setup was completely fixed, from the targets to
the head of the participant itself (fixed to the chin-rest). However, this
configuration is not an ecologically valid condition. Usually, the observer is
free to move his head and small movements of the head could help to evaluate
distances. The observed objects could also be moving, providing more depth cues
such as motion parallax. We choose to use simple environments to prevent for
biases but a richer and more ecological environment could provide a specific
blending of many more cues which could interact with each other. The evaluation
of the specific interaction of different devices in such an ecological situation
is a very exciting open question. 


%HMD optics
% The influence of the position of the stimuli in the visual field does not
% appear as a factor which leads to the observed anisotropy. However, many
% studies \jmn{refs} underline the impact of the limited \gls{FoV} on depth
% underestimation in \gls{VR}. \gm{refs} In our study, the same \gls{HMD} was
% used during the whole series of experiments. Future studies, using other
% optics, \glspl{HMD} or other immersive devices, could thus be conducted to
% provide additional views on this anisotropy phenomenon. 

%Only assess the relative depth perception
% Our series of experiments evaluated the anisotropy of distance perception
% considering different experimental conditions. However, given the considered
% 2AFC experimental protocol, our results concern the ``relative'' distance
% perception of objects. Future studies could then also focus on ``absolute''
% egocentric distance perception of virtual objects. 
% %


% The reporting technique can also vary, using an open-loop judgment task (e.g.
% blind walking, pointing, virtual throwing, etc.), that may give better results
% for egocentric distance estimations but is more complex to use in a controlled
% experiment. 

\bigskip
Beyond the distance, it is also possible to evaluate the \textbf{intrinsic
characteristics} of objects, such as size, weight, etc. Most of these
characteristics are related to distance perception but could also show specific
biases in \gls{MR}. It is therefore interesting to study them specifically, by
studying their evolution according to their shadows, appearance, etc. 

\paragraph*{Refer to real.}
Finally, we conducted most of our experiments using real estimates as a
baseline. This provided some comparable data, and could helped to isolate
factors leading to egocentric distance errors in \gls{MR}. However, we were not
able to do this for all the experiments (especially the one evaluating the
effect of shadows and the one on anisotropic perception), mainly for technical
reasons (precisely placing real targets is usually harder to ensure compared to
virtual ones). It would therefore be relevant to introduce a comparison with
reality for all perception experiments in \gls{MR}. This could allow both to
have a reference to evaluate perception in virtual but also to bring out
explanations and extensions to the general theories of perception, not specific
to \gls{MR}. 


\subsection{Perspectives on conducted experiments}

\subsubsection{An intrinsic bias: the visual space orientation?}

% VE is oriented
In \autoref{sec:anisotropicVR}, our setup for evaluating the anisotropy of space
perception, involved a participant seated on a chair, which is very common to
many consumer applications. Interestingly, it naturally induces a strong
orientation of the \gls{VE}. This specific direction is here enforced by the
position of the participant, seated and holding a joystick in front of him. The
entire body is constrained toward one specific direction. Some other \glspl{VE}
or user positions/orientations could therefore be considered in future work,
without any strong \textit{forward} direction, and involving other settings.

Moreover, the present experiment was designed to provide an evaluation of the
anisotropy of distance perception in ecological conditions. It did not assess
the global shape of the visual space, which is of course more complex. The task
we chose is specific and cannot cover all the various numerical models proposed
for the visual space. Our observed variation in distance perception according to
the head direction already seems in favor of the non-Euclidean shape of the
visual space. But future work could also extend our results, to confront them
with the various models proposed regarding visual space perception. As an
example, our experiments considered the depth perception of a virtual object
always placed at \textasciitilde 3 meters. The evolution of the anisotropy
according to the distance considered, the vertical position or other
configurations in space seem other worthwhile topics to consider in order to
better assess the shape of the visual space in \acrlong{VR}. 
% Indeed, as mentioned in the literature, the space perception varies with the
% distance field considered, both in real~\autocite{Koenderink2000} and
% virtual~\autocite{Bruder2015c} environments. 
%
Thus, more studies in real, virtual and even augmented environments are needed
to provide a better characterization of this phenomenon and to identify the
specific influence of \gls{VR} or \gls{AR} compared with reality. 
%\gm{et l'AR, qui se situe entre les deux}


%\subsection{Exocentric distance estimation}
\subsubsection{Multiple objects imply multiple biases?}

% vary layout (add occluders, objects in between)
When studying exocentric distance perception in \autoref{sec:exocentric}, we
choose to limit the layout of the objects to two objects placed in the
frontoparallel plane. This layout could be extended by adding other objects to
evaluate the impact of a virtual or a real object placed between the two
original ones, or by moving the relative positions of the objects. 

The objects' layout could also be modified by rotating the plane where the objects
are placed. Indeed, we could not only consider extending our study on exocentric
distance perception in the sagittal plane but also on all the possible angles
between those two (frontoparallel and sagittal) layouts. This is motivated by
previous evidence (see \cite{Loomis1992}) showing that physically equal
distances in a sagittal plane on the ground are perceptually underestimated
relative to those in a frontoparallel plane, even under full-cue conditions. We
expect that placing virtual objects outside of the frontoparallel plane
would lead to different exocentric distance estimations.

On the other hand, new targets could be organized in several layouts: two
side-by-side or one behind the other, or even in a regular grid or some other
random pattern. Those layouts would allow to evaluate the potential impact of
references, occlusion, ordering or relative distances. While the literature
provides an extensive corpus around egocentric distance perception in \gls{AR},
the specificities of multi-targets distance evaluation and their interaction
still require more evaluation. 
%
Indeed, when comparing two objects' depths, occlusions are a very strong cue.
When using \gls{OST} devices, this cue is not available for mixed conditions
(with real and virtual objects) and the \gls{VAC} would prevent the observer to
adjust the accommodation in a natural way. With \glspl{RPD}, the increment of
vergence from one object to another could be associated with an increment in
depth of the accommodation without inducing a conflict. Retinal projection could
then be an even more accurate technology for depth perception when it comes to
inter-objects estimations. As such, the effect of occluders or objects in other
planes could be a very important question to tackle in \gls{AR}, especially if
one also considers the type of device used. 


% use matching task
Finally, using a second object, as for matching tasks for example, could have a
major impact on perception. Those tasks are different from the tasks used in our
studies since they provide visual feedback during distance estimation. If the
participant has to align the designated target with another object, say, a
cursor, the nature of this cursor is fundamental. Indeed, if the cursor has the
same type (real or virtual) as the target, then the potential bias would be the
same and the alignment should be accurate. However, in the case where the target
and the cursor have a different type, the depth cues provided by the two kinds
of rendering would interfere, leading to potential unusual biases. Moreover, we
already underlined the major influence of the accommodation cue in the specific
case of \glspl{HMD}. Here, the different nature of the target and the cursor
might be even more impacting for distance perception since accommodation is a
very strong cue for ordering~\autocite{Mon-Williams2000}.

Overall, presenting more than one object to the observer could lead to many
different layouts and then have many different effects on distance perception.
Many studies are still lacking to assess all the potential effects of these
combinations of factors.  


\subsubsection{Shadows influence}
% With the recent advances in commercial \gls{AR} \glspl{HMD} technologies, we
% believe it is of tremendous importance to address potential AR-related
% perceptual biases. By exploring the influence of virtual objects shadows and
% lighting coherence in \gls{OST} \gls{AR}, our results contribute to better
% understand the complex phenomenon of how people perceive egocentric distances
% with virtual objects.

% \gm{tu pourrais ajouter que la techno même des casques VST ne te permet pas de retrancher de la luminosité lors du rendu. exception faire d'expériences passées de Yuta}

It has to be noted that all \gls{MR} technologies do not provide the same level of
shadows' rendering. In particular, \gls{OST} \glspl{HMD} and projector-based
\gls{AR} devices could not remove light to display ``dark'' shadows. They have to
make the global environment darker, or use local adjustment to create this
effect of shadows. Some displays using a subtractive rendering technique exist
but they are ready yet to do \gls{AR} \autocite{Itoh2019}. Displaying realistic
shadows is therefore already a challenge in itself and much remains to be done
to allows \gls{AR} devices to display shadows properly. 

In \autoref{sec:shadows}, we provided a study of the impact of shadows on depth
perception, focussing on the shape and position of the shadows. However, in
order to further study this phenomenon, we envision different future leads
focusing on the nature of the shadows. We could study the influence of shadows'
rendering in \gls{OST} \gls{AR}, e.g. by increasing the brightness of the rest
of the scene to make them more noticeable). We could also compare the impact of
the use of ``drop shadows'', i.e. shadows placed immediately below the objects,
vs. realistic shadows on distance perception accuracy. 

Results from such experiments could lead to the definition of guidelines for
proper use of shadows when designing \gls{AR} applications which may become more
and more accessible in a near future.


\subsubsection{Retinal Projection Displays}
% \gm{le titre est questionnable dans la mesure où tu parles in fine plus de méthode de mesure que de RPD...}

% reproduce blur + use eyetracking
\glspl{RPD} are lacking accommodation cues. However, the blur induced by the
adjustment of the focal distance could be reproduced with dedicated
rendering~\autocite{Dunn2018,Cholewiak2017}.\jmn{d'ailleurs, Someya devait pas
bosser la dessus?} 
Then, using blur to help depth estimation in \gls{RPD} would be interesting to study. However, it would require very precise and fast eye tracking as well as an evaluation of the actual accommodation state of the eye. 
For the time being, those technologies are still subject to ongoing research but are improving quickly and their integration with \glspl{RPD} could provide very accurate depth rendering in \gls{AR}. 

% RPD = sharp even for vision impaired ppl => test on those ppl to improve vision
One last specificity of \glspl{RPD} is the use of those displays for visually impaired people. 
Since the image is projected directly on the retina with a laser beam, the resulting image is always sharp, even for people suffering from eye accommodation disabilities such as myopia or hypermetropia. 
Then, virtual images rendered with \glspl{RPD} are even sharper than real images. 
As such, while improving the quality of the image seen, it is still unclear if
retinal projected rendering would significantly enhance distance perception for
visually impaired people or if this kind of rendering, drastically different
from their usual viewing will disturb their perception of distance. 

% \gm{ tu pourrais extraire certains morceaux dans des paragraphes spécifiques qui seraient expérience-agnostique}

\subsection{Long term perspectives}
% \al{long term perspectives: new biases, new protocols, new devices?} 
% \al{le "peillard's test"}

I would like to insist on the plurality and the always different nature of the
observed biases in \gls{MR}: intrinsic biases, biases linked to technologies but
also biases induced by rendering and visualization choices. Thus, it seems
extremely complex, if not almost impossible, to characterize all the elements
that can impact the perceptual process. However, I have to recall the purpose of
perception: action. Indeed, all the biases highlighted above lead to changes in
the use of \gls{MR} technologies. An unsuitable focal length could lead to a
change in the user's navigation strategy, a misperception of the shape of an
object, impacting its handling, etc. 

Therefore, I consider perception as an entry point that is at
once original, relevant and promising for many issues already raised in
\gls{MR}. Thus, I want to highlight three research axes: Perception
and Representation, Perception and Interaction, and Perception and Cooperation. 

\subsubsection{Perception and Interaction}
Interaction is a major issue in \gls{MR} and in particular in \gls{AR}. The real
and virtual nature of objects and environments require a constant adaptation of
interaction techniques to adapt to these changes. The stakes are first
technical: it is impossible for a virtual visualization system to move a real
element and, conversely, a synchronous movement of a virtual object via a real
object remains difficult to achieve. However, the stakes of interaction are also
related to the users performing these interactions. I want to emphasize here the
major importance of interaction in the perceptual process. Action is often
considered as the finality of the perceptual process. Humans evaluate the size
of an obstacle to overcome it, or the shape of an object to grasp it. However,
the proximity between perception and action leads to an interdependence between
these two processes. Thus, we have presented in our prospects a study aimed at
modifying or even improving perception in \acrlong{AR} by using different
interaction techniques. To go further, I propose to study more broadly the
effects of rendering and of the environment (i.e. what is seen) on actions,
interactions and affordances (i.e. what is done) in mixed environments. This
could lead to the proposal of efficient interaction techniques more adapted to
the uses and users. These techniques could also benefit from the use of other
sensory modalities, other than vision, to get closer to the multisensory nature
of perception. 

\subsubsection{Perception and Representation}
Representation and display, whether of realistic elements or data, present major
challenges for \gls{MR}. How to accurately represent realistic objects with
limited technology? How to take advantage of the limits of perception to propose
ecologically valid representations? How to represent complex, potentially
multidimensional and non-spatial digital data in a 3D virtual environment? These
questions are already open research questions, but I wish to bring a different,
perception-based interrogation to them. We have extensively presented the
problems of perceptual biases in \gls{MR} in this thesis. Within the framework
of the study of representations (of data or realistic elements), it seems
important to me to consider these representation techniques as levers to
compensate perceptual biases. Ultimately, this approach would allow to improve
data visualization and representation in general by relying on the capabilities
and limits of the human perceptual system and to propose representations adapted
to the context and to the observers. By taking advantage of the limits of
perception in \acrlong{VR}, it would then be possible to build and represent
virtual environments more adapted to human perception. 

\subsubsection{Perception and Cooperation}
Finally, I would like to consider the study of collaboration in mixed
environments. This subject is already significantly studied within the
scientific community and the new avenues opened up from the perspective of
perception seem particularly exciting to me. I have already detailed some major
elements and steps of the perceptual process, but broadening them in the context
of multi-user augmented applications is a research question that is both
original and stimulating. Can perceptual corrections and improvements
observed for one user be transferred to other users cooperating in the same
environment? Do several users in the same environment develop similar perceptual
biases? Do interaction techniques used in mixed environments vary when
several users are present? In a mixed environment cooperation involving body
representation of participants, how do the issues of avatars and embodiment
extend in the case of \acrlong{AR}? More generally, the cooperation of
several agents in a mixed environment raises questions about the types and
characteristics of cues and references used. In my opinion, these questions
present major challenges for \gls{MR}, both in its theoretical approach and in
its practical implementation.  

\bigskip

In conclusion, I would like to present my work on the question of the study of
perception in \acrlong{MR} not as a theoretical goal but as a powerful tool for
questioning in a way that is both original and embodied the multiple problems
posed by mixed environments. 