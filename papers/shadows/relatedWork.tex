Over the past decades, depth perception has been widely studied in VR
\protect\hyperlink{_bookmark23}{{[}2,}
\protect\hyperlink{_bookmark24}{3,} \protect\hyperlink{_bookmark25}{4,}
\protect\hyperlink{_bookmark26}{5{]},} and while some research has been
carried out in AR (e.g. \protect\hyperlink{_bookmark27}{{[}6{]})}, this
issue was, comparatively to VR, less investigated.

Underestimation of egocentric distances in VR has been a puzzling
problem for the community for several years
\protect\hyperlink{_bookmark28}{{[}7,}
\protect\hyperlink{_bookmark29}{8{]}.} However, with the development of
recent commodity level VR hardware, in particular new HMDs, this issues
has been reinvestigated and previous findings about distance
underestimation do not seem to be as strong as they used to. Indeed,
Kelly et. al {[}9{]} found that while distance underestimation in the
HTC Vive is lower than when using older displays, it still
remains\protect\hyperlink{_bookmark30}{.} More recently, Buck et.al
{[}10{]}, examined eight different VR HMDs to evaluate how much users
underestimate distance. Their results show that new commodity-level HMDs
provide more accurate estimates of distance than prior generations of
HMDs\protect\hyperlink{_bookmark31}{,} but this question remains open
since their findings are slightly different from Kelly et al.
\protect\hyperlink{_bookmark30}{{[}9{]}'s} ones regarding the particular
case of the HTC Vive.

Some studies focus on the factors which influence distance perception in
VR. Li et al. \protect\hyperlink{_bookmark32}{{[}11{]}} found that field
of view (FoV) plays an important role in distance perception in VR,
their experiment result shows that a smaller FoV caused a more serious
underestimation of distance judgment in VR. Jones et al.
\protect\hyperlink{_bookmark33}{{[}12{]}} found that applying constant
white light in an observer's far periphery lead to more accurate
distance judgments using blind walking in a Virtual Environment (VE). In
addition, it was also shown the users overestimate distances in the real
world after they interacted with a VE for 18 trials
\protect\hyperlink{_bookmark34}{{[}13{]}.} This result was confirmed by
Jones et al. \protect\hyperlink{_bookmark35}{{[}14{]}'s research.} Their
experiment reveals that participants tend to walk roughly 5\% farther
when performing blind walking in the real world after accomplishing the
tasks in VR. The phenomenon was explained as the mismatched physical and
visual movement in a synthetic environment can cause errors on returning
to a natural environment.

As for research on AR, results of different studies are not uniform.
Rolland et al. {[}7{]} found that the depth of virtual objects lying
within 0.8-1.2m was overestimated in
AR\protect\hyperlink{_bookmark28}{.} While, in their follow-up
experiment {[}15{]}, results show that there is no consistent bias in
depth judgment\protect\hyperlink{_bookmark36}{.} However, Swan et al.
\protect\hyperlink{_bookmark37}{{[}16{]} found} an interesting
phenomenon: there is a switch in users' bias from underestimation to
overestimation in AR OST-HMDs when distance to virtual objects is ≥ 23m.
Livingston et al. {[}17{]} found that there is an overall pattern of
underestimation in indoor environments, whereas participants
overestimated depth in outdoor environments through OST-HMDs in
AR\protect\hyperlink{_bookmark38}{.} Jones et al. {[}18{]} conducted an
experiment to measure egocentric depth perception in VR, AR as well as
in reality, and they found that participants underestimate depth in the
VR condition while no underestimation was found in the AR
condition\protect\hyperlink{_bookmark39}{.}

In order to improve depth accuracy in VR and AR, numerous experiments
were conducted to identify factors causing depth perception errors. It
was found that one of the most fundamental factors is the breakdown
between convergence and accommodation in VR and AR display systems
\protect\hyperlink{_bookmark40}{{[}19{]}.} When users view virtual
objects through a binocular AR display, their eyes accommodate on the 2D
screen where the augmented information is in focus, but at the same
time, the eyes need to converge on the rendered depth of the real object
to fuse the stereoscopic pair \protect\hyperlink{_bookmark41}{{[}20{]}.}
When viewing real objects, accommodation distance and convergence depth
are the same. Swan et al. \protect\hyperlink{_bookmark42}{{[}21{]}}
found that observers can accurately match the distance of a real target,
but when viewing an AR target, they overestimated the distance by 0.5cm
to 4.0cm. An explanation for this phenomenon is that collimating optics
cause eyes' vergence angle to rotate outward by a constant angular
amount. Singh et al.'s experiment
\protect\hyperlink{_bookmark43}{{[}22{]}} confirmed these results and
showed that the bias of vergence angle bias is constant within distances
of 33.3cm to 50cm when the subjects match virtual targets in AR. They
also found that presenting AR objects at the AR display's focal distance
results in more accurate depth matches. Obviously, it is hard to
eliminate the influence of this accommodation/convergence issue since it
is caused by most AR displays hardware.

Some researchers also focused on studying factors related to the display
of virtual objects, which are easier to control, compared with the
hardware of AR display systems. For example, Singh et al. {[}23{]} found
that the presence of a highly salient occluding surface has a
complicated effect that disrupts the linear relation between depth
judging distance and distance errors within near field distance in
OST-HMDs\protect\hyperlink{_bookmark44}{.} Swan et al. {[}16{]} designed
an experiment to study the effects of upper versus lower visual field
locations, the number of repetitions of the task as well as X-Ray vision
(i.e. displaying virtual objects in AR environment when located behind
opaque surfaces) in OST-HMDs\protect\hyperlink{_bookmark37}{.} Results
show that distance was overestimated by 10 percent in the upper visual
field condition compared with the lower visual field condition within
the first three repetitions. An equation was established to show that
observers had an additional 8cm-error in the non-X-Ray condition
compared with the X-Ray condition. They also found that the accuracy of
distance judgment increases along with the number of repetitions.
Messing and Durgin {[}24{]} found that lowering the horizon line in VR
HMDs lead to an overestimation in distance
judgments\protect\hyperlink{_bookmark45}{.} Also known to all, the size
of objects is a depth cue in that distant objects looks smaller than
close objects. Some papers studied the depth cue of relative size of
objects. Diaz et al. {[}6{]} found a significant linear relationship
between distance error and objects' size which reveals that
participants' depth perception improved as virtual objects got bigger in
AR\protect\hyperlink{_bookmark27}{.}

In addition to the above factors, shadow is another important depth cue
for the human's visual system. Mamassian et al. {[}25{]} found that cast
shadows are helpful to the recovery of spatial arrangement, especially
when the shadow is in motion\protect\hyperlink{_bookmark46}{.} Hubona et
al. {[}26{]} designed an experiment to verify the effectiveness of
computer rendered shadows, and results show that the use of object
shadows enhances the accuracy of object positioning in a VR-like
environment\protect\hyperlink{_bookmark47}{.} As for shadows in AR, they
allow for a connection between virtual objects and the real world as
well as help virtual objects look more realistic. Diaz et al. {[}6{]}
conducted an experiment to prove that the presence of shadows can
improve the accuracy of depth perception in
AR\protect\hyperlink{_bookmark27}{.} They used two types of shadows in
their research: (i) ``drop shadows'' and (ii) ``ray-traced'' cast
shadows. Neither solutions correspond to realistic shadows since ``drop
shadows'' correspond to a vertical projection of the virtual object on
the floor while the so-called ``ray-traced'' cast shadows correspond to
the display of a elliptic shadow at a position computed by a ray-cast
between the light source and the floor plane. Hence, the shape of the
shadows were not realistic in that they were always elliptic and do not
change according to the real shape of the objects. Shadows' opacity was
modified according to a relatively simple approximation based on the
distance between lights sources and the furthest point of the shadow.
Moreover, it should be noted that the use of ``drop shadows'' changes
the experimental task since participants do not evaluate egocentric
distance to a virtual object but rather to the virtual shadow.

In fact, with the development of the rendering capabilities in AR, it is
now possible to use different kinds of real-time shadows in AR scenes.
Indeed, shadows can range from being very realistic to very crude. Of
course, the more realistic they look like, the more expansive they are
to compute. This can still be an issue for AR HMDs which do not always
benefit from a lot of computing power.

Based on those observations, a question naturally arises regarding the
proper choice of AR shadows under different conditions. Do different
kinds of shadows influence differently human perception in AR? To the
best of our knowledge, research carried out to answer this question is
very limited. One of the related work is that of Sugano et al. {[}27{]},
who studied the influence of lighting and shadows in AR in a subjective
task where subjects had to evaluate the ``virtual presence of virtual
objects in an AR scene''. Results of subjective questionnaires showed
that virtual objects' shadows enhance their ``virtual object presence''
by providing a strong connection between them and the real world. In the
same paper, the authors used dropped shadows in spatial arrangement
tasks (depth ordering and height ordering virtual objects).

Related to the notion of virtual shadows in AR, the alignment of the
simulated lighting model in AR with the physical lighting in the real
world is also an issue that needs to be considered. Indeed, it can prove
difficult to control and perform this alignment in a very precise way
especially in uncontrolled real environments. Does a misalignment can
influence human's perception in AR? Here again, to the best of our
knowledge, there has been limited research on lightings misalignment
between AR scenes and the real world. Still using the same AR VST-HMD
setup, Sugano et al. {[}27{]} compared two lights conditions in a
spatial arrangement task: (i) the virtual light position is same as the
real light; (ii) the virtual light positioned ``15 degrees higher from
the real one''\protect\hyperlink{_bookmark48}{,} and results show no
difference between the two conditions. It should be noted that in both
conditions authors used ``dropped shadows'' and not realistic ones.
Moreover, in condition (ii) the difference between both lights seems
limited and is difficult to interpret. Finally, the experimental setup
consisted of an AR scene displayed in near-field (on a table in front of
the participant) and the real light used is a table lamp positioned next
to the participant.

According to these observations, questions about the influence of
realistic virtual shadows as well as alignment between real and virtual
lights on distance perception in OST AR remain.