
This second experiment aims at assessing hypotheses \textbf{H1}: ``Participants
underestimate egocentric distances in \gls{OST} \gls{AR}'' and \textbf{H4}:
``Lighting misalignment between the real and the virtual environments impact
negatively depth perception in \gls{OST} \gls{AR}''. Similar to experiment 1,
details about the apparatus and the task have been given in
\autoref{subsec:shadowsTask}.

\subsubsection{Experimental conditions}

In order to select the type of virtual shadow used in this experiment, we used
results from the first experiment. Since we did not want to induce a bias in
depth perception, we chose to rely on a realistic type of shadow, namely the
\emph{hard-edge} shadow. Thus, all of the trials in this experiment were
performed with the shadow ON condition and using the \emph{hard-edge shadow}
setting, see \autoref{fig:shadowsConditions} middle right.

We selected three virtual lights' positions, see \autoref{fig:shadows2-lights}:
(i) \emph{L0m}: coherent lighting conditions where the positions of virtual
lights coincide with the real lights' positions, (ii) \emph{L+3m}: virtual
lights were positioned 3 meters behind the real lights' positions along the
Z-axis, and (iii) \emph{L-3m}: virtual lights were positioned 3 meters before
the real lights' positions along the Z-axis.

In addition, we also conducted the experiment under four different target
distances, as in the first experiment, namely 6m, 7.5m, 9m and 10.5m. All the
other settings of the trials as well as the task are similar with those of the
first experiment. A total of 756 trials (twenty-one observers × three lights'
positions × four distances three repetitions) were conducted in this experiment.

\begin{figure}[tb]
  \centering
  \includegraphics[width=.4\textwidth]{media/image9.jpeg}
  \caption[Exp. 3.2 - Lighting conditions]{Experiment 2. Lighting conditions.}
  \label{fig:shadows2-lights}
\end{figure}


\subsubsection{Participants}

A total of 21 participants (all different from experiment 1) passed the
screening step and took part in this experiment (11 males and 10 females).
Participants were between 21 and 29 years of age (\emph{M} = 25\emph{.}9\emph{,
SD} = 3\emph{.}19). Results of the subjective questionnaires showed that 11
participants had no previous experience with \gls{VR} or \gls{AR}, 9
participants had little experience (1-5 times) with VR or AR, and 1 participant
had extensive experience (\emph{\textgreater{}} 5 times) with \gls{VR} or
\gls{AR}. Each participant completed a total of 36 trials.


\subsubsection{Data Analysis and Results}

Like experiment 1, the dependent variable of our statistical analysis is the
relative error between the cube and the floor reference target.

From the initial dataset, 3 participants were removed: 2 because of a standard
deviation more than 5 times higher than the standard deviation of the other
participants and one because of an abnormal mean (being more than three times
the standard deviation smaller than the global mean). Here again the analysis
was performed using IBM SPSS Statistics 24.0 with a 0\emph{.}05 significance
level.

The final data of different light position levels and distance levels was
processed by a Shapiro-Wilk test respectively. The results show that some levels
of data were not distributed normally: L0 (\emph{W} = 0\emph{.}978\emph{, p} =
0\emph{.}002), 7.5m (\emph{W} = 0\emph{.}978\emph{, p} = 0\emph{.}011). Thus,
non-parametric tests were applied to all of the data.

As in the previous experiment, we first computed a Kruskal-Wallis H Test.
Results showed a significant effect of distance (\emph{H} = 22\emph{.}610;
\emph{p \textless{}} 0\emph{.}001) and lights' position (\emph{H} =
15\emph{.}961; \emph{p \textless{}} 0\emph{.}001).

We thus followed with post-hoc tests: all pairwise comparisons with adjusted
\emph{p} values using the Bonferroni correction for multiple tests. Results
found significant differences between (i) 6m and 10.5m (\emph{Z} =
\emph{-}4\emph{.}277\emph{, p \textless{}} 0\emph{.}001), (ii) 7.5m and 10.5m
(\emph{Z} = \emph{-}3\emph{.}873\emph{, p} = 0\emph{.}001). Other distances
groups were not significantly different.

As for lights' positions: pairwise comparisons revealed significant difference
between groups (i) L+3m and L-3m (\emph{W} = \emph{-}3\emph{.}947\emph{, p
\textless{}} 0\emph{.}001) and (ii) L+3m and L0m (\emph{W} =
\emph{-}2\emph{.}511\emph{, p} = 0\emph{.}036). Note that no significant
difference was found between the L0m and L-3m groups.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image10.png}
  \caption[Exp. 3.2 - Relative errors per distance]{Experiment 2. Relative errors in distance judgments, averaged for all participants grouped per distance condition.}
  \label{fig:shadows-exp2-results1}
\end{figure}

\autoref{fig:shadows-exp2-results1} presents the mean relative errors for all
participants under four distance conditions. It can be observed that the mean
relative error increases along with the distance of the virtual object.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{media/image11.png}
  \caption[Exp. 3.2 - Relative errors per lighting displacement]{Experiment 2. Relative errors in distance judgments, averaged for all participants grouped per lighting placement.}
  \label{fig:shadows-exp2-results2}
\end{figure}

Regarding hypothesis \textbf{H1}, our results are not conclusive. There seems to
be a tendency toward a slight underestimation (\emph{mean} = 0\emph{.}046m), but
the high variance levels do not allow us to conclude on a clear distance
underestimation in \gls{OST} \gls{AR} no matter the lighting misalignment
condition.

Fig. 11 presents the mean relative errors for all participants in distance
judgments grouped per lighting placement. The mean relative error has the
highest value under the condition L+3m. The condition L0m has a mean relative
error slightly higher than that compared with the condition L-3m, however, the
condition L0m also corresponds to a smaller standard deviation.

In the same way, the completion time for each trial was analyzed. Results of the
Shapiro-Wilk test show that data were not distributed normally: L-3m (\emph{W} =
0\emph{.}902\emph{, p} \textless{} 0\emph{.}001), L0m (\emph{W} =
0\emph{.}827\emph{, p} \textless{} 0\emph{.}001), L+3m (\emph{W} =
0\emph{.}879\emph{, p} \textless{} 0\emph{.}001), 6m (\emph{W} =
0\emph{.}838\emph{, p} \textless{} 0\emph{.}001), 7.5m (\emph{W} =
0\emph{.}795\emph{, p} \textless{} 0\emph{.}001), 9m (\emph{W} =
0\emph{.}874\emph{, p} \textless{} 0\emph{.}001), 10.5m (\emph{W} =
0\emph{.}909\emph{, p} \textless{} 0\emph{.}001). Thus, a Kruskal-Wallis H Test
was applied to all of the data. Results showed a significant effect of distance
(\emph{H} = 41\emph{.}523; \emph{p \textless{}} 0\emph{.}001), see Fig. 12, and
non-significant effect of lights' position (\emph{H} = 0.468; \emph{p =} 0.791),
see \autoref{fig:shadows-exp2-results4}.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image12.png}
  \caption[Exp. 3.2 - Completion time per distance]{Experiment 2. Completion time in distance judgments, averaged for all participants grouped per target distance}
  \label{fig:shadows-exp2-results3}
\end{figure}

As post-hoc tests for the effect of distance we computed all pairwise
comparisons with adjusted \emph{p} values using the Bonferroni correction for
multiple tests. Results found significant differences between (i) 6m and 9m
(\emph{Z} = \emph{-}3.902\emph{, p =} 0\emph{.}001), (ii) 6m and 10.5m (\emph{Z}
= \emph{-}5\emph{.}618\emph{, p} \textless{} 0\emph{.}001), (iii) 7.5m and 9m
(\emph{Z} = \emph{-}3\emph{.}091\emph{, p} = 0\emph{.}002), (iv) 7.5m and 10.5m
(\emph{Z} = \emph{-}4\emph{.}807\emph{, p} \textless{} 0\emph{.}001). Other
distances groups were not significantly different.

Then, the Mann-Whitney U Test was used to check the differences between
different levels of lights position, however no significant result was found.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image13.png}
  \caption[Exp. 3.2 - Completion time per lighting displacement]{Experiment 2. Completion time in distance judgments, averaged for all participants grouped per lighting placement.}
  \label{fig:shadows-exp2-results4}
\end{figure}

Similar to experiment 1, we computed the participants' individual data to
confirm the stability of our findings. As shown in
\autoref{fig:shadows-exp2-results4}, 8 subjects have positive relative errors, 4
subjects have negative relative errors, and 6 of them have both positive and
negative relative errors. In other words, the number of subjects who
underestimate distances in AR accounts for the largest proportion in experiment
2. This result is consistent with the general trend of the data.


\begin{figure}
  \centering
  \includegraphics[width=.9\textwidth]{media/image14.png}
  \caption[Exp. 3.2 - Individual results]{Experiment 2. Subjects individual relative errors.}
  \label{fig:shadows-exp2-results5}
\end{figure}

In order to reinforce the findings about the lights placements' effects on
distance relative errors, we computed for each subject the mean relative error
under different light placements (see \autoref{tab:shadows-results2-1}). As
mentioned above, results show that L+3m corresponds to a significant higher
relative error than L0m, while L-3m corresponds to smaller relative error than
L0m. From \autoref{tab:shadows-results2-1}, we can see that for 9 out of the 18
subjects, results were coherent with the general trend. In addition, three
subjects (Subject 3, Subject 13, and Subject 16) show the highest distance
judging accuracy under the condition L0m. A Shapiro-Wilk test was adopted to
assess data normality for relative error of these three subjects under different
lights placements. The results show that the data of Subject 3 and Subject 13
distributed normally, while the data of Subject 16 under L-3m condition does not
distributed normally: L-3m (\emph{W} =0.847 ; \emph{p =}0.034 ). A paired sample
t-test was applied to data of Subject 3 and Subject 13. The result shows that
there is no significant difference between L-3m and L0m for these two subjects.
Subject 13 exhibit a significant higher relative error under L+3m condition
compared to the L0m condition (Subject 13: t\textsubscript{11}=2.366, p=0.037).
In addition, A Mann-Whitney U was adopted to analyze relative errors of Subject
16. The result shows that L+3m exhibits significant higher relative errors than
L0m (Z=2.021, p=0.043). This means that most of the subjects' (12 out of 18)
individual data were coherent with the general result.


\begin{table}[]
\footnotesize
  \caption[Exp 3.2 - Individual mean relative errors by light position]{Individual mean relative errors under different lights placements.}
  \input{papers/shadows/data/table2-1}
  \label{tab:shadows-results2-1}
\end{table}

Moreover, we computed the mean relative error under different distances for each
subject (see \autoref{tab:shadows-results2-2}). Results show that 13 out of 18
subjects have the highest mean relative error under the distance of 10.5m. This
means most of the subjects' data coherent with the general trends.

\begin{table}[]
\footnotesize
  \caption[Exp 3.2 - Individual mean relative errors by distance]{Individual mean relative errors under different targets' distance.}
  \input{papers/shadows/data/table2-2}
  \label{tab:shadows-results2-2}
\end{table}

Similar to experiment 1, we also computed the individual mean completion time
for each subject. Since only distances add a significant effect, to further
analyze this effect, we computed the mean completion time of each subject for
each different targets' distance (see \autoref{tab:shadows-results2-3}). Results
show that 14 out of 18 subjects have longer mean completion times for distances
of 9m and 10.5m compared to 6m and 7.5m. This is also coherent with the global
statistical analysis.

\begin{table}[]
\footnotesize
  \caption[Exp 3.2 - Completion time]{Individual mean completion time in distance judgments under different targets' distances.}
  \input{papers/shadows/data/table2-3}
  \label{tab:shadows-results2-3}
\end{table}

\subsubsection{Discussion}

The present section is the first study to investigate the influence of the
lighting misalignment on distance perception quantitatively in \gls{OST}
\gls{AR}. As in the first experiment, we found a distance underestimation trend
in \gls{AR} \gls{OST}-\glspl{HMD}. However, high variance levels prevent us from
confirming it statistically.

As expected, we found a significant influence of lighting coherence's on
distance perception in \gls{OST} \gls{AR}. The condition L+3m has a
significantly higher distance judgment error than L0m. Although the mean
relative error of L-3m is slightly lower than L0m, we did not find any
significant difference between these two conditions. In addition, since L-3m
leads to a better distance estimation accuracy than L+3m, this could imply that
placing the virtual lights closer to the participants might be better than
placing them farther away. Individual data were also observed to confirm these
results, and we found that 12 out of the 18 subjects' individual data were
coherent with the general result.

Obviously, the direct consequence of changing virtual lights' locations is the
modification of the virtual shadows' positions. In condition L+3m, the virtual
lights were farther away from the participants and as a consequence virtual
shadows were closer to them. Since our participants wore an \gls{OST}-\gls{HMD},
this created a perceptual conflict. Indeed, in \gls{OST} \gls{AR}, participants
still see the real lights. Thus participants had to estimate distance of the
virtual cube based on the real lights' positions and the virtual shadows (that
were closer to them than they should have been). This could lead participants to
believe that objects were closer to them and thus to underestimate the
egocentric distance to the virtual cube. In other words, the lights' location
L+3m might have increased the degree of distance underestimation in \gls{OST}
\gls{AR}.

In a similar, but opposite way, condition L-3m should have lessened
underestimation of distance perception in \gls{OST} \gls{AR}. However, results
showed a non-significant lower relative error in condition L-3m compared to L0m
(the same relative error is significant between L-3m and L+3m). This means that
we did not notice a cancelling, let alone a reduction of egocentric distance
estimation in \gls{OST} \gls{AR} when moving the virtual lights closer to the
participant than in reality.

As for the distances' influence on the relative errors, we found some
significant results that distance perception at 10.5m is significantly worse
than the condition of 6m and 7.5m. We can also observe a global trend that the
mean relative error increases with the distance between the cube and
participants. This trend is consistent with the results of the first experiment.
Individual data for each subject (see \autoref{tab:shadows-results2-1}) show
that 13 out of 18 subjects have the highest mean relative error at 10.5m, which
is coherent with the global trend.

Moreover, our results reveal that trials' completion time is significantly
longer under farther targets' distances (9m and 10.5m) compared to nearer
targets' distances (6m and 7.5m) , while lighting placement has no significant
effect on completion time. Participants' individual data were also consistent
with these global trends.
