
Two experiments were designed to verify our hypotheses. Since both experiments
have very similar experimental procedures, tasks and apparatus, we decided to
regroup their description in the following.
  
\subsubsection{Apparatus}
  
Both experiments were conducted on a Microsoft HoloLens. It has a $30\deg \times
17.5\deg$ FoV with a resolution of $1268 \times 720 pixels$ per eye. It contains
an inertial measurement unit (IMU) and four sensors (a depth camera, a RGB
camera, a four-microphone array, and an ambient light sensor) used to map the 3D
environment in real-time. The HoloLens also supports voice input, basic gesture
recognition, and head tracking. In addition, the HoloLens displays are fixed at
an optical distance approximately 2.0m away from the user, which means that
users have to accommodate near 2.0m to maintain a clear image. In other words,
placing virtual augmentations around this distance can minimize the
vergence-accommodation conflict. We selected the HoloLens mainly due to the
quality and robustness of its tracking. In addition, a Bluetooth mouse was
adopted as an input mechanism for our task (see \autoref{subsec:shadowsTask}).
The AR scenes were built using Unity3D v2017.3.1f1 and the HoloLens
Toolkit\footnote{\url{https://github.com/Microsoft/MixedRealityToolkit-Unity/releases?after=2017.4.0.0-rc}}.
  

\subsubsection{Experimental Environment and Tasks}\label{subsec:shadowsTask}

\begin{figure}
  \centering
  \includegraphics[width=.9\textwidth]{media/image1.jpeg}
  \caption[Exp. 3.1-2 - Overview]{Figure 1: Top left: Top view of the position of the different targets
  and two real lights. Bottom left: View from the HoloLens. Right: Overall view
  of a participant in the corridor}
  \label{fig:shadows-overview}
\end{figure}
  
The experiments were conducted in a corridor which is 14.43m long, 1.74m wide
and 3.15m high. The corridor is an enclosed space and there were two lights on
the ceiling for lighting, each light consisting of four fluorescent tubes. A
virtual cube floated in the air at a height of 1.25m from the ground (see
\autoref{fig:shadows-overview}) and rotated at a constant speed of $12\deg /s$
to enhance participants' stereoscopic sensation. In order to facilitate the task
the cube is oriented such that one of its vertex points to the floor (see
\autoref{fig:shadows-overview}). Participants saw the virtual cube through the
HoloLens. Due to the HoloLens' limited \gls{FoV}, it was necessary to guarantee
that subjects had a low visual horizon so that they could see virtual shadows on
the floor. In order to do so, subjects were seated on a chair located on a
reference line at the end of the corridor (see \autoref{fig:shadows-overview}).
In addition, four white lines were drawn on the floor (at 6m, 7.5m, 9m and 10.5m
from the subjects position) to serve as references for four different targets
distances. Four A4 (210 × 297mm) pieces of paper with numbers from 1 to 4
printed on them served as a way to recognize the targets (named as Target 1,
Target 2, Target 3, Target 4 from nearest to furthest from the subjects
position), (see \autoref{fig:shadows-overview}). The two real lights were
located at 5m and 12.20m from the subjects' position and at 3.15m high.

Three main factors were considered when determining this range for our targets:
(i) the shadows of the virtual objects had to be visible by the participants,
(ii) virtual objects should be displayed at a distance allowing natural
interaction and (iii) virtual objects should be within a comfort zone of the
HoloLens.

Regarding (i), as shown in \autoref{fig:shadows-overview}, the two real lights
were located at 5m and 12.20m from the subjects' position at a height of 3.15m.
If the virtual cube was too close to the participants, its shadows could be
displayed behind them. On the other hand, if the virtual cube was too far away
from the participants, its shadow would be invisible (i.e. too far away and too
small). Preliminary tests allowed us to determine that the target distance range
of 6m to 10.5m could guarantee shadows of the virtual cube to be displayed on
the ground within a visible area. This range also corresponds to a common
distance range for human daily interaction which is highly suitable for most
\gls{AR} applications (cf. criterion (ii)). Finally, although the distance range
of 6m to 10.5m exceeds the optimal distance (1.25m to 5m) for placing virtual
objects in the HoloLens, this distance range is still within a relative comfort
zone for HoloLens, and will not be
uncomfortable\footnote{\url{https://docs.microsoft.com/en-us/windows/mixed-reality/comfort}}
for users (criterion (iii)).

Each experiment consisted of a fixed number of trials. Participants saw a
virtual cube floating in the air with or without a shadow. They could move the
cube forward and backward along the axis of the corridor by scrolling the wheel
of the Bluetooth mouse. The minimal step of the wheel induced a 5cm translation
of the cube. Participants had to match the lowest vertex of the cube with a
target line on the ground specified in each trial (see
\autoref{fig:shadows-overview} bottom left). Target lines were announced vocally
to participants directly using the HoloLens built-in speakers at the beginning
of each trial. Participants needed to confirm the target line to make sure they
picked the correct one. Participants confirmed the final location of the cube by
clicking on the mouse's wheel. We recorded the final distance of the cube for
each trial and loaded the next trial automatically. In order to guarantee that
participants understood the procedure of the experiment correctly, each of them
went through a pre-experiment practice session which included 10 trials of the
same task.
  

\subsubsection{Experimental Procedure}

Both experiments were within-subject studies and followed the same procedure.
Each experiment lasted for 20-25 minutes. The procedure of the experiment is
depicted in \autoref{fig:shadows-procedure} and consisted of five steps: (1)
Screening; (2) Introduction and subject informed consent letter; (3)
Calibration; (4) Practice session; (5) Formal experiment. Screening consisted of
making sure that participants had normal or corrected to normal vision as well
as normal color vision and normal stereoscopic vision. In order to do so, all
the participants performed visual function tests including the visual acuity
test, the stereo acuity test and the Ishihara test \autocite{Hardy1947}.

In addition, the subjects experience with \gls{VR} and \gls{AR} was also
recorded through a subjective questionnaire before the experiment. After that,
the experimenter introduced the experiment content to the subjects, without
mentioning the goals, and gave them the Subject Informed Consent Letter. This
letter tells subjects that the experiment design and procedure were in
accordance with the Declaration of Helsinki. Subjects were also informed that
they could leave the experiment at any time or if they felt uncomfortable
without the need of getting permission from the experimenter nor to justify
themselves. Finally, they signed the letter.

\begin{figure}
  \centering
  \includegraphics[width=.6\textwidth]{media/image2.jpeg}
  \caption[Exp. 3.1-2 - Experimental procedure]{Experimental procedure}
  \label{fig:shadows-procedure}
\end{figure}

Participants sat on a chair and were helped to put on the HoloLens. A
calibration procedure was carried out to guarantee that the trajectory of the
virtual cube matched the axis of the corridor. The calibration procedure is as
follows: participants saw three dots arranged in a line in the HoloLens' FoV.
They had to align the dots with the middle axis of the corridor and make sure
this line was perpendicular to the corridor's start line (see
\autoref{fig:shadows-overview}, top left). After the calibration, the practice
session, consisting of 10 trials and during which the experimenter explained the
task, loaded automatically. Once the practice session was over, the formal
experiment started. The order of the trials was randomized for each participant.
In both experiments, the task was identical to that of the practice session
which is to match the position of the virtual cube with a specified target on
the floor. At the beginning of each trial, a vocal announcement in the HoloLens
built-in speakers specified the target.

  
Objective measures were recorded: signed distance errors, absolute distance
errors and completion time. The signed distance error represents the distance
between the center of the final position of the virtual cube (aligned with the
lowest vertex) and the target along the Z-axis (i.e. the axis of the corridor).
A signed distance error $>0$ means that it is an egocentric underestimation
(i.e. the cube is behind the target line). A signed distance error $<0$ means
that it is an egocentric overestimation (i.e. the cube is in front of the target
line). The absolute distance error is the absolute value of the signed distance
error.
  
