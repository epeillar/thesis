
The main purpose of the first experiment is to verify: \textbf{H1}
``Participants underestimate egocentric distances in OST AR'', \textbf{H2}
``Virtual objects' shadows improve distance perception accuracy in OST AR'', and
\textbf{H3} ``Shadows with different realism levels have different influences on
distance perception in OST AR''.

A matching task under different conditions was designed to verify these
hypotheses. For more details on the task, please refer to
\autoref{subsec:shadowsTask}. Participants accomplished the tasks under the
conditions of presence or absence of virtual objects' shadows in the AR scene.
In addition, when virtual shadows were present, three different levels of shadow
realism were studied. Hypotheses \textbf{H1}, \textbf{H2} and \textbf{H3} were
then assessed by comparing distance errors under the aforementioned conditions.

\subsubsection{Experimental Conditions}

The conditions that we want to manipulate in this experiment were (i) the
presence (Shadow ON) or absence (Shadow OFF) of virtual objects' shadows in the
AR scene and the impact of the level of realism of virtual objects' shadows.
This second condition was decomposed into three sub-conditions, namely:

\begin{itemize}
\item
  ``Round'' shadows (see \autoref{fig:shadowsConditions} top right): they have
  an incorrect shape and have sharp crispy edges. In the following, we refer to
  them as \emph{round shadows}.
\item
  ``Hard-edge'' shadows (see \autoref{fig:shadowsConditions} middle right): they
  have a correct shape but have sharp crispy edges. In the following, we refer
  to them as \emph{hard-edge shadows}.
\item
  ``Soft-edge'' shadows (see \autoref{fig:shadowsConditions} bottom right): the
  more realistic shadows which shape is correct and which fade off at the edges
  to represent a penumbra area. In the following, we refer to them as
  \emph{soft-edge shadows}.
\end{itemize}

When the shadows are ON, we display two shadows (see
\autoref{fig:shadowsConditions}) on the ground in a coherent way: the ground
plane being detected during the HoloLens' calibration procedure; the virtual
shadows lie on a virtual plane that exactly corresponds to the real ground
plane.

To implement our shadows we rely on Unity3D's internal shadow implementation
using the ``hard shadows'' and ``soft shadows'' options as well as the ``Ultra''
quality settings for our \emph{hard-edge} and \emph{soft-edge} shadows
respectively. While Unity's ``hard shadows'' are generated using the \emph{very
high} resolution parameter, the ``soft shadows'' are produced in \emph{low
resolution}. This creates a blurrier shadow and increases the difference between
the two shadow renderings. Regarding the ``round shadows'' we used the same
settings as for the ``soft-edge shadows'' for which we used a sphere to cast the
shadows instead of the cube. The sphere has a diameter equals to that of the
cube's side. Participants still see the virtual cube, the sphere was only used
as the geometry to cast shadows. More information about Unity3D's shadows is
available
online\footnote{\url{https://docs.unity3d.com/Manual/ShadowOverview.html}}.

We therefore had 4 shadows conditions: (i) Shadows OFF: no shadows were
displayed in the AR scene; (ii) ``Round'' Shadows ON; (iii) ``Hard-Edge''
Shadows ON; (iv) ``Soft-Edge'' Shadows ON.

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{media/image3.jpeg}
  \caption[Exp. 3.1 - Shadows conditions]{Left: Participant's viewpoint within the HoloLens in the ``hard-shadow'' condition. Right: The different shadow conditions, from top to bottom: ``Round'', ``Hard-Edge'' and ``Soft-Edge'' shadows.}
  \label{fig:shadowsConditions}
\end{figure}


The shadows that would have been cast by the two lights of the room are computed
in real-time in Unity3D and are consistent with the size and positions of the
real lights in the corridor thus the shape, size and location of the virtual
shadows vary according to the position of the virtual cube in the corridor.

Regarding virtual shadows and \gls{OST}-\glspl{HMD}, one should take into
account the following limitation: due to the transparency of the screens, it is
impossible to display black in \gls{OST}-\glspl{HMD}. Two options were then
possible regarding the display of virtual shadows in our AR scene: (i)
artificially increase the brightness of the whole AR scene, except for the
shadows or (ii) increase the brightness of the shadows only.

Both options present advantages and drawbacks. Option (i) would allow for more
realistic shadows since they would appear darker than option (ii). Nevertheless,
given the HoloLens' relatively small \gls{FoV}, option (i) would give rise to a
brighter window within the user's perspective. Indeed, the screens (i.e. where
it is possible to render in the HoloLens) would seem brighter than the rest of
the user's view, e.g. the peripheral view. Option (ii) on the contrary would
keep a coherent brightness between the screens and the ``non renderable'' part
of the HoloLens' lenses. As a consequence, we chose option (ii), that is to
display our virtual shadows in a slightly brighter way.

The experiment was carried out at four different target distances: 6m, 7.5m, 9m
and 10.5m. Moreover, the largest distance used in our studies (10.5m) guarantees
that participants were still within a relative comfort zone for the
HoloLens\footnote{\url{https://docs.microsoft.com/en-us/windows/mixed-reality/comfort}}.

960 trials (20 observers × 4 types of shadow × 4 distances × 3 repetitions) were
conducted in the experiment.

\subsubsection{Participants}

A total of 22 participants passed the screening step and took part in this
experiment (11 males and 11 females). They were between 21 and 31 years of age
(\emph{M} = 24\emph{.}91\emph{, SD} = 2\emph{.}72). Results of the subjective
questionnaires show that 7 participants had no experience with \gls{VR} or
\gls{AR}, 12 participants had little experience (used gls{VR} or \gls{AR} 1-5
times), and 3 participants were experienced users ($>5$ times). Each participant
completed 48 trials.


\subsubsection{Data Analysis and Results}

In order to conduct our analysis, and based on our measurements, we used as
dependent variable the relative error of egocentric distance estimation. This is
computed as follows:

\[E_{relEgo} = (d_{cube} - d_{target}) / d_{target} \]

where $d_{cube}$ is the distance of the cube estimated by the participant and
$d_{target}$ is the real distance of the target.

We removed 5 participants from the initial dataset: 2 of them misunderstood
instructions and thus did not conduct the tasks in a right way, another 2
subjects had abnormally high standard deviations (more than 3 times the standard
deviations of other subjects). The last one was removed because of its large
number of outliers (12 among 48 trials). Data from the remaining 17 participants
were kept for our statistical analysis, using IBM SPSS Statistics 24.0 with a
0\emph{.}05 significance level.

We first conducted Shapiro-Wilk tests to assess data normality for different
levels of factors, which includes four types of shadows (OFF, Round, Hard-edge,
Soft-edge) and four distances (6m, 7.5m, 9m, 10.5m). The results show that the
data does not distributed normally: OFF (\emph{W} = 0\emph{.}970; \emph{p
\textless{}} 0\emph{.}001), Round (\emph{W} = 0\emph{.}981; \emph{p} =
0\emph{.}008), Hard-edge (\emph{W} = 0\emph{.}984; \emph{p} = 0\emph{.}022),
Soft-edge (\emph{W} = 0\emph{.}976; \emph{p} = 0\emph{.}002); 6m (\emph{W} =
0\emph{.}924; \emph{p \textless{}} 0\emph{.}001), 7.5m (\emph{W} = 0\emph{.}945;
\emph{p \textless{}} 0\emph{.}001), 9m (\emph{W} = 0\emph{.}960; \emph{p
\textless{}} 0\emph{.}001), 10.5m (\emph{W} = 0\emph{.}975; \emph{p} =
0\emph{.}001).

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image4.png}
  \caption[Exp. 3.1 - Relative error per distance]{Experiment 1. Relative errors in distance judgments, averaged for all participants grouped per distance condition.}
  \label{fig:shadows-exp1results-1}
\end{figure}

Thus, we decided to conduct non-parametric tests on the dataset. First, a
Kruskal-Wallis H Test was applied. Results showed, as expected, a significant
effect of distance (\emph{H} = 52\emph{.}631; \emph{p} \textless0\emph{.}001)
confirming that judgment errors increase along with the distance between the
cube and participants, see \autoref{fig:shadows-exp1results-1}. We followed by
computing post-hoc tests: all pairwise comparisons with adjusted \emph{p}-values
using the Bonferroni correction for multiple tests. Significant effects were
found between (i) 6m and 10.5m (\emph{Z} = \emph{-}5\emph{.}227; \emph{p
\textless{}} 0\emph{.}001), (ii) 7.5m and 10.5m (\emph{Z} =
\emph{-}6\emph{.}951; \emph{p \textless{}} 0\emph{.}001), (iii) 9m and 10.5m
(\emph{Z} = \emph{-}4\emph{.}442; \emph{p \textless{}} 0\emph{.}001).

As for the effects of shadows, unexpected to us, the Kruskal-Wallis H test
showed a non-significant effect of shadow type (\emph{H} = 7\emph{.}446;
\emph{p} = 0\emph{.}059), see \autoref{fig:shadows-exp1results-2}. This was
unexpected since the presence of shadows has been reported to increase accuracy
of distance judgments in AR (see e.g. \textcite{Diaz2017}). However, since the
Kruskall-Wallis' \emph{p} value is close to the significance level used in our
analysis, we computed Mann-Whitney U Tests to compare data of each different
shadow pair.

Results showed that there is significant difference between the Shadow OFF
condition and other conditions (i) OFF and Round-edge (\emph{Z} =
\emph{-}2\emph{.}064; \emph{p} = 0\emph{.}039), (ii) OFF and Hard-edge (\emph{Z}
= \emph{-}2\emph{.}435; \emph{p} = 0\emph{.}015), (iii) OFF and Soft-edge
(\emph{Z} = \emph{-}2\emph{.}106; \emph{p} = 0\emph{.}035), see
\autoref{fig:shadows-exp1results-2}. Moreover, results showed a non-significant
effect of the three Shadow ON conditions.

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image5.png}
  \caption[Exp. 3.1 - Relative error per shadow type]{Experiment 1. Relative errors in distance judgments, averaged for all participants grouped per shadow type.}
  \label{fig:shadows-exp1results-2}
\end{figure}

In addition, the completion time (i.e. the time it took for each participant to
complete the task) has been analyzed for each trial. First, Shapiro-Wilk tests
were applied to assess data normality for different levels of factors. Results
show that the data is not normally distributed for any of the factors: OFF
(\emph{W} = 0\emph{.}841; \emph{p \textless{}} 0\emph{.}001), Round (\emph{W} =
0\emph{.}752; \emph{p \textless{}} 0\emph{.}001), Hard-edge (\emph{W} =
0\emph{.}840; \emph{p \textless{}} 0\emph{.}001), Soft-edge (\emph{W} =
0\emph{.}869; \emph{p \textless{}} 0\emph{.}001); 6m (\emph{W} = 0\emph{.}905;
\emph{p \textless{}} 0\emph{.}001), 7.5m (\emph{W} = 0\emph{.}851; \emph{p
\textless{}} 0\emph{.}001), 9m (\emph{W} = 0\emph{.}840; \emph{p \textless{}}
0\emph{.}001), 10.5m (\emph{W} = 0\emph{.}770; \emph{p \textless{}}
0\emph{.}001).

\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image6.png}
  \caption[Exp. 3.1 - Completion time per distance]{Experiment 1. Completion time in distance judgments, averaged for all participants grouped per target distance.}
  \label{fig:shadows-exp1results-3}
\end{figure}

Then, a non-parametric Kruskal-Wallis H Test was applied to the data of
completion time. Results show a significant effect of distance (\emph{H} =
67.520; \emph{p} \textless0\emph{.}001) proving that completion time increases
along with the distance between the cube and participants, see
\autoref{fig:shadows-exp1results-3}. We followed by computing post-hoc tests:
all pairwise comparisons with adjusted \emph{p}-values using the Bonferroni
correction for multiple tests. Significant effects were found between (i) 6m and
9m (\emph{Z} = \emph{-}4\emph{.}488; \emph{p \textless{}} 0\emph{.}001), (ii) 6m
and 10.5m (\emph{Z} = \emph{-}7.078; \emph{p \textless{}} 0\emph{.}001), (iii)
7.5m and 9m (\emph{Z} = \emph{-}3\emph{.}886; \emph{p =} 0\emph{.}001), (iv)
7.5m and 10.5m (Z = -6.515; \emph{p \textless{}} 0\emph{.}001).

As for the effects of shadows, the Kruskal-Wallis H test showed a
non-significant effect of shadow type (\emph{H} = 1.904; \emph{p} =
0\emph{.}593), see \autoref{fig:shadows-exp1results-4}. Afterwards, we computed
Mann-Whitney U Tests to compare data of each different shadow pair. However, no
significant effect was found in the results.


\begin{figure}
  \centering
  \includegraphics[width=.7\textwidth]{media/image7.png}
  \caption[Exp. 3.1 - Completion time per shadow type]{Experiment 1. Completion time in distance judgments, averaged for all participants grouped per shadow type}
  \label{fig:shadows-exp1results-4}
\end{figure}

From the results, it can be seen that there is a trend towards underestimation
but the high variance levels of our results could not confirm this bias. In
order to further analyze this effect, we studied results for each participant
individually.

Participants' individual data were observed to confirm the stability of the
above findings. As shown in \autoref{fig:shadows-exp1results-5}, 9 out of 17
subjects have positive relative errors, 5 out of 17 subjects have negative
relative errors, and 3 out of 17 subjects have both positive and negative
relative errors. In other words, the number of subjects who underestimate
distances in AR accounts for the largest proportion in experiment 1. This result
is consistent with the general trend of the data.

\begin{figure}
  \centering
  \includegraphics[width=.9\textwidth]{media/image8.png}
  \caption[Exp. 3.1 - Individual results]{Experiment 1. Individual relative errors in distance judgments.}
  \label{fig:shadows-exp1results-5}
\end{figure}

In addition, the mean relative error of each subject under different shadow
types was computed. Results show that 11 out of the 17 subjects have higher
distances judging accuracy under at least one of the shadows ON conditions
compare with the shadows OFF condition (see \autoref{tab:shadows-results1-1}).


\begin{table}[]
\footnotesize
  \caption[Exp 3.1 - Individual mean relative errors by shadow type]{Individual mean relative errors in distance judgments under different shadow types.}
  \input{papers/shadows/data/table1-1}
  \label{tab:shadows-results1-1}
\end{table}

On the other hand, the mean relative error of each subject under different
targets' distances was computed. Results show that 14 out of 17 subjects have
the highest mean relative errors under distance 10.5m (see
\autoref{tab:shadows-results1-2}).

\begin{table}[]
\footnotesize
  \caption[Exp 3.1 - Individual mean relative errors by distance]{Individual mean relative errors in distance judgments under different targets' distances.}
  \input{papers/shadows/data/table1-2}
  \label{tab:shadows-results1-2}
\end{table}

As for the results of the completion time, we also computed the individual data
for each subject. Due to our statistical analysis only found significant
distances' effect on completion time, we computed the mean completion time for
each subject under different targets' distances (see
\autoref{tab:shadows-results1-3}). The result shows 14 out of 17 subjects have
longer mean completion time under the distance of 9m and 10.5m than under the
distance of 6m and 7.5m. This result is coherent with the global statistical
analysis.

\begin{table}[]
\footnotesize
  \caption[Exp 3.1 - Completion time]{Individual mean completion time in distance judgments under different targets' distances.}
  \input{papers/shadows/data/table1-3}
  \label{tab:shadows-results1-3}
\end{table}


\subsubsection{Discussion}

As mentioned in \autoref{subsec:RW-AA}, many previous studies showed that
shadows are very important for human's visual perception, and that they
represent an important depth cue \autocite{Mamassian1998,Hubona1999}. While it
has been showed that shadows can help people confirm objects' position in a
virtual scene \autocite{Yonas1978,Thompson2011}, research on shadows' influence
on depth perception in AR is still limited, even more so in \gls{OST} \gls{AR}
\autocite{Diaz2017}. 

The present experiment aimed at investigating the influence of different levels
of realism shadows on distance perception in \gls{OST} \gls{AR}. First of all,
we wanted to test whether the presence or absence of virtual objects' shadows
would improve distance perception accuracy in OST AR (hypothesis \textbf{H2}).
Our results show that while distance perception is better in the shadows ON
condition, no significant difference was found between the three shadows
conditions. Participants' individual data were also observed to confirm this
phenomenon. Results show that for most subjects (11 out of 17), their individual
data were coherent with this result. We believe it is a very interesting result,
since it would tend to show that shadow type does not to play an important role
on distance judgment in \gls{OST}-\glspl{HMD}, disproving our hypothesis
\textbf{H3}. This is a meaningful result since application developers could use
``cheap'' non-realistic shadows, like our round shadows, instead of costly ones,
especially since most \gls{OST}-\glspl{HMD} use standalone computers with
limited CPUs/GPUs.

Finally, regarding hypothesis \textbf{H1} (participants underestimate distances
in \gls{OST} \gls{AR}), we can confirm a trend towards underestimation but high
variance levels of our results could not confirm this bias. The relative errors
for each individual were also observed, and we found that most of the
participants (9 out of 17) underestimate distance in \gls{AR}, and 3 out of 17
subjects have both positive and negative relative errors. In addition, our
results also confirm that errors on perceived distances increase along with the
distance between the virtual object and participants.

As for the completion time of each trial, although the initial position of the
virtual cube is random, the statistical analysis reveals that completion time of
trials increases significantly along with the distance, however, the shadow type
has no significant effect on the completion time of trials. It could be inferred
that the participants need to spend more time on matching task when the targets'
distances were farther away.
