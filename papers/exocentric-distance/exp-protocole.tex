%\fa{Move this introductory part to the beginning of the chapter}
In this section we present a user experiment aimed at the evaluation exocentric
distance perception in \gls{OST} \gls{AR}. To do so, we designed an experiment
where participants had to evaluate distances between objects (spheres placed in
a plane perpendicular to the users' view) in the following configurations: (i)
two real spheres, (ii) two virtual spheres displayed in \gls{AR} and (iii/iv)
one real sphere and one virtual sphere. This last condition is in fact two-fold,
since the real sphere can be displayed on the left hand side or on the right
hand side of the participant. In the following, we detail the experimental
design, the apparatus, as well as the protocol.

\subsection{Participants}
This experiment involved 29 participants (21 male and 8 female), aged from 19 to 54 years (mean=25.0, SD=7.2). Among them, 27 were right-handed, one was left-handed and one reported to be ambidextrous. 
%
The eye dominance was assessed using the method proposed by \textcite{Durand1910} in which participants were asked to look at a distant object through a hole made with their two hands.
9 participants had a dominant left eye and 20 had a dominant right eye. 
%
Participants were students of the university or members of the laboratory and naive about the purpose of the experiment. They were not paid for their participation. All participants had normal or corrected-to-normal vision and 23 out of 29 had already used a \gls{HMD} before but none of them had used an \gls{OST} \gls{AR} \gls{HMD} before.

\subsection{Experimental Apparatus}

%real stuff
The real stimuli were orange 3D-printed spheres with a radius of 20~mm. They were placed on a bench located 2.1~meters away from the participant  (see~\autoref{fig:exp2-apparatus} left). This bench was composed of two symmetric rails, one for each sphere. A moving trolley was placed on each rail and controlled by a stepper motor. 
%The rails were arranged opposite each other, one as an extension of the other. 
Given the mechanical characteristics of the motor and of the transmission system, spheres could be positioned with a precision of 0.2~mm. The control software of the bench run on a Raspberry Pi and was written in Python.   %v5. 

The placing mechanism was completely hidden by a black wooden structure. In addition, a \textit{moving panel} was cut inside and could be toggled on and off in order to reveal or hide the visual stimuli. Figure~\ref{fig:exp2-apparatus} (center) allows to better appreciate the mechanism in which the moving panel is opened and the two real spheres are visible. All the other parts of the bench were always hidden during the experiment. AR markers were placed at each end of the panel (see~\autoref{fig:exp2-markers}) 
which were used to calibrate the position of the bench in the \gls{VE}. 


\begin{figure}
	\centering
	\includegraphics[height=5cm,trim=0 0 4cm 0,clip]{pictures/front-bench}
	\includegraphics[height=5cm]{pictures/back-rail}
	\caption[Exp. 2 - Apparatus]{Left, bench displaying two real spheres. The hinge-actuated moving panel, opened here, could be automatically opened/closed to reveal/hide the visual stimuli. Right, one of the two rails of the bench, seen from behind. An orange sphere is attached on top of a trolley that can slide on the rail. The trolley is moved by a stepper motor through a belt. The other half of the bench is symmetrical. }
	\label{fig:exp2-apparatus}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[height=6cm,trim=6cm 0 3cm 0,clip]{pictures/hands}
	\caption[Exp. 2 - Answering protocol]{Participants could provide the perceived exocentric distance by placing two sliding spheres. After the participants placed the spheres the system automatically took a picture of both spheres which was used to measure the distance between both spheres.}
	\label{fig:exp2-answer}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=.8\columnwidth]{pictures/RR}
	\includegraphics[width=.8\columnwidth]{pictures/RV}
	\includegraphics[width=.8\columnwidth]{pictures/VV}
	\caption[Exp. 2 - Stimuli conditions]{Three out of the four possible display conditions, from top to bottom: \emph{real-real}, \emph{real-virtual} (note that \emph{virtual-real} is also part of the experimental design) and \emph{virtual-virtual}.}
	\label{fig:exp2-hololensPoV}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=0.6\columnwidth]{pictures/markers}
	\caption[Exp. 2 - Calibration markers]{An image used as a Vuforia marker, viewed from inside the HoloLens. The green corners represent the detection of the marker by the system and allowed users to assess a correct alignment during the calibration phase.}
	\label{fig:exp2-markers}
\end{figure}

%virtual stuff
The virtual stimuli were spheres displayed on an \gls{OST} \gls{AR} headset, the Microsoft HoloLens. The HoloLens has a \gls{FoV} of 30°×17.5° with a resolution of 1268×720~px per eye. It contains an \gls{IMU} and four sensors (a depth camera, a RGB camera, a four-microphone array, and an ambient light sensor) used to map the 3D environment in real-time. The HoloLens also supports voice input, basic gesture recognition, and head tracking. We selected the HoloLens mainly due to the quality and robustness of its tracking. The \gls{AR} scenes were built using Unity3D v2018.3.0f2. 
%
The spheres were displayed using the standard Unity shader, they were lit with a single frontal directional light and without texture. This provided a coherent ambient and specular lightning (with no visible shadow since the light was frontal). 

The \gls{HMD} was calibrated before the experiment for each participant using the HoloLens' built-in calibration procedure. At the beginning of the experiment, the location of the bench was determined using the markers placed on it that were detected using the Vuforia Engine v8.0.10\footnote{\url{https://developer.vuforia.com/}}. 
This position was registered and remained fixed during the whole experiment. 

In order to ensure the correct alignment between real and AR content, before each run we asked participants to visually assess that the real and the virtual spheres displayed and placed at the same position were superimposed. If there were any mismatch between the two spheres, the application was reset. This never occurred except when participants took off the headset during the break, which leads us to believe that the calibration was robust enough for our purpose.

\begin{figure}
	\centering
	\includegraphics[width=.95\columnwidth]{pictures/camView-reversed-Blur.png}
	\caption[Exp. 2 - Computer vision for answer recording]{Detection of the spheres' positions with the camera. Left: original image from the camera. Right: Image obtained after applying a color threshold in the LAB color system.}
	\label{fig:exp2-computer}
\end{figure}

%Participant pov & answer
Participants were seated on a fixed chair in front of a table. They were asked to sit straight and with their back resting on the back of the chair during the whole experiment. Two spheres, identical to those placed on the bench, were fixed on sliding support on a rail in front of them, see~\autoref{fig:exp2-apparatus} left. Participants could answer by sliding these \emph{answering spheres}. The distance between the two answering spheres was recorded by a camera placed under the bench, in front of the participant. To compute the distance between both spheres, the camera took a picture and the image obtained was filtered to detect the spheres' centroid positions. To do so, the picture was first converted in the LAB color system to get rid of the luminosity component and then thresholded to only keep the specific orange color of the sphere (see~\autoref{fig:exp2-computer}). The whole procedure was implemented using OpenCV 3.2.0. 
The camera was calibrated using OpenCV's standard calibration method at the beginning of the experiment by placing the spheres at a reference known distance. The resolution of the camera was 1280×720~px which corresponds to a precision of 2~mm on the recorded answer. 


\subsection{Procedure}
Participants started by reading and filling out a short form containing written instructions about the experiment. After verbal explanations, they carried out the in-built calibration procedure of the HoloLens. Then, participants performed 10 trials in order to get used to the experimental procedure. Finally, participants had to perform two blocks of 48 trials each (see~\autoref{ref:exp-design}). Participants were allowed to have a break between the two blocks. The procedure for each trial was as follows. 

At the beginning of each trial, the moving panel of the occluder was closed and the moving spheres in front of the participant were side by side, regrouped at the center of the rail. 
%
The moving panel opened, revealing zero, one or two real spheres, according to the trial condition. Simultaneously, the virtual spheres (if relevant for the trial) were displayed through the \gls{HMD}. 
There were two distinct spheres visible at a time. The spheres were visible for five seconds. After that time, the moving panel closed hiding the physical spheres, and, if present, the virtual spheres disappeared. 

The participant then had five seconds to move the spheres in front of him to reproduce the same distance as the distance between the spheres he just saw. During this time, a virtual indicator informed the participant of the time left to answer. Once this time was up, the camera recorded the distance between the two spheres.
%
If the participant was still adjusting the distance after five seconds this response was removed from the analysis. We believe this did not have a strong impact on the results as this happened rarely (less than 0.005\% of the trials). 

The participant had then five seconds to bring the sphere in front of him at their initial position while the spheres of the bench, behind the occluder, was moving to prepare the next trial. 

The proposed protocol was designed to ensure that (1) all participants looked at the stimuli the same amount of time, (2) that the stimuli was placed at an optimal viewing distance to minimize the potential accommodation and vergence bias and (3) that the stimuli was visible simultaneously by both eyes without head motions even when the distance between them was the maximum. This design encouraged participants to exhibit a similar exploration behavior (static), thus potentially reducing variability. 

In total, the average time per participant, including pre-questionnaires, instructions, calibration, experiment, breaks, post-questionnaire and debriefing, was 45~min. Participants wore the \gls{HMD} for approximately 30~min.

\subsection{Experimental Design} \label{ref:exp-design}

\begin{table}[t]
	\small
	\centering
	\caption[Exp. 2 - Summary of experimental variables]{Summary of Independent and Dependent Variables.}
	\begin{tabular}{|c|r|c|l|}
		\hline
		\multicolumn{4}{|c|}{\textbf{Independent Variables}} \\
		\hline
		\multicolumn{2}{|r|}{\textit{Observers}} & 29 & (random variable) \\
		\hline
		\textbf{C1} & \textit{Distance (cm)} & 8 & 25, 30, 35, 40, 45, 50, 55, 60\\
		\hline
		\textbf{C2} & \textit{Type} & 4 & real-real (RR), virtual-virtual (VV)\\ 
		& & &  real-virtual (RV) , virtual-real (VR)\\
		\hline
		\multicolumn{2}{|r|}{\textit{Repetition}} & 2 & 1, 2 \\
		\hline
		\multicolumn{4}{|c|}{\textbf{Dependent Variables}} \\
		\hline
		\textbf{D1} & \textit{Reporting} & \multicolumn{2}{p{120pt}|}{Distance between the two spheres positioned by the participant} \\
		\hline
	\end{tabular}
	\label{tab:exp-variables}
\end{table}

We used a mixed-model within-subjects design with the following conditions:

% need to follow the template ^^
\begin{itemize}%[itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt,leftmargin=5mm]
	\item \textbf{C1}: The distance between the two stimuli. Eight possible values were chosen: 25~cm, 30~cm, 35~cm, 40~cm, 45~cm, 50~cm, 55~cm, 60~cm. 
	
    \item \textbf{C2}: The type for each stimulus, which could be both real, both virtual, or one real and one virtual (\emph{RR}, \emph{VV}, \emph{RV}, \emph{VR}). In the \textit{one real/one virtual} condition, in order to minimize potential biases due to the placement of the ``real'' sphere (placed on the left or on the right), the placement of the real sphere was counterbalanced. Therefore, for the real-real and virtual-virtual conditions, the number of trials was also doubled to ensure the same number of repetitions for all \textbf{C2} conditions. We were expecting that the effect of the side was not significant, but we included it in the design for completeness.  
\end{itemize}

The distance between the participant and the stimuli (2.1~m) was chosen to be in the comfort zone of the HoloLens\footnote{\url{https://docs.microsoft.com/en-us/windows/mixed-reality/comfort}}.
%
This specific distance should limit potential biases due to a mismatching focal distance between the displayed virtual objects and the real objects.
%
In addition, since the horizontal augmented \gls{FoV} of the HoloLens is 30°, \textbf{C1} was chosen to ensure that both spheres could be simultaneously on the user's field of view. 

In summary, participants were presented with 96 trials: 8 distances (\textbf{C1}) $\times$ 4 stimuli (\textbf{C2}) $\times$ 2 (for asymmetrical cases) $\times$ 2 blocks. For each block, the order of \textbf{C1} and \textbf{C2} were fully randomized to minimize potential order effects. The only dependent variable was the reported exocentric distance \textbf{D1}.

Following previous research, and based on our experimental design, our main research hypotheses were the following: %\fa{there should be a clear link between the previous work and the Hs, for me is not that clear in the current state}

\begin{itemize}%[itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt,leftmargin=5mm]
    \item \textbf{H1} Exocentric distance perception is the most accurate for the real/real condition.
    
    \item \textbf{H2} Exocentric distance perception for the virtual/virtual is underestimated when compared to the real/real condition.
    
    \item \textbf{H3} Exocentric distance perception for the real/virtual and virtual/real conditions is in-between the both real/real and virtual/virtual conditions. 
\end{itemize}

%\jmn{Good point. I believe our hypotheses mainly rely on results from a recent paper: Singh2017 -- The Effect of Focal Distance, Age, and Brightness on Near-Field Augmented Reality Depth Matching. Nevertheless this paper focused on egocentric OST AR distance perception.}

%\jmn{After checking a bit more, it seems that near-field exocentric distance perception has been shown to be underestimated... Shouldn't that be our hypotheses? (which would be consistent with the results)}


\subsection{Statistical Analysis}

The reported distance was analyzed using general linear mixed model. The model included the independent variables \textbf{C1} and \textbf{C2} as fixed effects and the \textit{participant} factor as a random effect. Tukey post-hoc tests ($\alpha > 0.05$) were done when needed using the Bonferroni correction for multiple comparisons. The degrees of freedom were corrected using the Greenhouse Geisser correction method when the sphericity assumption was violated. 
%
The statistical analysis was carried out using the R software using the function \textit{aov\_ez} of the \textit{afex} package. The functions \textit{emmeans} (\textit{multcomp} package) and \textit{glht} (\textit{multcomp} package) were used for pairwise comparisons.

The statistical analysis only considered the relative distance estimates, computed as the percentage between the real distance between stimuli and the estimated distance from participants. %\jmn{should we say why? we did not choose absolute distance estimates?}