
%rappel des résultats
% 
Results of this study show that the distance between two objects presented in \gls{OST} \gls{AR} in the perceptual middle-field are overall overestimated (by around 25.5\%) across all four conditions where both objects can be either: \emph{real-real}, \emph{real-virtual}, \emph{virtual-real} and \emph{virtual-virtual}.

When further analyzing those results, we also notice that two virtual objects in \gls{OST} \gls{AR} are perceived as significantly closer than two real objects at the same positions. 
%
For the farthest distance (over 50~cm between both objects), this effect is equivalent to an underestimation of 4\% for the \textit{both virtual} case when compared with the \textit{both real} layout. This effect is in line with previous results of studies showing an underestimation of exocentric distances both in \gls{AR}~\autocite{Dey2012} and in \gls{VR}~\autocite{Kelly2015}, but here we observed it for smaller distances. 

%
One possible explanation of this phenomenon can be linked to the underestimation of egocentric distances. Under the tested condition, if the virtual spheres are perceived closer than the real ones, and since their positions in the visual field is the same, the resulting exocentric distance is perceived smaller. 

Even if the \gls{VE} was carefully designed to provide the same lighting, the limitations of the \gls{OST} display induced a different representation between the virtual and the real spheres. Therefore, there were clear, noticeable differences in lighting and shading according to the nature (real or virtual) of the sphere~\autocite{Ashley1898}. This difference in rendering between real and virtual spheres could thus lead to a bias in perceived distances.


% 
The global overestimation of distances for all the tested conditions is not a usual result. In our case, exocentric distances are reported as 25.5\% larger on average. This result is unusual since the majority of studies report a rather accurate exocentric distance perception in the frontoparallel plane, see~\autocite{Geuss2012}. However, these results can be linked with the chosen distances between objects as well as the reporting procedure chosen in our experiment. Here, participants had to move two real objects in front of them. Yet, the proprioceptive distance perception is known to be non-homogeneous~\autocite{Fasse2000}. Since the reporting task involves hand movement, the answer could be biased by this effect. 


% RV/VR
Finally, the most surprising result in our study corresponds to the asymmetrical results from the \emph{real-virtual}/\emph{virtual-real} setups. These results are more difficult to interpret. While we expected for these two layouts to be symmetrical, this was not the case. We also hypothesized (see \textbf{H3}) that the \emph{RV} and \emph{VR} conditions would lead to results in-between those of the \emph{RR} and \emph{VV} conditions. This result is unexpected since previous results, see Lin et al.~\autocite{Lin2015}, showed a constant underestimation in the \textit{one virtual/one real} situation.

We thus tried to further analyze this result to understand what could lead to this asymmetry of error estimations based on the position of the virtual stimulus. 
%
Since our experimental setup was designed to be completely symmetrical, this effect of the side was not expected and we suggested that it relies on some internal lateralization of the participants. 
%
To do so, we grouped trials based on the position of the stimulus in relation with the participants' dominant eye of the subject. 
%
Our results showed a significant effect between \emph{Vdom} (virtual object on the side of the dominant eye) and \emph{Rdom} (real object on the side of the dominant eye) with an overestimation in the \emph{Rdom} case of around 5\% (see~\autoref{fig:exp2-error-eye-dominance}).
Therefore, the results are closer to the \textit{real-real} condition when the real object is on the side of the dominant eye, and are closer to the \textit{virtual-virtual} condition when the virtual object is on the side of the dominant eye. 
%
As a first explanation of this eye dominance, we supposed that the observer could have focus more one the object on the same side as its dominant eye and consider the other object as symmetrical. 

Hand dominance could also be a potential factor explaining this result, yet, only one subject was left-handed, which shows that the effect happened already with a right handed population, ruling out this potential bias.

Another possible explanation for this phenomenon could rely on luminance. Indeed, luminance has been reported as being a strong factor for depth estimation for real objects~\autocite{Ashley1898,Farne1977,Coules1955} or more recently in \gls{AR}~\autocite{Singh2017}. More saturated and brighter objects tend to be perceived as nearer than less saturated and bright objects. Then, a difference of illumination between the screens of the HoloLens could lead to a depth mismatch of the objects and then a perceived exocentric distance biased. Therefore, we measured the respective luminance of the screens by displaying a full screen white rectangle on the HoloLens and taking pictures of the screens using a camera (Point Grey Flea3 FL3-U3-32S2C). We then calculated the average brightness of the central part of the screens (where the sphere was displayed). We found that the right screen was brighter than the left one with a contrast value of 14\%. However, since the experiment was designed in way that both spheres were always visible by both eyes at the same time, the potential effect of luminance in that case should also be counterbalanced. Moreover, the HoloLens being an \gls{OST} \gls{HMD}, the relative illumination of the augmented objects is background-dependent and can vary because of the head movements of the observer. 

As of now, we cannot provide a specific explanation for this phenomenon and further studies are required to better analyze it. 

\subsection{Limitations and Future Work}
\fa{to the global future works, the limitations can remain here though.}
% change answer protocol
Our protocol was focused on absolute distance perception. However, our results -- and in particular the specific case of the \textit{real-virtual} and \textit{virtual-real} layouts -- were difficult to interpret. 
New experiments, more focused on this specific cases are needed to assess the similarities of distance perception under those conditions. In particular, \gls{2AFC} reporting protocols could be used since they are known to be more accurate when assessing the existence of potential biases. 

% vary hmd
The present study was conducted with a specific \gls{OST} \gls{HMD}: the Microsoft HoloLens. This device has particular characteristics, one of the most important being its limited \gls{FoV}. This characteristic constrained our experimental design and the position tested for the virtual sphere, since they had to be both displayed. The focal distance of the \gls{HMD} is also an important factor which should be studied. 
%
Then, new experiments can be conducted using other \gls{OST} \glspl{HMD} to provide additional views on exocentric distance perception in \gls{AR}. The \gls{HMD}'s display technique can also vary, by using a \gls{VST} \gls{HMD}, or even with projectors in the case of \gls{SAR}. 

% vary distance
As stated by the literature, space perception varies according to the distance field considered~\autocite{Howard2002}. Here, we focused on one specific distance but exocentric distance perception should be studied for other distance fields. This factor has also to be studied combined with the focal distance of the \gls{HMD}, as discussed before. 

% vary shape, size and rendering & shading
The visual aspect of the considered objects can also influence the results. In this study, we focused on very salient objects, with known size and simple shape. However, the shape and size of the presented objects could vary. Two different objects can also be presented to the observers to evaluate the influence of the relative size of the objects in \gls{AR}. Moreover, the virtual rendering and shading methods can be changed, in particular to observe the impact of the rendering quality on exocentric distance perception. Lastly, augmented objects -- with mixed real and virtual parts -- could also be used. 

% vary layout (add occluders, objects in between)
The layout of the objects was here limited to two objects placed in the frontoparallel plane. This layout can be extended by adding other objects to evaluate the impact of a virtual or real object placed between the two original ones. Occluders or objects in other planes can also be used. Finally, the objects' layout can also be modified by rotating the plane where the objects are placed. Indeed, we could not only consider extending our study on exocentric distance perception in the sagittal plane but also on all the possible angles between those two (frontoparallel and sagittal) layouts. This is motivated by previous evidence (see \autocite{Loomis1992}) that physically equal distances in a sagittal plane on the ground are perceptually underestimated relative to those in a frontoparallel plane, even under full-cue conditions. We would expect that placing virtual objects outside of the frontoparallel plane would lead to different exocentric distance estimation.