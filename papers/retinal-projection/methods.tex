
We conducted a user experiment to evaluate the egocentric distance perception in \glspl{RPD} compared with \gls{OST} \glspl{HMD}. 
Participants had to perform a blind-reaching task; they had to align their finger with a target without seeing their hand.  
Two groups were involved in this experiment. The first group was wearing a \gls{RPD} while the other group used an \gls{OST} \gls{HMD}. In each group, participants had to perform the blind reaching task with both virtual and real targets in order to compare their egocentric distance perception in \gls{AR} with real perception. 
In the following, the apparatus, the protocol, as well as the experimental design are presented. 

\subsubsection{Participants}
\label{subsec:participants}

Twenty one right-handed participants took part in the study (18 males, 3 females, mean age: 24.0 (SD: 3.0), range: 21--34 years). 
%
The eye dominance was assessed using the method proposed by \textcite{Durand1910} in which participants were asked to look at a distant object through a hole made with their two hands. Five participants had a dominant left eye and 16 had a dominant right eye. 
%
Participants' \gls{IPD} was measured using the technique detailed in \textcite{Willemsen2008}, ranged between 48 and 69 mm (mean: 59.5, SD: 5.76). 
%\fa{48 seems a really low number, we can assure that this is not an measurement error?}\ep{no,  I double checked during the experiment because it seams rlly low to me also}
%
Participants were students of the university or members of the laboratory and naive about the purpose of the experiment. They were rewarded \anonymizedSum{1,000 JPY} for their participation. 
%\al{Pourquoi ? Fait bizarre}\gm{la monnaie utilisée rompt l'anonymat} 
All participants had normal or corrected-to-normal vision, 8 were wearing glasses. Nineteen out of 21 had already used a \gls{VR} \gls{HMD} before and 13 of them had already used an \gls{OST} \gls{AR} \gls{HMD} before.

This study was conducted in accordance with principles as stated in the declaration of Helsinki. The experiment was approved by the ethical committee of the \anonymizedContent{Tokyo Institute of Technology}.


\subsubsection{Apparatus}
\label{subsec:apparatus}
%\fa{can you add some dashed lines to illustrate the visible area from the participants' view point in \autoref{fig:rpd-pole}?}\ep{not really; the focal lengh of the camera is way smaller than 50mm and the picture is not represnetative of the view from a human eye}

\begin{figure}
	\centering
	\includegraphics[width=0.85\linewidth]{pictures/virt-global-setup.png}
	\caption[Exp. 4 - Overview of the experimental setup]{Overview of the experimental setup. The participant had his head on a chinrest and was able to see virtual elements with an OST device or a RPD. He was asked to point a target (virtual or real) with his finger (wearing a tracked ring). The frame was covered by white panels to prevent the participant from seeing his hand. (Here, one panel is transparent for illustration purpose).
    \label{fig:rpd-apparatus}
    }
\end{figure}


\begin{figure}
	\centering
	%\includegraphics[width=\linewidth]{pictures/global_setup_large.jpg}
	\def\svgwidth{\linewidth}
	\LARGE \input{papers/retinal-projection/pictures/global_setup_large.pdf_tex}
	\caption[Exp. 4 - User point of view and close-ups]{
		(a) Overview of the experimental system, here with the \gls{RPD}. The red cross is used only for the calibration and removed during the experiment itself. 
		(b) The real pole with the orange sphere used as target with the reflective tracking marker on the top. 
		(c) The 3D printed ring used to support 5 reflective tracking markers and provide the position of the fingertip.
		\label{fig:rpd-system}
		\label{fig:rpd-pole}
		\label{fig:rpd-ring}
	}
\end{figure}

\begin{figure}
	\centering
	\small
	\includegraphics[width=.7\linewidth]{pictures/RPD-frame-drawing.png}
	%\def\svgwidth{\linewidth}
	%\input{pictures/RPD-frame-drawing.pdf_tex}
	\caption[Exp. 4 - Target positioning system]{
		Diagram of the moving element under the structure which allows the experimenter to move the target using two magnets. 
		\label{fig:rpd-slider}
	}
\end{figure}

\paragraph{Overall structure and tracking system.}
A structure was built (30~cm wide, 65~cm long and 18~cm high, see~\autoref{fig:rpd-system}) and covered by white cardboard panels to prevent the use of visual cues from the background.
%
A moving part carrying a magnet was placed beneath the ``floor'' cardboard panel. Another magnet, fixed to the base of the white pole, allowed the experimenter to place the target at a specific distance (see~\autoref{fig:rpd-slider}). The absolute position of the white pole was recorded using the reflective ball located at the top of the white target pole. 
The average reported placement error was 5~mm in depth which provides a sufficient precision for the placement since the following analysis is made using the veridical position. 
%
The participant was wearing a ring carrying a 5-sphere motion tracking constellation to measure the position of his finger tip (see~\autoref{fig:rpd-ring}). 
The motion tracking system was a 6-camera OptiTrack system. Under the experimental conditions, the precision of the system once calibrated was always below 0.1~mm. 
%
The headset used to display the virtual images (either a \gls{RPD} or an \gls{OST} \gls{HMD}) was fixed on a chin rest. The height of the chair and the height of the chin rest were adjusted at the beginning of the experiment to maintain the same eye level for every participant (i.e. for each participant, the sphere was always visible in the center of their field of view). 
% 
See \autoref{fig:rpd-apparatus} for a global view of the experimental setup. 

\paragraph{Real and virtual target.}
The real stimulus was an orange 3D-printed sphere with a 10~mm radius, placed on a 1.5~mm radius white pole. The center of the ball was at 85~mm height above the frame floor, so as to be at eye height (see~\autoref{fig:rpd-system}). The white pole continues above the 3D-printed orange sphere to support an OptiTrack motion tracking ball marker, placed at 180~mm high (see~\autoref{fig:rpd-pole}). Given the characteristics of the experimental setup, the base of the pole and the motion tracking marker were out of the participant's sight.
%
The virtual stimulus was designed to reproduce the appearance of the real stimulus, including the pole. Colors, lightning and shading were carefully chosen to be as close as possible to the real target. 

\paragraph{Rendering displays.}
One group of participants used the Epson Moverio {BT-30C} \gls{AR} glasses\footnote{\url{https://epson.com/moverio-bt30c-usb-c-compatible-smart-glasses}}. The Moverio headset has a 22.8°$\times$12.8° \gls{FoV} with a resolution of 1280$\times$720 pixels per eye. 

\begin{figure}
	\centering
	\includegraphics[width=.6\linewidth]{pictures/rpd-headset.jpg}
	\caption[Exp. 4 - Custom binocular RPD headset]{
		Close-up of the custom binocular headset with its two \gls{RPD} modules mounted on an eye tracking device. 
		\label{fig:rpd-hmd}
	}
\end{figure}

The other group of participants was using a custom mounted binocular \gls{RPD}  (see~\autoref{fig:rpd-hmd}). This headset consisted in two QD~Laser RETISSA Displays, one for each eye. Each display module had a resolution of 1024$\times$600 pixels and an approximate horizontal \gls{FoV} of 20°.
These two modules have been assembled directly on Tobii Pro Glasses 2\footnote{\url{https://www.tobiipro.com/}} and the whole unit can be worn directly on the head. To the best of our knowledge, this custom \gls{HMD} is the first reported prototype of a binocular \gls{AR} headset using \gls{RPD} technology and capable of eye tracking. However, like the \gls{OST} headset, this headset was rigidly mounted on the structure to ensure optimal alignment. In addition, eye tracking data was not collected during this experiment. 
%\gm{rien sur le vertical? si pixels carrés, on peut faire une règle de 3}
\fa{Resolution was not "exactly" the same, can this have an influence on depth estimates?}\ep{i do not know, existing RW on the link between resolution and depth estimation is not very thorough}
% Each module has a 20° field of view for each eye. However, the part allowing stereoscopic vision corresponds to the overlapping region of the two modules. Due to alignment limitations, the final stereoscopic viewing area was smaller than this 20°. 
The positioning of both \gls{RPD} modules ensured that the stimuli was always in the \gls{FoV} for both eyes. 
Moreover, each module has to project the image onto the observer's retina and was manually positioned and rotated along every axis to ensure the best viewing conditions. 
This adjustment was made at the beginning of the experiment (during the installation process) for each participant. 
%
Note that participants kept wearing the \gls{AR} device (the \gls{OST} or the \gls{RPD}) at all time during the experiment. This means that whenever a real object was showed to them, it was seen through the \gls{AR} device.

The \gls{AR} scene was built using Unity3D v2019.1.8f1. The spheres and their support were displayed using the standard Unity shader, they were lit with a single frontal directional light and without texture. This provided a coherent ambient and specular lightning with no visible shadow (since the light was frontal).

\paragraph{Calibration procedure.}
The calibration process was divided into several steps. Before the experiment, the \gls{FoV} of the \gls{AR} device was assessed during an iterative calibration process on an optical bench. 
Since both \gls{AR} displays are always the same for every participants, the \gls{FoV} of each device remains the same for the whole study.  
The other parameters of the calibration (rotation and translation) are adjusted for each eye separately by the participant. This was achieved by aligning, in each eye display, a virtual cross with a real calibration cross drawn on the ``back'' panel of the setup (see~\autoref{fig:rpd-system}).

At the end of this phase, the calibration was checked by visually assessing the alignment between a real and a virtual object placed at the same location.
The calibration cross was removed after the calibration procedure and the participant was able to see the target as depicted in \autoref{fig:rpd-pole}.b. 

\subsubsection{Task and Procedure}
\label{subsec:task-procedure}

Participants started by reading and filling out a short form containing written instructions about the experiment and giving their consent. After some verbal explanations, they carried out the calibration procedure as described above. 
%
Then, participants performed one trial using a real target and one trial using a virtual target to get used to the experimental procedure.
%
Finally, participants had to perform four blocks of 16 trials. Each block consisted in 8 trials with a real target and 8 trials with a virtual target. In each group of participants, half of them started with the virtual target and the other half started with the real target. Participants were allowed to have a break after the two first blocks. 

Each trial was performed as follows: the target was displayed in front of the participant at a certain depth. 
%
If the target was virtual, it was directly displayed by the \gls{AR} device. 
If the target was real, the experimenter moved the part under the frame to place the magnet at the chosen distance and then placed the real target over it at the correct location thanks to the magnetic guidance. \fa{ensure that this is totally clear, it's a tricky part on the experiment that cannot be 100\% clear to the reader}\ep{i added fig.5; should I put more detail on it?}
The participant was then asked, without seeing his hand, to place his fingertip on the side of the setup, so as to align it with the target. They were free to used either hand but all our participants were right-handed and as such used their right hand. The target remained visible during this step. 
When the participant considered the alignment as correct, he reported it to the experimenter and the depth answer of the participant was recorded. 
Finally, the virtual target disappeared or the real target was taken off. 
The experiment was not time-constrained and the participant was able to observe the target for as long as necessary. On average, one trial lasted 8.3 seconds. %10.0 seconds for a real target and 6.6 seconds for a virtual target (including the time needed for the experimenter to place the target if real). 

The total average time per participant was 45 min, including instructions, calibration, experiment, breaks, post-questionnaire and debriefing. Participants worn the \gls{AR} displays for $\sim$30 min.

\subsubsection{Experimental Design}

\begin{table}[t]
	\small
	\centering
	\caption[Exp. 4 - Summary of experimental variables]{Summary of Independent and Dependent Variables.}
	\begin{tabular}{|c|r|c|l|l|}
		\hline
		\multicolumn{5}{|c|}{\textbf{Independent Variables}} \\
		\hline
		\multicolumn{2}{|r|}{\textit{observers}} & 21 & \multicolumn{2}{l|}{(random variable)} \\
		\hline
		\textbf{C1} & \textit{distance} & 8 & \multicolumn{2}{l|}{pseudo-random, from 30 to 50 cm} \\
		\hline
		\textbf{C2} & \textit{object type} & 2 & virtual or real object & (within subject)\\
		\hline
		\textbf{C3} & \textit{device} & 2 & \gls{OST} \gls{HMD} or \gls{RPD} & (between subject) \\
		\hline
		\multicolumn{2}{|r|}{\textit{repetition}} & 4 & \multicolumn{2}{l|}{} \\
		\hline
		\multicolumn{5}{|c|}{\textbf{Dependent Variables}} \\
		\hline
		\textbf{D1} & \textit{reported distance} & \multicolumn{3}{p{120pt}|}{Projected distance between the eye and the fingertip of the participant along the depth axis} \\
		\hline
	\end{tabular}
	\label{tab:rpd-exp-variables}
\end{table}

We used a mixed-model design with the following conditions:
\begin{itemize}
	\item \textbf{C1}: The distance between the participant's eye and the target. This distance was pseudo-randomly picked between 30 and 50 cm. For a given trial, the total range was divided into eight segments and one distance was randomly picked inside each segment. 
	\item \textbf{C2}: The type of the object: real or virtual. 
	\item \textbf{C3}: The device used: \gls{RPD} or \gls{OST} \gls{HMD}. Each participant performed the experiment with only one device. 
\end{itemize}

The distance between the participant and the target (\textbf{C1}) was chosen to be in the near distance field and easily reachable to ensure the precision of the reporting technique. The closest distance was limited by the specificity of the \gls{AR} devices used. The size of the target and the dimensions of all the surrounding elements were chosen to ensure that the target was always fully visible by both eyes and was the only element which could be seen by the participant during the experiment. 

In summary, participants were presented with 64 trials: 8 distances (\textbf{C1}) $\times$ 2 target types (\textbf{C2}) $\times$ 4 repetitions. For each block, the order of \textbf{C1} was randomized and the first type of object (\textbf{C2}) was counterbalanced among participants. The participants were divided in two groups according to the \gls{AR} device they were using (\textbf{C3}). The only dependent variable was the reported distance \textbf{D1}. Eventually participants rated the difficulty of the task after achieving all trials. 

\subsubsection{Research Hypotheses}
\label{subsec:researh-hyp}

The key element of this experimental study is the egocentric distance perception of virtual elements, compared to real element distance perception. 
Given the literature, consistent results assess that, while using an \gls{OST} \gls{HMD}, egocentric distances of virtual objects are usually overestimated when compared with real objects' perception~\autocite{Swan2015,Singh2017}. 

In this experiment, two types of rendering are compared. Their specificities rely on the accommodation cue they are providing. 
\gls{OST} \glspl{HMD} provide a fixed but incorrect accommodation cue which is suspected to be partially responsible for the depth overestimation in \gls{AR}. 
On the other hand, \glspl{RPD} have no focal plane since the image is always sharp and then the accommodation cue is lacking. 
However, since the eye has to accommodate at a certain distance, the accommodation demand of the eye is expected to be driven by vergence, through the accommodation-vergence reflex. 
As such, the accuracy of distance estimation of virtual objects is expected to be better with \glspl{RPD} than with \gls{OST} \glspl{HMD}. 

However, since this accommodation cue is lacking with \glspl{RPD} the overall depth perception performance may be reduced. Indeed, the focal plane of \gls{OST} \glspl{HMD} is incorrect but fixed, which usually drive the depth overestimation by a fixed amount~\autocite{Swan2015}. 
However, the lack of focal plane for \glspl{RPD} could confuse the viewer, leading to a distance more difficult to evaluate. Distance estimation is then expected to be less precise  with \glspl{RPD}, i.e. with a larger spreading of the results. 

Considering this forewords, our main research hypotheses were the following: 
\gm{accuracy vs precision: j'ai fait une proposition dans les hypothèses}\ep{très bien}
%\gm{je reviens sur precision vs accuracy qui n'est pas défini explicitement. wikipedia dit qu'en matière de stats, on préfère parler de bias et variability. En fait, j'ai surtout peur d'un relecture non nativement anglophone qui pourrait interpréter des choses à l'envers ou de manière ambigüe.}\ep{où exatement ? tu préfères que j'uilise la terminologie "stat" pour les hypotheses ?}

\begin{itemize}
	\item \textbf{H1}: Egocentric distance perception is overestimated for virtual targets compared with real targets when seen with an \gls{OST} \gls{HMD}. 
	\item \textbf{H2}: Egocentric distance perception in \gls{AR} is more accurate (i.e. has a smaller bias) with \glspl{RPD} than with \gls{OST} devices. 
	\item \textbf{H3}: The precision of egocentric distance estimation is better (i.e. has a smaller variability) with \gls{OST} devices compared to \glspl{RPD}. 
\end{itemize}

\subsubsection{Statistical Analysis} 
\label{subsec:statMethod}

The reported distance \textbf{D1} was analysed using a multiple linear regression analysis. The model included the independent variables \textbf{C1}, \textbf{C2} and \textbf{C3} as fixed effect and the \textit{participant} as a random factor. 

Since \textbf{C1} is continuous, a multiple linear regression approach allows to evaluate the depth estimation as a continuous variable, along another continuous variable (the veridical distance) and to highlight the differences across \textbf{C2} and \textbf{C3} which are categorical. 
Contrary to an ANOVA, a \gls{LME} analysis is able to consider \textbf{C1} as a continuous variable and then provides slopes and intercepts for each condition. 
This approach provides more information about the evolution of depth perception along the distances considered~\autocite{Swan2015,Pedhazur1975}. 

For each participant and condition a linear regression was also computed. 
This supplementary element provides additional information at an individual level and also allows to evaluate the precision for each participant.
Usually, the \gls{SSE} is used to represents the part of the dispersion not explained by the model. However, the \gls{SSE} is dependent to the number of data points in each group. 
Here, not all groups have the same number of participants and therefore the same number of data points. 
So, for virtual and real targets for each participant, the \gls{MSE} of the residuals is used instead to evaluate the precision of the participants. 
This result was analysed using a two-way ANOVA with the \textit{device} (\textbf{C3}) considered as a between-subject factor and the \textit{object type} (\textbf{C2}) of the target as a within-subject factor. 
Finally, the results of the Likert-scale estimation of the difficulty of the task was analysed. A Wilcoxon-Mann-Whitney test was used since the results did not pass the requirement for a parametric test. 