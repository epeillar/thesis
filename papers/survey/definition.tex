
\subsection{Devices} \label{sec:def-devices} The devices used for \gls{AR}
purpose can be classified in three categories:

\textbf{\acrfull{HMD}.} This category contains mostly headset which convey the image
of the real world seen from the user point of view to the user's eyes, after
mixing this image with the generated digital augmentation. This devices can also
be categorized in two groups: \textit{\acrfull{OST}} and \textit{\acrfull{VST}}
\glspl{HMD}. The \gls{VST} \glspl{HMD} use cameras placed in front the user's eye in
order to provide the visual feedback of the real world while \gls{OST} \glspl{HMD}
use semi-transparent mirror to superimpose the augmentation and the direct view
of the environment. 
Even if the first one were not tracked in position, today's every modern
\glspl{HMD} have in-build tracking systems.
However, while the \gls{VST} technique allow a good superimposition of the real
and the augmented layers, the intrinsic specificities of the cameras used to
provide visual feedback, such as resolution limitation and optical distortion,
may induce viewing issues. On the contrary,
\gls{OST} \glspl{HMD} use the direct view of the environment, which provide a sharp
view but induce more constraints on the augmentations' rendering (e.g. latncy,
colors and depth of field). 

\textbf{Handheld Devices.} This category includes phones and tablets, as well as
some more unusual devices like autosteroscopic tablets. Those devices have the
characteristic to use a camera to capture the video stream of the real
environment and then display it on a screen; that is to say from a displaced
point of view. This specific paradigm has been detailed in \autocite{Kruijff2010}
but can also be compared with some studies conducted on pictures and even
paintings. This particular effect appends the more common issue  of the limited
\gls{FoV} for such devices. The tracking part of this tools are mostly
image-based. Thus, the development of this tracking techniques as well as the 
increase of computational power of handheld devices~\autocite{Dey2012} during the past
decade increased the number of experiments conducted on this platform. 

\textbf{Projectors.} We choose here to regroup those devices in one category but
we can distinguish two different uses of projectors for \gls{AR}. They can be
used to do \gls{SAR}~\autocite{Bimber2005} which directly projects virtual elements
onto a physical real scene. As a consequence, it is not dependent on the point
of view and allows several people to see and interact at the same time with the
same augmentation. However, projectors can also be used to display images on
semi-transparent glasses. Some researchers have also used them to mimic \gls{OST}
and \gls{VST} conditions~\autocite{Plopski2016}. 


\subsection{Egocentric - Exocentric} In the perceptual space, the three
dimensions are \emph{up-down}, \emph{sideways} and \emph{towards-away}. This
last dimension inform our brain about depth and is known to be the one giving
the less accurate information~\autocite{Ware2008}.

The majority of studies in the scope of this survey aims to evaluate the
distance perception of the observers, from their own point of view. That is to
say the distance between the object and their own body. This is defined as
\emph{egocentric distance} or \emph{depth perception}. It is relevant to observe
that exocentric distances evaluation (between two distant objects) are rarely
tackled in \gls{AR}. The only studies reported about this topic are \autocite{Dey2012}
and \autocite{Plopski2016}, plus \autocite{Sugano2003a} which considers both ego- and
exo-centric depth perception.
%TODO: more detail & recent studies

\subsection{Depth Cues and Cue Theory} \label{part:def-cues} 
The human is able to analyse the 2D images given by its eyes to get a 3D
representation of his environment. This is reported to be achieved by using some
\emph{depth cues}. This cues can be pictorial and non-pictorial. Pictorial ones
focus on the representation of the environment seen through one eye, while the
non-pictorial ones arise with the ability to see through two eyes and during a
period of time. Generally, 10 depth cues are defined~\autocite{Howard2002}:
\begin{enumerate}
	\item binocular disparity,
	\item binocular convergence, 
	\item accommodative focus, 
	\item atmospheric haze, 
	\item motion parallax,
	\item linear perspective and foreshortening, 
	\item occlusion,
	\item height in the visual field,
	\item shading,
	\item texture gradient. 
\end{enumerate}
%TODO: detail

\input{papers/survey/tabDepthCues}

As summarized in Table~\ref{tab:depthCues}, in addition to type (mono/binocular) and
effective distance range (see \ref{part:def-fields}), depth cues can also
provide an absolute or relative distance information. In addition, the occlusion
cue is specific due to its ordinal and then non-metric property.

In the real world, several of those cues are then combined to evaluate
distances. Interaction and fusion models have been proposed to specify the
weight of each cues~\autocite{Landy1995}. Those models suggest that the consistency
between cues play a great role. In the real world, those cues appear coherent
even if vastly dependant of the scene. However, this is often not granted for
\gls{AR} systems and applications. This can be due to technical devices which
failed to reproduce all real world stimuli but also to some specific uses of
\gls{AR} such as x-ray vision.

% Relative importance
% \autocite{Nagata1991} in Ellis1991

% Accomodation-vergence reflex? (swan2017)

%TODO age and brightness

%TODO look vergence angle

While depth cue theory explains high-level depth informations, some
specificities of the human vision such as stereoscopic depth perception of local
surfaces provides low-level
interpretation~\autocite{Collett1985,Ramachandran1985,Takeichi1992,Nakayama1996}.


\subsection{Distance Fields} \label{part:def-fields} 
%TODO depth perception in% real world review?
\Citeauthor{Vishton1995}~\autocite{Cutting2003} proposed a categorization of spaces
based on the relative strength of depth cues according to distances. (See Fig.
\ref{fig:vishton1995-000} for this relative weights plotted.) This induced three
different spaces: \textit{near-}, \textit{medium-} and \textit{far-}field , also
reported in \autocite{Vishton1995} as \textit{personal}, \textit{action} and
\textit{vista} space. As the second naming suggest, this spaces are also related
to human action.

\begin{figure} \centering
	\includegraphics[width=0.95\linewidth]{papers/survey/pictures/Vishton1995-000.jpg}
	\caption[Relative deth cues strengh]{Relative depth cues strength from
		\autocite{Vishton1995}, originally from \autocite{Nagata1991}. These function delimit
		three types of spaces, determined by different depth cues' weight.}
	\label{fig:vishton1995-000} 
\underline{}\end{figure}

\textit{Personal space} is defined as the zone immediately surrounding the
observer's heard, within arm's reach and slightly beyond.
\Citeauthor{Cutting2003} defined it within 1.5~meters, which is the working
space for a stationary individual. At that range, fives cues are the most
important: occlusion, retinal disparity, relative size, convergence and
accommodation, in that order.

\textit{Action space} is defined as the space of an individual's public action.
He can move quickly within this space, talk without difficulty and throw
objects. This space induces also a different ranking of depth cues. The five
most, sorted by weight are occlusion, height in the visual the field, binocular
disparity, motion perspective and relative size. This field extends to about
30~meters.

\textit{Vista space} extends beyond these 30~meters, until the visual field
limit, a space which can be seen but not reached quickly. At these distances,
the four cues the most important are the usually called \textit{pictorial cues}:
occlusion, height in the visual field, relative size and aerial perspective.

\subsection{X-ray vision} Showing to the user occluded information that would
not be visible in the real world is called "x-ray vision" \autocite{Feiner1995}.
This possibility is specific to \gls{AR}. Different visualization techniques have
been experimented to provide an accurate representation. This ability has been
used to many application in different field such as medical, architectural,
inspection or military application. %TODO ref

The first applications provided a symbolic representation of the occluded
objects or by displaying a ``virtual hole'' in the real object~\autocite{State1994}.
Subsequent studies experimented other representations which provide a more
realistic view of the occluded part such as edge-based~\autocite{Avery2009},
saliency-based~\autocite{Sandor2010} or melted visualization~\autocite{Dey2010}. 
%TODO more recent Dey things

To understand this kind of representation, the human mind has to interpret both
the relation between the occluded object with its surroundings and the relation
with the viewer point of view. Moreover, the corresponding depth cue is stated
as the most important one and also defined as ordinal. Breaking its coherence
with the other cues induce major impact on depth perception.

\subsection{Perceived Distance Evaluation Techniques} 
When it comes to perception an usual problem arises: the perceived distance of
an object by an observer is a subjective cognitive data which cannot directly be
measured. Whichever is the chosen reporting technique, the \textit{distance
	judgement} given by the observer induces but not perfectly traduces the
perceived distance.

However, many studies, in both real, \gls{VR} and \gls{AR} environments suggested
various techniques to report judged distance. \autocite{Loomis2003} presented a
review of most of this techniques, and recently \autocite{Swan2015} provided a more
in depth analysis of some of these.

Perceived distance evaluation techniques can be defined in several groups:
verbal report, perceptual matching, open-loop action based tasks and forced
choice techniques.

\textbf{Verbal report} is the more common technique to evaluate perceived
distance. Observers have to verbally estimate the distance to an object in
whatever unit they are familiar with~\autocite{Foley1977,Gibson1954,Gibson1955,
	Gogel1976,Gogel1981,Gogel1973,Mershon1977}, or using multiples of a 
given referent (called \textit{magnitude estimation} in psychophysical studies)
~\autocite{Baird1967,Galanter1973,Teghtsoonian1970,Teghtsoonian1970a,
	Teghtsoonian1970b,Teghtsoonian1978}. This technique have been widely 
used in real environments (up to 9km~\autocite{DaSilva1985}) but indicates 
systematically compressed perceived distances %TODO ref
and can be biased by cognitive knowledge~\autocite{Loomis2008}.

%TODO: think about citing \autocite{Loomis2003,Ellis1994,Peer2017} 
