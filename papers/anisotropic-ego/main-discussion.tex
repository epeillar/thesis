The results of our studies show a novel bias in distance perception in virtual environments: the perception of egocentric distances in \gls{VR} is anisotropic. Virtual objects located on the sides are perceived to be farther away than objects in front.
%Clarify that we are talking about objects placed on the side
The first experiment showed that an object placed on the side is perceived 5\% farther than an object placed in front. The work of Koenderink et al.~\autocite{Koenderink2002}, conducted in a real environment, underlines that the \textit{forward} direction is specific and leads to a deformation of the visual space. 
%Moreover, the optics used in \glspl{HMD}
%suffer from a rather distorted image near the edges of the lenses\gm{à prouver !!}. 
Thus, with our second experiment we aimed to characterize the observed bias by assessing distance estimation while varying the exploration angle. The results of the second experiment show that the overestimation effect on the side increases along with the angle. The greater the angle between the \textit{in front} and the \textit{on the side} stimuli, the more the observer tends to overestimate the distance of the stimuli on the side. Combined with the results of the first experiment, this effect seems to reach a plateau at 45°. 


However, \gls{VR} suffers from specific limitations which are not present in real environments. One of the most important regarding vision is the relative size of the \gls{FoV} compared with ordinary real vision, and potential image distortions introduced by \glspl{HMD}' optical systems. We conducted a third experiment to evaluate the potential impact of \glspl{HMD}' optical systems specificities on the observed bias. We forced participants to look at specific directions in order to ensure that the visual stimulus was always displayed and viewed at the center of the \gls{HMD}'s \gls{FoV}. With such an experimental protocol, the previously discussed issues were minimized. In this case, the results did not differ from those obtained in the first experiment. As a conclusion, we can state that the ocular particularity of side vision in \gls{VR} does not seem to be responsible for the observed anisotropy of distance perception. Then, the effect observed in the two first experiments could not be due to different exploration behaviours or hardware limitations, we hypothesized that the motor action of the head was a good candidate to explain this bias.


The increase in overestimation as related to the head rotation amplitude seems to verify the effort-based theory of depth perception. Indeed, according to this theory, distance judgments seem to be influenced by the amount of energy observers anticipate to expend~\autocite{Decety1989, Proffitt2006}. In our experiments, the \textit{in front} object is directly seen by the observer, while the \textit{on the side} object requires an action (head movement) to be seen. This additional movement from the resting position could modify distance perception and lead to an overestimation. 
This assumption requires subsequent studies to be evaluated. In particular, additional movements or forces could be added during the experiment to assess the effort-linked dimension of space perception under these conditions. 
%
%The plateau observed at 45° is more difficult to interpret. However, rotating the head far aside is rather uncomfortable and it may be possible that, beyond 45°, people are more likely to stop rotating their head and use their eyes. In that case, the anticipated effort required to look at an object farther than 45° would not imply a head movement but just an eye rotation, which could be considered as ``easier''. According to the effort-based perception theory, this movement could have less impact on the perceived distance.

Finally, the last experiment we conducted, aimed at evaluating the impact of the \gls{VE} on the observed bias. Indeed, in the three first experiments the \gls{VE} mainly provided binocular depth cues which could have made the task more difficult, facilitating the appearance of the perceptual bias. In contrast, in our fourth experiment, the visual stimulus provided additional graphical depth cues, lighting, and increased contrast. Here again, results show that, even with more salient depth cues, the same perceptual bias is observed even if it is slightly weaker than in the first experiment. Since the environment provided more depth cues, the ordering task was probably easier. 
Participants were thus more likely to choose the correct sphere, decreasing a bit the appearance of the perceptual bias. 
%
%Thus, this consistent result implies that the anisotropy of distance perception should be taken in account when designing \gls{VR} applications.

To analyze this study with respect to those conducted in real environments, our series of experiments can be compared with the frontoparallel curvature experiments of Koenderink et al.~\autocite{Koenderink2002}. In this paper, they determined that frontoparallels are perceived as being curved, with the concave side facing the observer. In their experiments, when participants had to place objects to a given distance to define a frontoparallel, the objects in front of them were placed further away than the correct position, which implies that they were perceived as being closer. This observation is coherent with our results, suggesting that the bias we observe is not dependent on the visual stimulus but on the distance perception assessment (e.g. motor actions) and on the directionality of the virtual environment. 
%
However, since experimental conditions were not the same in their experiments and in ours, other studies, at different distances, are needed to further characterize this bias. 

% Last, the observed bias could be considered to draw new guidelines
% for the design of \gls{VR} applications. When a \gls{VR} application deals with realism, in architectural applications for example, it could be used to better match the space perception of the observer, and/or to provide an improved experience. The \gls{VE} could also be oriented in order to be perceived longer in some direction. Moreover, \gls{VR} applications which rely on highly oriented \glspl{VE}, such as industrial accessibility tests, driving simulations, or some specific immersive games, could also consider this new perceptual bias to better specify their scenarios and their 3D contents.

\subsection{Limitations and Future Work}

% VE is oriented

Our setup, involving a participant seated on a chair, is very common to many consumer applications. Interestingly, it naturally induces a strong orientation of the \gls{VE}. This specific direction is here enforced by the position of the participant, seated and holding a joystick in front of him. The entire body is constrained toward one specific direction. Some other \glspl{VE} or user positions/orientations could therefore be considered in future work, without any strong \textit{forward} direction, and involving other settings.

%HMD optics
The influence of the position of the stimuli in the visual field does not appear
as a factor which leads to the observed anisotropy. However, many studies
underline the impact of the limited \gls{FoV} on depth underestimation in
\gls{VR}. In our study, the same \gls{HMD} was used during the whole series of experiments.
Future studies, using other optics, \glspl{HMD} or other immersive devices, could thus be conducted to provide additional
views on this anisotropy phenomenon. 

%Only asses the relative depth perception
Our series of experiments evaluated the anisotropy of distance perception considering different experimental conditions. However, given the considered 2AFC experimental protocol, our results concern the ``relative'' distance perception of objects. Future studies could then also focus on ``absolute'' egocentric distance perception of virtual objects. 
%
Moreover, the present experiment was designed to provide an evaluation of the anisotropy of distance perception in ecological conditions. It did not assess the global shape of the visual space, which is of course more complex, as detailed in~\autoref{ref:relatedWork}. The task we chose is specific and cannot cover all the various numerical models proposed for the visual space. Our observed variation in distance perception according to the head direction already seems in favor of the non-Euclidean shape of the visual space. But future work could also extend our results, so to confront them with the various models proposed regarding visual space perception. As an example, our experiments considered the depth perception of a virtual object always placed at \textasciitilde 3 meters. The evolution of the anisotropy according to the distance considered seems another worthwhile topic to consider. Indeed, as mentioned in the literature, the space perception varies with the distance field considered, both in real~\autocite{Koenderink2000} and virtual~\autocite{Bruder2015c} environments. 
%
Thus, more studies in real environments are needed to provide a better characterization of this phenomenon and to elicit the specific influence of \gls{VR} compared with reality. 
% Last, the interaction of this anisotropic distance perception with the well-known depth underestimation in \gls{VR} represents another exciting topic to tackle in future work. 
%

