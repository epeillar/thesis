
While the first experiment showed that distance perception in \gls{VR} was anisotropic, it did not assess how this anisotropy changed with respect to the angle defined by the ``in front'' and ``on the side'' stimuli.
%
Thus, this second experiment aimed to test our second hypothesis \textbf{H2}: the anisotropy of egocentric distance perception in \gls{VR} is correlated with this angle. 
%
It followed the same apparatus and response protocol as the first experiment (see~\autoref{ref:exp1-procedure}), but the experimental design was modified to assess changes in the estimated distance for a discrete number of angle values (15°, 30° and 45°).


\subsubsection{Participants} \label{ref:exp3-participants}
This experiment involved 16 participants (2 females, 14 males). They were aged
from 18 to 56 (mean=$29.4$, SD=$10.4$). 
% Among them, 2 were left-handed and 14 were
% right-handed, 7 had a left dominant eye and 9 had a right dominant eye. 
The participants were students or members of the laboratory, naive about the purpose of the experiment. 
They were not paid for
their participation. All our participants had normal or corrected-to-normal
vision and 13 out of 16 already took part in an experiment or demo
involving \glspl{HMD} before.
%
None of them took part in any of the previous experiments.

\subsubsection{Experimental design}\label{ref:exp3-expDesign}

\begin{figure}
	\centering
	\includegraphics[width=0.6\columnwidth]{pictures/configuration3}
	\caption[Exp. 1.2 - Stimulus configuration]{Configuration of the displayed spheres for Experiment 2. Only one sphere is visible at a time. They are displayed by pairs of two, one in front and one on the side, sequentially, in a randomized order. The side (left or right) is not a parameter of the experimental design and is chosen randomly.}
	\label{fig:configuration3}
\end{figure}

\begin{table}
	\centering
	\caption[Exp. 1.2 - Summary of experimental variables]{Independent and
	Dependent Variables of Experiment 2.}
	\begin{tabular}{|c|r|c|l|}
		\hline
		\multicolumn{4}{|c|}{\textbf{Independent Variables}} \\
		\hline
		\multicolumn{2}{|r|}{\textit{observers}} & 21 & (random variable) \\
		\hline
		\textbf{C1} & \textit{reference position} & 2 & In front \\
		& & & On the side \\
		\hline
		\textbf{C2} & \textit{angle} & 3 & 15°, 30°, 45° \\
		\hline
		\textbf{C3} & \textit{distance} & 5 & -10\%, -5\%, 0\%, 5\%, 10\% \\
		\hline
		\textbf{C4} & \textit{first sphere} & 2 & Reference \\
		& & & Comparison \\
		\hline
		\multicolumn{2}{|r|}{\textit{repetition}} & 3 & 1, 2, 3 \\
		\hline
		\multicolumn{4}{|c|}{\textbf{Dependent Variables}} \\
		\hline
		\multicolumn{2}{|r|}{\textit{chosen sphere}} & \multicolumn{2}{p{120pt}|}{Reference or comparison sphere chosen} \\
		\hline
	\end{tabular}
	\label{tab:exp3-variables}
\end{table}

We used a 2$\times$3$\times$5$\times$2 full-factorial within-subjects
design:

\begin{itemize}[itemsep=1pt,topsep=1pt,parsep=1pt,partopsep=1pt,leftmargin=5mm]
	\item \textbf{C1} is the position of the reference sphere, which can be in	front or on the side (but always displayed 3m away from the participant's head center).

	\item \textbf{C2} is the angle between the reference and comparison spheres. The three possible values chosen were 15º, 30º and 45º.

	%In the pilot experiment (see~\autoref{ref:preExp}) we did not observe strong effects between 45º and 90º, thus w
	We decided to focus on smaller angles in order to determine the angle from which the effect appears. These values can also be completed by the 60° case studied in the first experiment.

	\item \textbf{C3} is the relative distance between the reference and the comparison spheres. Five possible values were chosen after preliminary testings:
	$-0.3$ m, $-0.15$ m, $0$ m, $+0.15$ m, $+0.3$ m compared to the reference position.

	\item \textbf{C4} is the order of presentation of the two spheres, which was counterbalanced
	to minimize order effect.
\end{itemize}

Since the \textit{side} factor was found not significant in the previous
experiment, we chose to remove it from our experimental factors to reduce the number of trials. However, the \textit{on the side} sphere was randomly placed right or left to avoid displaying the \textit{on the side} stimuli always on the same side. This could have generated discomfort due to the fact that the subjects would have been turning their head always to the same side. In addition this helped the participant to keep facing front during the experiment.

In summary, participants were presented with 180 trials, divided in 3 blocks of 60
trials in a different randomized order for each block. Each block of 60 trials
presented a set of couples of spheres made of: 2 reference positions
(\textbf{C1}) $\times$ 3 angles (\textbf{C2}) $\times$ 5 relative positions
(\textbf{C3}) $\times$ 2 presentation orders (\textbf{C4}).
